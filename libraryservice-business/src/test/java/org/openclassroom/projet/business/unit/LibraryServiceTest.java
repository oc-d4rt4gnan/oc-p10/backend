package org.openclassroom.projet.business.unit;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openclassroom.projet.business.services.contract.LibraryService;
import org.openclassroom.projet.business.services.impl.LibraryServiceImpl;
import org.openclassroom.projet.consumer.DaoFactory;
import org.openclassroom.projet.consumer.repository.LibraryRepository;
import org.openclassroom.projet.consumer.repository.StockRepository;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.library.Stock;
import org.openclassroom.projet.model.database.library.StockId;
import org.openclassroom.projet.model.enums.FunctionalExceptionEnum;
import org.openclassroom.projet.technical.exceptions.FunctionalException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


/**
 * @author D4RT4GNaN - Maxime Blaise
 * @since 29-08-2021
 */
@ExtendWith(MockitoExtension.class)
class LibraryServiceTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    @InjectMocks
    private static LibraryService libraryService;
    private static Book           book     = new Book();
    private static Library        expected = new Library();
    private static StockId        stockId  = new StockId();
    private static Stock          stock1   = new Stock();
    private static Stock          stock2   = new Stock();
    
    @Mock
    private DaoFactory        daoFactory;
    @Mock
    private LibraryRepository libraryRepository;
    @Mock
    private StockRepository   stockRepository;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void initialize () {
        libraryService = new LibraryServiceImpl();
        
        book.setReference("d8f6e2be-ce1a-42fc-8eb5-5a414dd6b2fc");
        expected.setNumberRef(1);
        stockId.setBookReference(book.getReference());
        
        stockId.setLibaryNumberRef(0);
        stock1.setQuantity(10);
        stock1.setQuantityBorrowed(10);
        stock1.setStockId(stockId);
        
        stockId.setLibaryNumberRef(expected.getNumberRef());
        stock2.setQuantity(10);
        stock2.setStockId(stockId);
    }
    
    @Test
    @DisplayName("As a system, when trying to get the library where are the returned book, I want to get an " +
                 "the library information")
    void givenABookReference_whenGetLibraryWhereAreTheReturnedBook_thenReturnTheRightLibrary ()
    throws FunctionalException {
        // GIVEN
        stock2.setQuantityBorrowed(9);
        
        List<Stock> stocks = Arrays.asList(stock1, stock2);
        
        when(daoFactory.getLibraryRepository()).thenReturn(libraryRepository);
        when(daoFactory.getStockRepository()).thenReturn(stockRepository);
        when(stockRepository.findAllByStockIdBookReference(anyString())).thenReturn(stocks);
        when(libraryRepository.findByNumberRef(anyInt())).thenReturn(expected);
        
        // WHEN
        Library result = libraryService.getLibraryWhereAreTheReturnedBook(book);
        
        // THEN
        assertThat(result.getNumberRef()).isEqualTo(expected.getNumberRef());
    }
    
    @Test
    @DisplayName("As a system, when trying to get the library where are an unavailable book, I want to get an " +
                 "exception")
    void givenAnUnavailableBook_whenGetLibraryWhereAreTheReturnedBook_thenThrowAFunctionalException () {
        // GIVEN
        stock2.setQuantityBorrowed(10);
        
        List<Stock> stocks = Arrays.asList(stock1, stock2);
        
        when(daoFactory.getStockRepository()).thenReturn(stockRepository);
        when(stockRepository.findAllByStockIdBookReference(anyString())).thenReturn(stocks);
        
        // THEN
        assertThatThrownBy(() -> {
            // WHEN
            libraryService.getLibraryWhereAreTheReturnedBook(book);
        }).isInstanceOf(FunctionalException.class).hasFieldOrPropertyWithValue("type",
                                                                               FunctionalExceptionEnum.NO_BOOK_AVAILABLE);
    }
    
    @Test
    @DisplayName("As a system, I want to call the findAll method, When trying to get the list of libraries")
    void givenNothing_whenTryingToGetTheListOfLibraries_thenItCallTheFindAllMethod () {
        // GIVEN
        when(daoFactory.getLibraryRepository()).thenReturn(libraryRepository);
        when(libraryRepository.findAll()).thenReturn(null);
        
        // WHEN
        libraryService.getLibraries();
        
        // THEN
        verify(libraryRepository, times(1)).findAll();
    }
    
    @Test
    @DisplayName("As a system, I want to retrieves the list of libraries, When trying to get it")
    void givenNothing_whenTryingToGetTheListOfLibraries_thenItReturnTheListOfLibraries () {
        // GIVEN
        Library library = new Library();
        List<Library> libraries = Collections.singletonList(library);
        
        when(daoFactory.getLibraryRepository()).thenReturn(libraryRepository);
        when(libraryRepository.findAll()).thenReturn(libraries);
        
        // WHEN
        List<Library> result = libraryService.getLibraries();
        
        // THEN
        assertThat(result).isEqualTo(libraries);
    }
    
    @Test
    @DisplayName("As a system, I want to call the findByNumberRef method, When trying to get a library by its number " +
                 "reference")
    void givenTheNumberReferenceOfALibrary_whenTryingToGetIt_thenItCallTheFindByNumberRefMethod () {
        // GIVEN
        when(daoFactory.getLibraryRepository()).thenReturn(libraryRepository);
        when(libraryRepository.findByNumberRef(anyInt())).thenReturn(null);
        
        // WHEN
        libraryService.getLibrary(1);
        
        // THEN
        verify(libraryRepository, times(1)).findByNumberRef(1);
    }
    
    @Test
    @DisplayName("As a system, I want to retrieves a library, When trying to get it by its reference")
    void givenTheNumberReferenceOfALibrary_whenTryingToGetIt_thenItReturnTheGoodLibrary () {
        // GIVEN
        Library library = new Library();
        
        when(daoFactory.getLibraryRepository()).thenReturn(libraryRepository);
        when(libraryRepository.findByNumberRef(anyInt())).thenReturn(library);
        
        // WHEN
        Library result = libraryService.getLibrary(1);
        
        // THEN
        assertThat(result).isEqualTo(library);
    }
}