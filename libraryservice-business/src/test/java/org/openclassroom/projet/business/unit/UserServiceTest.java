package org.openclassroom.projet.business.unit;


import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.openclassroom.projet.business.services.contract.UserService;
import org.openclassroom.projet.business.services.impl.MailService;
import org.openclassroom.projet.business.services.impl.UserServiceImpl;
import org.openclassroom.projet.consumer.DaoFactory;
import org.openclassroom.projet.consumer.repository.UsagerRepository;
import org.openclassroom.projet.consumer.repository.VerificationTokenRepository;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.model.database.usager.UsagerDto;
import org.openclassroom.projet.model.database.usager.VerificationToken;
import org.openclassroom.projet.model.enums.TokenTypeEnum;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.validation.ConstraintViolationException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


/**
 * Test class for {@link UserService}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 25-09-2021
 */
@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    @InjectMocks
    private static UserService classUnderTest;
    
    @Mock
    private DaoFactory                  daoFactory;
    @Mock
    private UsagerRepository            usagerRepository;
    @Mock
    private VerificationTokenRepository verificationTokenRepository;
    @Mock
    private AuthenticationManager       authenticationManager;
    @Mock
    private UserDetailsService          userDetailsService;
    @Mock
    private MailService                 mailService;
    
    private Usager            user;
    private String            email;
    private String            password;
    private String            confirmation;
    private String            token;
    private VerificationToken verificationToken;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new UserServiceImpl();
    }
    
    @BeforeEach
    void initVariable () {
        email        = "nom.prenom@email.fr";
        password     = "4bH@k~Ng%7BR5>QM";
        confirmation = "4bH@k~Ng%7BR5>QM";
        token        = "token";
    
        verificationToken = new VerificationToken();
    
        user = new Usager();
        user.setId(1);
        user.setEmail(email);
        user.setEnabled(true);
        user.setAddress("adresse");
        user.setLastName("lastname");
        user.setFirstName("firstname");
    
        when(daoFactory.getVerificationTokenRepository()).thenReturn(verificationTokenRepository);
        when(daoFactory.getUsagerRepository()).thenReturn(usagerRepository);
    }
    
    @Nested
    @MockitoSettings(strictness = Strictness.LENIENT)
    class changeUserPassword {
        
        @BeforeEach
        void initVariable () {
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user);
        }
        
        @Test
        @DisplayName("As a system, I want return true, When I succeed to change password")
        void givenAValidUserAndMatchingPasswordAndConfirmation_whenTryingToUpdateThePassword_thenItReturnTrue () {
            // GIVEN
            when(usagerRepository.save(any())).thenReturn(null);
            when(authenticationManager.authenticate(any())).thenReturn(null);
            
            // WHEN
            boolean result = classUnderTest.changeUserPassword(email, password, confirmation);
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        @DisplayName("As a system, I want to throw a ConstraintViolationException, When the user or password is not " +
                     "invalid")
        void givenAnInvalidConfirmationPassword_whenTryingToUpdateThePassword_thenItThrowAConstraintViolationException () {
            // GIVEN
            confirmation = "password";
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.changeUserPassword(email, password, confirmation);
            }).isInstanceOf(ConstraintViolationException.class);
        }
        
        @Test
        @DisplayName("As a system, I want to throw a RuntimeException, When the account is not enabled")
        void givenAUserNotEnabled_whenTryingToUpdateThePassword_thenItThrowARuntimeException () {
            // GIVEN
            user.setEnabled(false);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.changeUserPassword(email, password, confirmation);
            }).isInstanceOf(RuntimeException.class).hasMessage(
                    "This account is not already activated ! First check your email !");
        }
        
        @Test
        @DisplayName("As a system, I want return false, When the user is not authenticated")
        void givenAnUnauthenticatedUser_whenTryingToUpdateThePassword_thenItReturnFalse () {
            // GIVEN
            when(usagerRepository.save(any())).thenReturn(null);
            
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                    = new UsernamePasswordAuthenticationToken(user, password, ((UserDetails) user).getAuthorities());
            usernamePasswordAuthenticationToken.setAuthenticated(false);
            
            UserServiceImpl spy = (UserServiceImpl) Mockito.spy(classUnderTest);
            doReturn(usernamePasswordAuthenticationToken).when(spy).authenticate(user.getUsername(), password);
            
            // WHEN
            boolean result = spy.changeUserPassword(email, password, confirmation);
            
            // THEN
            assertThat(result).isFalse();
        }
        
    }
    
    @Nested
    @MockitoSettings(strictness = Strictness.LENIENT)
    class createNewPasswordForUsagerWith {
        
        @BeforeEach
        void initVariable () {
            verificationToken.setUsager(user);
            verificationToken.setType("PASSWORD");
        }
        
        @Test
        @DisplayName("As a system, I want return true, When I reset the password")
        void givenAValidUserAndValidTokenAndMatchingPasswordAndConfirmation_whenTryToResetTheUserPassword_thenItReturnTrue () {
            // GIVEN
            when(verificationTokenRepository.findByToken(anyString())).thenReturn(verificationToken);
            when(usagerRepository.save(any())).thenReturn(null);
            doNothing().when(verificationTokenRepository).delete(any());
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user);
            
            // WHEN
            boolean result = classUnderTest.createNewPasswordForUsagerWith(token, password, confirmation);
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        @DisplayName("As a system, I want return false, When I try to reset the password to an unauthenticated account")
        void givenAnUnauthenticatedAccount_whenTryToResetTheUserPassword_thenItReturnFalse () {
            // GIVEN
            when(verificationTokenRepository.findByToken(anyString())).thenReturn(verificationToken);
            when(usagerRepository.save(any())).thenReturn(null);
            doNothing().when(verificationTokenRepository).delete(any());
            
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                    = new UsernamePasswordAuthenticationToken(user, password, ((UserDetails) user).getAuthorities());
            usernamePasswordAuthenticationToken.setAuthenticated(false);
            
            UserServiceImpl spy = (UserServiceImpl) Mockito.spy(classUnderTest);
            doReturn(usernamePasswordAuthenticationToken).when(spy).authenticate(user.getUsername(), password);
            
            // WHEN
            boolean result = spy.createNewPasswordForUsagerWith(token, password, confirmation);
            
            // THEN
            assertThat(result).isFalse();
        }
        
        @Test
        @DisplayName("As a system, I want throwing a ConstraintViolationException, When I try to reset the password " +
                     "of an invalid account")
        void givenAnInvalidAccount_whenTryToResetTheUserPassword_thenItThrowAConstraintViolationException () {
            // GIVEN
            String confirmation = "password";
            
            when(verificationTokenRepository.findByToken(anyString())).thenReturn(verificationToken);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.createNewPasswordForUsagerWith(token, password, confirmation);
            }).isInstanceOf(ConstraintViolationException.class);
        }
        
        @Test
        @DisplayName("As a system, I want throwing a RuntimeException, When I try to reset the password with no " +
                     "account associated to the token")
        void givenATokenWithNoAccountAssociated_whenTryToResetTheUserPassword_thenItThrowARuntimeException () {
            // GIVEN
            verificationToken = new VerificationToken();
            
            when(verificationTokenRepository.findByToken(anyString())).thenReturn(verificationToken);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.createNewPasswordForUsagerWith(token, password, confirmation);
            }).isInstanceOf(RuntimeException.class).hasMessage(
                    "There is no account associated to this token for reset password !");
        }
        
        @Test
        @DisplayName("As a system, I want throwing a RuntimeException, When I try to reset password with EMAIL token")
        void givenAVerificationTokenOfEMAILType_whenTryToResetTheUserPassword_thenItThrowARuntimeException () {
            // GIVEN
            verificationToken.setType("EMAIL");
            
            when(verificationTokenRepository.findByToken(anyString())).thenReturn(verificationToken);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.createNewPasswordForUsagerWith(token, password, confirmation);
            }).isInstanceOf(RuntimeException.class).hasMessage("This token is not for reset password !");
        }
        
    }
    
    @Nested
    @MockitoSettings(strictness = Strictness.LENIENT)
    class login {
    
        @Test
        @DisplayName("As a system, I want return the right user, When I try to login.")
        void given_when_thenItReturnTheRightUser () throws Exception {
            // GIVEN
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user);
            when(usagerRepository.findByEmail(anyString())).thenReturn(user);
            when(authenticationManager.authenticate(any())).thenReturn(null);
        
            // WHEN
            Usager result = classUnderTest.login(user.getUsername(), password);
        
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo(user);
        }
    
        @Test
        @DisplayName("As a system, I want throwing an Exception, When I try to login with the wrong credentials")
        void givenAWrongCredentials_whenTryingToLogIn_thenItThrowAnException () {
            // GIVEN
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                    = new UsernamePasswordAuthenticationToken(user, password, ((UserDetails) user).getAuthorities());
            usernamePasswordAuthenticationToken.setAuthenticated(false);
    
            UserServiceImpl spy = (UserServiceImpl) Mockito.spy(classUnderTest);
            doReturn(usernamePasswordAuthenticationToken).when(spy).authenticate(user.getUsername(), password);
        
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                spy.login(user.getUsername(), password);
            }).hasMessage("Incorrect username or password !");
        }
        
        @Test
        @DisplayName("As a system, I want throwing an Exception, When I try to login with an inactivate account.")
        void givenAnAccountNotEnabled_whenTryingToLogIn_thenItThrowAnException () {
            // GIVEN
            user.setEnabled(false);
            
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user);
            when(authenticationManager.authenticate(any())).thenReturn(null);
            when(usagerRepository.findByEmail(anyString())).thenReturn(user);
        
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.login(user.getUsername(), password);
            }).hasMessage("Your account is not yet activated. Check your mail before !");
        }
    }
    
    @Nested
    @MockitoSettings(strictness = Strictness.LENIENT)
    class resendVerificationEmail {
        
        @Test
        @DisplayName("As a system, I want to send again the account verification mail, When the method is called with" +
                     " an unverified account.")
        void givenAnUnverifiedAccount_whenTryingToSendAgainTheAccountVerificationMail_thenItSendAgainTheAccountConfirmationMail ()
        throws Exception {
            // GIVEN
            user.setEnabled(false);
            
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user);
            when(mailService.resendConfirmationEmail(any(), anyString())).thenReturn(true);
            
            UserServiceImpl spy = (UserServiceImpl) Mockito.spy(classUnderTest);
            doReturn(token).when(spy).createVerificationToken(user, TokenTypeEnum.EMAIL);
            
            // WHEN
            spy.resendVerificationEmail(user.getEmail());
            
            // THEN
            verify(mailService, times(1)).resendConfirmationEmail(user, token);
        }
        
        @Test
        @DisplayName("As a system, I want throwing an Exception, When I try to send again the account verification " +
                     "mail to an already activated account.")
        void givenAnActivatedAccount_whenTryingToSendAgainTheAccountVerificationMail_thenItThrowAnException () {
            // GIVEN
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.resendVerificationEmail(user.getEmail());
            }).hasMessage("This account is already activated !");
        }
    }
    
    @Nested
    @MockitoSettings(strictness = Strictness.LENIENT)
    class save {
        
        // INNER VARIABLE
        private UsagerDto userDTO;
        
        @BeforeEach
        void initVariable () {
            userDTO = new UsagerDto();
            userDTO.setEmail(email);
            userDTO.setFirstName("firstname");
            userDTO.setLastName("lastname");
            userDTO.setAddress("adresse");
            userDTO.setPassword(password);
            userDTO.setConfirmPassword(confirmation);
            userDTO.setEnabled(false);
        }
        
        @Test
        @DisplayName("As a system, I want to return the token newly created, When I try to create a new account for a" +
                     " user.")
        void givenANewUser_whenTryingToCreateNewAccount_thenItReturnTheToken () throws Exception {
            // GIVEN
            when(usagerRepository.save(any())).thenReturn(null);
            when(usagerRepository.findByEmail(anyString())).thenReturn(null);
            UserServiceImpl spy = (UserServiceImpl) Mockito.spy(classUnderTest);
            doReturn(token).when(spy).createVerificationToken(any(), any());
            
            // WHEN
            String result = spy.save(userDTO);
            
            // THEN
            assertThat(result).isEqualTo(token);
        }
        
        @Test
        @DisplayName("As a system, I want throwing a ConstraintViolationException, When I try to create a new account" +
                     " with invalid user's information")
        void givenAnInvalidUser_whenTryingToCreateNewAccount_thenItThrowAConstraintViolationException () {
            // GIVEN
            userDTO.setConfirmPassword("password");
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.save(userDTO);
            }).isInstanceOf(ConstraintViolationException.class);
        }
        
        @Test
        @DisplayName("As a system, I want throwing an Exception, When I try to create a new account for a user that " +
                     "already have an account")
        void givenAUserAlreadyRegistered_whenTryingToCreateNewAccount_thenItThrowAnException () {
            // GIVEN
            when(usagerRepository.findByEmail(anyString())).thenReturn(user);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.save(userDTO);
            }).hasMessage("There is already user with this email!");
        }
    }
    
    @Nested
    @MockitoSettings(strictness = Strictness.LENIENT)
    class sendEmailToResetPasswordFor {
        
        @Test
        @DisplayName("As a system, I want return the newly created token, When I try to allow an user to reset his " +
                     "password")
        void givenAnExistingUser_whenTryingToAllowUserToResetHisPassword_thenItReturnTheToken () throws Exception {
            // GIVEN
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user);
            when(mailService.sendResetPasswordEmail(user, token)).thenReturn(true);
            
            UserServiceImpl spy = (UserServiceImpl) Mockito.spy(classUnderTest);
            doReturn(token).when(spy).createVerificationToken(any(), any());
            
            // WHEN
            String result = spy.sendEmailToResetPasswordFor(email);
            
            // THEN
            assertThat(result).isEqualTo(token);
        }
        
        @Test
        @DisplayName("As a system, I want throwing an Exception, When I try to allow an user to reset his password")
        void givenAnInactivateUser_whenTryingToAllowUserToResetHisPassword_thenItThrowAnException () {
            // GIVEN
            user.setEnabled(false);
            
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.sendEmailToResetPasswordFor(email);
            }).hasMessage("This account is not already activated ! First check your email !");
        }
    }
    
    @Nested
    @MockitoSettings(strictness = Strictness.LENIENT)
    class updateUsagerInfos {
        
        @Test
        @DisplayName("As a system, I want return true, When I try to update basic user information")
        void givenAnUserWithUpdatedInformation_whenTryingToUpdateUserInfos_thenItReturnTrue () throws Exception {
            // GIVEN
            Usager expected = new Usager();
            expected.setEmail(email);
            expected.setFirstName("Maxime");
            expected.setLastName("Blaise");
            expected.setAddress("15 rue Basse 06250 Mougins");
            
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user, expected);
            when(usagerRepository.save(any())).thenReturn(null);
            
            // WHEN
            boolean result = classUnderTest.updateUsagerInfos(email, expected);
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        @DisplayName("As a system, I want return true, When I try to update the user email")
        void givenAnUserWithUpdatedEmail_whenTryingToUpdateUserInfos_thenItReturnTrue () throws Exception {
            // GIVEN
            Usager expected = new Usager();
            expected.setEmail("prenom.nom@email.com");
            
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user, expected);
            when(usagerRepository.save(any())).thenReturn(null);
            UserServiceImpl spy = (UserServiceImpl) Mockito.spy(classUnderTest);
            doReturn(true).when(spy).resendVerificationEmail(expected.getEmail());
            
            // WHEN
            boolean result = spy.updateUsagerInfos(email, expected);
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        @DisplayName("As a system, I want return false, When I try to update user with no changes.")
        void givenUserWithNoChanges_whenTryingToUpdateUserInfos_thenItReturnFalse () throws Exception {
            // GIVEN
            when(userDetailsService.loadUserByUsername(anyString())).thenReturn(user);
            when(usagerRepository.save(any())).thenReturn(null);
            
            // WHEN
            boolean result = classUnderTest.updateUsagerInfos(email, user);
            
            // THEN
            assertThat(result).isFalse();
        }
    }
    
    @Nested
    @MockitoSettings(strictness = Strictness.LENIENT)
    class validAccountFor {
        
        @Test
        @DisplayName("As a system, I want return true, When I try to validate an account")
        void givenTheRightTokenForAnUser_whenTryingToValidateAnAccount_thenItReturnTrue () {
            // GIVEN
            verificationToken.setUsager(user);
            verificationToken.setType("EMAIL");
            
            when(usagerRepository.save(any())).thenReturn(null);
            doNothing().when(verificationTokenRepository).delete(any());
            when(usagerRepository.findByEmail(anyString())).thenReturn(user);
            
            // WHEN
            boolean result = classUnderTest.validAccountFor(verificationToken);
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        @DisplayName("As a system, I want throwing a RuntimeException, When the input token is not associated with an" +
                     " account")
        void givenATokenWithoutAssociatedUser_whenTryingToValidateAnAccount_thenItThrowARuntimeException () {
            // GIVEN -- Nothing to do
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.validAccountFor(verificationToken);
            }).hasMessage("There is no account associated to this token !");
        }
        
        @Test
        @DisplayName("As a system, I want throwing a RuntimeException, When I try to validate an account with a " +
                     "password token")
        void givenAPasswordToken_whenTryingToValidateAnAccount_thenItThrowARuntimeException () {
            // GIVEN
            verificationToken.setUsager(user);
            verificationToken.setType("PASSWORD");
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.validAccountFor(verificationToken);
            }).hasMessage("There is no email token associated to this account !");
        }
    }
    
    @Nested
    @MockitoSettings(strictness = Strictness.LENIENT)
    class verifyEmailFrom {
        
        @Test
        @DisplayName("As a system, I want return the verification token, When I try to get it by its token")
        void givenAToken_whenTryingToGetTheVerificationTokenAssociated_thenItReturnIt () {
            // GIVEN
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(new Date());
            calendar.add(Calendar.WEEK_OF_MONTH, 2);
            
            verificationToken.setType("EMAIL");
            verificationToken.setExpiryDate(calendar.getTime());
            
            when(verificationTokenRepository.findByToken(anyString())).thenReturn(verificationToken);
            
            // WHEN
            VerificationToken result = classUnderTest.verifyEmailFrom(token);
            
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo(verificationToken);
        }
        
        @Test
        @DisplayName("As a system, I want throwing a RuntimeException, When I try to get a verification token with an" +
                     " expired token")
        void givenAnExpiredToken_whenTryingToGetTheVerificationTokenAssociated_thenItThrowARuntimeException () {
            // GIVEN
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(new Date());
            calendar.add(Calendar.WEEK_OF_MONTH, -2);
            
            verificationToken.setType("EMAIL");
            verificationToken.setExpiryDate(calendar.getTime());
            
            when(verificationTokenRepository.findByToken(anyString())).thenReturn(verificationToken);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.verifyEmailFrom(token);
            }).isInstanceOf(RuntimeException.class).hasMessage("the link is no longer valid !");
        }
        
        @Test
        @DisplayName("As a system, I want throwing a RuntimeException, When I try to get a verification token with " +
                     "the wrong type of token")
        void givenAWrongTokenType_whenTryingToGetTheVerificationTokenAssociated_thenItThrowARuntimeException () {
            // GIVEN
            verificationToken.setType("PASSWORD");
            
            when(verificationTokenRepository.findByToken(anyString())).thenReturn(verificationToken);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.verifyEmailFrom(token);
            }).isInstanceOf(RuntimeException.class)
              .hasMessage("This verification token doesn't exist or is not valid !");
        }
        
        @Test
        @DisplayName("As a system, I want throwing a RuntimeException, When I try to get a verification token without" +
                     " token")
        void givenATokenWithNoCorrespondingVerificationToken_whenTryingToGetTheVerificationTokenAssociated_thenItThrowARuntimeException () {
            // GIVEN
            when(verificationTokenRepository.findByToken(anyString())).thenReturn(null);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.verifyEmailFrom(token);
            }).isInstanceOf(RuntimeException.class)
              .hasMessage("This verification token doesn't exist or is not valid !");
        }
        
    }
    
    @Nested
    @MockitoSettings(strictness = Strictness.LENIENT)
    class createVerificationToken {
    
        @Test
        @DisplayName("As a system, I want return the new created token, When I try to create a new token")
        void givenAUserWithNoToken_whenTryingToCreateANewVerificationToken_thenItReturnTheToken () {
            // GIVEN
            UserServiceImpl spy = (UserServiceImpl) Mockito.spy(classUnderTest);
        
            when(verificationTokenRepository.findByUsager(any())).thenReturn(null);
            when(verificationTokenRepository.save(any())).thenReturn(null);
        
            // WHEN
            String result = spy.createVerificationToken(user, TokenTypeEnum.EMAIL);
        
            // THEN
            assertThat(result).isNotBlank();
        }
    
        @Test
        @DisplayName("As a system, I want return the new created token, When I try to update / reset an existing token")
        void givenAUserAlreadyWithAToken_whenTryingToCreateANewVerificationToken_thenItReturnTheNewToken () {
            // GIVEN
            UserServiceImpl spy = (UserServiceImpl) Mockito.spy(classUnderTest);
        
            when(verificationTokenRepository.findByUsager(any())).thenReturn(verificationToken);
            when(verificationTokenRepository.save(any())).thenReturn(null);
        
            // WHEN
            String result = spy.createVerificationToken(user, TokenTypeEnum.EMAIL);
        
            // THEN
            assertThat(result).isNotBlank();
        }
    
    }
}