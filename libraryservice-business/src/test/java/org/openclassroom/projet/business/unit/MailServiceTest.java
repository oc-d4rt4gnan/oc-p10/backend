package org.openclassroom.projet.business.unit;


import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openclassroom.projet.business.services.impl.MailService;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.technical.exceptions.TechnicalException;
import org.openclassroom.projet.technical.spring.properties.MailConfig;
import org.openclassroom.projet.technical.utils.mails.MailUtils;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


/**
 * Test class for the {@link MailService}.
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 25/09/2021
 */
@ExtendWith(MockitoExtension.class)
class MailServiceTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    @InjectMocks
    private static MailService classUnderTest;
    
    @Mock
    private MailUtils mailUtils;
    @Mock
    private MailConfig mailConfig;
    
    private Usager user;
    private String token;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new MailService();
    }
    
    @BeforeEach
    void initVariable () throws TechnicalException {
        user = new Usager();
        user.setEmail("email");
        
        token = "token";
    
        ReflectionTestUtils.setField(classUnderTest, "clientUrl", "http://localhost:8080/");
    
        when(mailUtils.sendMailSMTP(anyString(), anyString(), anyString(), anyBoolean())).thenReturn(true);
    }
    
    @Nested
    class EmailVerification {
        
        private String content;
        
        @BeforeEach
        void initVariable () {
            content = "Thanks for signing up on our service! You must follow this link to activate your " +
                      "account:\n\nhttp://localhost:8080/confirmation/?token=token\n\nHave fun, and don't hesitate" +
                      " to contact us with your feedback.\n\nThe Library Team";
            
            ReflectionTestUtils.setField(classUnderTest, "confirmEmailUrl", "confirmation/");
        }
        
        @Test
        @DisplayName("As a system, I want to check that the right parameters have been passed, When I send the " +
                     "account confirmation email.")
        void givenAUserAndAToken_whenTryingToSendTheConfirmationEmail_thenItCallTheSendMailSMTPMethodWithGoodParameter ()
        throws TechnicalException {
            // GIVEN
            String object = "Confirm your account";
            
            // WHEN
            classUnderTest.sendConfirmationEmail(user, token);
            
            // THEN
            verify(mailUtils, times(1)).sendMailSMTP(user.getEmail(), object, content, true);
        }
        
        @Test
        @DisplayName("As a system, I want to check that the right parameters have been passed, When I send back the " +
                     "account confirmation email.")
        void givenAUserAndAToken_whenTryingToSendBackTheConfirmationEmail_thenItCallTheSendMailSMTPMethodWithGoodParameter ()
        throws TechnicalException {
            // GIVEN
            String object = "Resend : Confirm your account";
            
            // WHEN
            classUnderTest.resendConfirmationEmail(user, token);
            
            // THEN
            verify(mailUtils, times(1)).sendMailSMTP(user.getEmail(), object, content, true);
        }
        
    }
    
    @Test
    @DisplayName("As a system, I want to check that the right parameters have been passed, When trying to send the " +
                 "reminder mail of expired borrowing.")
    void givenAUser_whenTryingToSendTheReminderMailOfExpiredBorrowing_thenItCallTheSendMailSMTPMethodWithGoodParameter ()
    throws TechnicalException {
        // GIVEN
        String object = "Reminder : Borrowing expired !";
        String content
                = "Your borrowing has expired please extend it if it's not already done or return quickly your book !";
        
        // WHEN
        classUnderTest.sendReminderEmail(user);
        
        // THEN
        verify(mailUtils, times(1)).sendMailSMTP(user.getEmail(), object, content, true);
    }
    
    @Test
    @DisplayName("As a system, I want to check that the right parameters have been passed, When I try to send a reset" +
                 " password email.")
    void givenAUserAndAToken_whenTryingToSendAResetPasswordMail_thenItCallTheSendMailSMTPMethodWithGoodParameter ()
    throws TechnicalException {
        // GIVEN
        String object = "Forgotten password";
        String content =
                "To reset your password, follow this link and change " +
                "it:\n\nhttp://localhost:8080/reset-password/?token=token\n\nHave fun, and don't hesitate" +
                " to contact us with your feedback.\n\nThe Library Team";
        
        ReflectionTestUtils.setField(classUnderTest, "newPasswordUrl", "reset-password/");
        
        // WHEN
        classUnderTest.sendResetPasswordEmail(user, token);
        
        // THEN
        verify(mailUtils, times(1)).sendMailSMTP(user.getEmail(), object, content, true);
    }
    
    @Test
    @DisplayName("As a system, I want to check that the right parameters have been passed, When I try to send the " +
                 "mail of new availability of a book.")
    void givenAUserAndABookAndALibrary_whenTryingToSendMailForNewAvailabilityOfBook_thenItCallTheSendMailSMTPMethodWithGoodParameter ()
    throws TechnicalException {
        // GIVEN
        Book book = new Book();
        book.setTitle("title");
        
        Library library = new Library();
        
        String object = "Your Book title is available !";
        String content = "content";
        
        when(mailConfig.getTemplate(anyString())).thenReturn("");
        when(mailUtils.fillBookAvailabilityTemplate(anyString(), any(), any(), any())).thenReturn("content");
        
        // WHEN
        classUnderTest.sendMailOfNewAvailabilityOfBook(user, book, library);
        
        // THEN
        verify(mailUtils, times(1)).sendMailSMTP(user.getEmail(), object, content, false);
    }
    
}
