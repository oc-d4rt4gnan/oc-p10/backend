package org.openclassroom.projet.business.unit;


import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openclassroom.projet.business.services.contract.BorrowingService;
import org.openclassroom.projet.business.services.impl.BorrowingServiceImpl;
import org.openclassroom.projet.business.services.impl.MailService;
import org.openclassroom.projet.consumer.DaoFactory;
import org.openclassroom.projet.consumer.repository.BorrowingRepository;
import org.openclassroom.projet.consumer.repository.UsagerRepository;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.service.Borrowing;
import org.openclassroom.projet.model.database.service.BorrowingId;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.technical.exceptions.TechnicalException;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


/**
 * Test class for the business class {@link BorrowingService}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 19-09-2021
 */
@ExtendWith(MockitoExtension.class)
class BorrowingServiceTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    @InjectMocks
    private static BorrowingService classUnderTest;
    
    @Mock
    private DaoFactory          daoFactory;
    @Mock
    private BorrowingRepository borrowingRepository;
    @Mock
    private UsagerRepository    usagerRepository;
    @Mock
    private MailService         mailService;
    
    private Date    date;
    private Usager  user;
    private Book    book;
    private Library library;
    private Borrowing borrowing;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void initialize () {
        classUnderTest = new BorrowingServiceImpl();
    }
    
    @BeforeEach
    void initVariable () {
        date = new Date();
        
        user = new Usager();
        user.setId(1);
        
        book = new Book();
        book.setReference("reference");
        
        library = new Library();
        library.setNumberRef(1);
    
        BorrowingId id = new BorrowingId();
        id.setUsager(user);
        id.setBook(book);
        id.setLibrary(library);
        id.setBorrowingDate(date);
    
        borrowing = new Borrowing();
        borrowing.setBorrowingId(id);
    }
    
    @Test
    @DisplayName("As a system, I want to return the amount of book's returned, When the book is returned")
    void givenAnUserIdAndALibraryNumberAndBookReference_whenTheUserReturnHisBook_thenItReturnTheQuantityOfBookReturned () {
        // GIVEN
        borrowing.setStatus("OUTSTANDING");
        borrowing.setExtended(false);
        borrowing.setQuantity(2);
        borrowing.setExpiryDate(date);
        
        when(daoFactory.getBorrowingRepository()).thenReturn(borrowingRepository);
        when(borrowingRepository
                     .findByBorrowingId_BorrowingDateAndBorrowingId_Library_NumberRefAndBorrowingId_Usager_IdAndBorrowingId_Book_ReferenceAndStatusNot(
                             any(),
                             anyInt(),
                             anyInt(),
                             anyString(),
                             anyString())).thenReturn(borrowing);
        when(borrowingRepository.save(any())).thenReturn(null);
        
        // WHEN
        int result = classUnderTest.stopBorrowing(date, library.getNumberRef(), book.getReference(), user.getId());
        
        // THEN
        assertThat(result).isEqualTo(2);
    }
    
    @Test
    @Tag("ExtendBookBorrowed")
    @DisplayName("As a system, I want to extend a borrowing for a book, When expiration date is passed and the " +
                 "borrowing is not already extended.")
    void givenAnUserIdAndALibraryNumberAndBookReference_whenExtendingBorrowingOfBook_thenItReturnIfTheBorrowingIsExtended ()
    throws Exception {
        // GIVEN
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.WEEK_OF_MONTH, 1);
        Date expiryDate = calendar.getTime();
        
        borrowing.setStatus("OUTSTANDING");
        borrowing.setExtended(false);
        borrowing.setQuantity(2);
        borrowing.setExpiryDate(expiryDate);
        
        when(daoFactory.getBorrowingRepository()).thenReturn(borrowingRepository);
        when(borrowingRepository
                     .findByBorrowingId_BorrowingDateAndBorrowingId_Library_NumberRefAndBorrowingId_Usager_IdAndBorrowingId_Book_ReferenceAndStatusNot(
                             any(),
                             anyInt(),
                             anyInt(),
                             anyString(),
                             anyString())).thenReturn(borrowing);
        when(borrowingRepository.save(any())).thenReturn(null);
        
        // WHEN
        boolean result = classUnderTest.extendBookBorrowed(date,
                                                           library.getNumberRef(),
                                                           book.getReference(),
                                                           user.getId());
        
        // THEN
        assertThat(result).isTrue();
    }
    
    @Test
    @Tag("ExtendBookBorrowed")
    @DisplayName("As a system, I want to throw an exception, When the user try to extend an invalid borrowing.")
    void givenAnInvalidBorrowing_whenExtendingBorrowingOfBook_thenItThrowAnException () {
        // GIVEN
        borrowing.setExtended(false);
        
        Borrowing spy = Mockito.spy(borrowing);
        doReturn(false).when(spy).isValid();
        
        when(daoFactory.getBorrowingRepository()).thenReturn(borrowingRepository);
        when(borrowingRepository
                     .findByBorrowingId_BorrowingDateAndBorrowingId_Library_NumberRefAndBorrowingId_Usager_IdAndBorrowingId_Book_ReferenceAndStatusNot(
                             any(),
                             anyInt(),
                             anyInt(),
                             anyString(),
                             anyString())).thenReturn(spy);
        
        // THEN
        assertThatThrownBy(() -> {
            // WHEN
            classUnderTest.extendBookBorrowed(date, library.getNumberRef(), book.getReference(), user.getId());
        }).hasMessage("The loan has passed its deadline.");
    }
    
    @Test
    @Tag("ExtendBookBorrowed")
    @DisplayName("As a system, I want to throw an exception, When the user try to extend a borrowing already extended.")
    void givenABorrowingAlreadyExtended_whenExtendingBorrowingOfBook_thenItThrowAnException () {
        // GIVEN
        borrowing.setExtended(true);
        
        when(daoFactory.getBorrowingRepository()).thenReturn(borrowingRepository);
        when(borrowingRepository
                     .findByBorrowingId_BorrowingDateAndBorrowingId_Library_NumberRefAndBorrowingId_Usager_IdAndBorrowingId_Book_ReferenceAndStatusNot(
                             any(),
                             anyInt(),
                             anyInt(),
                             anyString(),
                             anyString())).thenReturn(borrowing);
        
        // THEN
        assertThatThrownBy(() -> {
            // WHEN
            classUnderTest.extendBookBorrowed(date, library.getNumberRef(), book.getReference(), user.getId());
        }).hasMessage("This borrowing is already extended !");
    }
    
    @Test
    @DisplayName("As a system, I want save new borrowing or modify the amount of current book in a borrowing, When " +
                 "the user add a new borrowing.")
    void givenABorrowing_whenUserWantToAddBorrowing_thenTheBorrowingIsModifiedOrAddedInDatabase () {
        // GIVEN
        borrowing.setExtended(true);
        
        when(daoFactory.getBorrowingRepository()).thenReturn(borrowingRepository);
        when(daoFactory.getUsagerRepository()).thenReturn(usagerRepository);
        when(usagerRepository.findByEmail(user.getEmail())).thenReturn(user);
        when(borrowingRepository
                     .findByBorrowingId_BorrowingDateAndBorrowingId_Library_NumberRefAndBorrowingId_Usager_IdAndBorrowingId_Book_ReferenceAndStatusNot(
                             any(),
                             anyInt(),
                             anyInt(),
                             anyString(),
                             anyString())).thenReturn(null);
        when(borrowingRepository.save(any())).thenReturn(borrowing);
        
        // WHEN
        classUnderTest.addNewBorrowing(borrowing);
        
        // THEN
        verify(borrowingRepository, times(1)).save(borrowing);
    }
    
    @Test
    @Tag("checkExpiration")
    @DisplayName("As a system, I want to return an empty list of overdue borrowing, When checking the expiration time" +
                 " of a valid list of borrowing.")
    void givenAValidListOfBorrowing_whenCheckingTheExpirationTime_thenItReturnAnEmptyListOfOverdueBorrowing ()
    throws TechnicalException {
        // GIVEN
        Borrowing spy = Mockito.spy(borrowing);
        doReturn(true).when(spy).isValid();
        
        List<Borrowing> borrowings = Collections.singletonList(spy);
        
        when(daoFactory.getBorrowingRepository()).thenReturn(borrowingRepository);
        when(borrowingRepository.findByStatusNot(anyString())).thenReturn(borrowings);
        
        // WHEN
        List<Borrowing> result = classUnderTest.checkExpiration();
        
        // THEN
        assertThat(result).isEmpty();
    }
    
    @Test
    @Tag("checkExpiration")
    @DisplayName("As a system, I want to return a list of overdue borrowing, When checking the expiration time of an " +
                 "invalid list of borrowing.")
    void givenAInValidListOfBorrowing_whenCheckingTheExpirationTime_thenItReturnAListOfOverdueBorrowing ()
    throws TechnicalException {
        // GIVEN
        Borrowing spy = Mockito.spy(borrowing);
        doReturn(false).when(spy).isValid();
        
        List<Borrowing> borrowings = Collections.singletonList(spy);
        
        when(daoFactory.getBorrowingRepository()).thenReturn(borrowingRepository);
        when(borrowingRepository.findByStatusNot(anyString())).thenReturn(borrowings);
        when(daoFactory.getUsagerRepository()).thenReturn(usagerRepository);
        when(usagerRepository.findById(anyInt())).thenReturn(user);
        doNothing().when(mailService).sendReminderEmail(any());
        
        // WHEN
        List<Borrowing> result = classUnderTest.checkExpiration();
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(borrowings);
    }
    
    @Test
    @Tag("checkExpiration")
    @DisplayName("As a system, I want to send a reminder email to the user, When it's borrowing is overdue.")
    void givenAInValidListOfBorrowing_whenCheckingTheExpirationTime_thenItSendAReminderEmailToTheUser ()
    throws TechnicalException {
        // GIVEN
        Borrowing spy = Mockito.spy(borrowing);
        doReturn(false).when(spy).isValid();
        
        List<Borrowing> borrowings = Collections.singletonList(spy);
        
        when(daoFactory.getBorrowingRepository()).thenReturn(borrowingRepository);
        when(borrowingRepository.findByStatusNot(anyString())).thenReturn(borrowings);
        when(daoFactory.getUsagerRepository()).thenReturn(usagerRepository);
        when(usagerRepository.findById(anyInt())).thenReturn(user);
        doNothing().when(mailService).sendReminderEmail(any());
        
        // WHEN
        classUnderTest.checkExpiration();
        
        // THEN
        verify(mailService, times(1)).sendReminderEmail(user);
    }
    
    @Test
    @DisplayName("As a system, I want to return the list of borrowing of an user, When he want to get it")
    void givenAValidListOfBorrowing_whenGettingTheListOfBorrowingOfAnUser_thenItReturnTheListOfBorrowingOfTheUser () {
        // GIVEN
        Borrowing spy = Mockito.spy(borrowing);
        doReturn(true).when(spy).isValid();
        
        List<Borrowing> borrowings = Collections.singletonList(spy);
        
        when(daoFactory.getBorrowingRepository()).thenReturn(borrowingRepository);
        when(borrowingRepository.findByBorrowingId_UsagerId(anyInt())).thenReturn(borrowings);
        
        // WHEN
        List<Borrowing> result = classUnderTest.getBorrowingsFor(user.getId());
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(borrowings);
    }
    
    @Test
    @DisplayName("As a system, I want to return the list of borrowing of an user with status changed, When he want to" +
                 " get it and some of them is invalid.")
    void givenAInValidListOfBorrowing_whenGettingTheListOfBorrowingOfAnUser_thenItReturnTheListOfBorrowingOfTheUserWithStatusChanged () {
        // GIVEN
        Borrowing spy = Mockito.spy(borrowing);
        doReturn(false).when(spy).isValid();
        
        List<Borrowing> borrowings = Collections.singletonList(spy);
        
        spy.setStatus("OVERDUE");
        List<Borrowing> expected = Collections.singletonList(spy);
        
        when(daoFactory.getBorrowingRepository()).thenReturn(borrowingRepository);
        when(borrowingRepository.findByBorrowingId_UsagerId(anyInt())).thenReturn(borrowings);
        
        // WHEN
        List<Borrowing> result = classUnderTest.getBorrowingsFor(user.getId());
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
}