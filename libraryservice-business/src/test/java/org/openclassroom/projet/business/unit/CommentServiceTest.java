package org.openclassroom.projet.business.unit;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openclassroom.projet.business.services.contract.CommentService;
import org.openclassroom.projet.business.services.impl.CommentServiceImpl;
import org.openclassroom.projet.consumer.DaoFactory;
import org.openclassroom.projet.consumer.repository.CommentRepository;
import org.openclassroom.projet.consumer.repository.UsagerRepository;
import org.openclassroom.projet.model.database.service.Comment;
import org.openclassroom.projet.model.database.usager.Usager;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


/**
 * Test class for service {@link CommentService}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 25-09-2021
 */
@ExtendWith(MockitoExtension.class)
class CommentServiceTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    @InjectMocks
    private static CommentService classUnderTest;
    
    @Mock
    private DaoFactory        daoFactory;
    @Mock
    private CommentRepository commentRepository;
    @Mock
    private UsagerRepository  usagerRepository;
    
    private Comment comment;
    private Usager  user;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void initialize () {
        classUnderTest = new CommentServiceImpl();
    }
    
    @BeforeEach
    void initVariable () {
        user = new Usager();
        user.setId(1);
        user.setEmail("email");
        
        comment = new Comment();
        comment.setBookReference("book reference");
        comment.setTitle("title");
        comment.setId(1);
        comment.setContent("content");
        comment.setUsager(user);
    }
    
    @Test
    @DisplayName("As a system, I want retrieve the new comment saved, When adding a comment in the database")
    void givenAComment_whenTryingToSaveTheCommentInTheDatabase_thenItReturnTheCommentSaved () {
        // GIVEN
        when(daoFactory.getUsagerRepository()).thenReturn(usagerRepository);
        when(usagerRepository.findByEmail(anyString())).thenReturn(user);
        when(daoFactory.getCommentRepository()).thenReturn(commentRepository);
        when(commentRepository.save(any())).thenReturn(null);
        
        // WHEN
        Comment result = classUnderTest.addComment(comment);
        
        // THEN
        assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo(comment);
    }
    
    @Test
    @DisplayName("As a system, I want verify that the save method is called, When adding a comment in the database")
    void givenAComment_whenTryingToSaveTheCommentInTheDatabase_thenTheSaveMethodIsCalled () {
        // GIVEN
        when(daoFactory.getUsagerRepository()).thenReturn(usagerRepository);
        when(usagerRepository.findByEmail(anyString())).thenReturn(user);
        when(daoFactory.getCommentRepository()).thenReturn(commentRepository);
        when(commentRepository.save(any())).thenReturn(null);
        
        // WHEN
        classUnderTest.addComment(comment);
        
        // THEN
        verify(commentRepository, times(1)).save(comment);
    }
    
    @Test
    @DisplayName("As a system, I want to call the method to get a list of comment in the repository, When trying to " +
                 "get the list of comment of a book")
    void givenABookReference_whenTryingToGetItsComments_thenItCallTheAssociatedMethodInRepository () {
        // GIVEN
        Comment comment = new Comment();
        List<Comment> comments = Collections.singletonList(comment);
        
        when(daoFactory.getCommentRepository()).thenReturn(commentRepository);
        when(commentRepository.findByBookReference(anyString())).thenReturn(comments);
        
        // WHEN
        classUnderTest.getCommentsFor("reference");
        
        // THEN
        verify(commentRepository, times(1)).findByBookReference("reference");
    }
    
}