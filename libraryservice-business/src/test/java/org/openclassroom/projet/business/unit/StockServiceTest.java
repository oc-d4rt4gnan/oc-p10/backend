package org.openclassroom.projet.business.unit;


import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openclassroom.projet.business.services.contract.StockService;
import org.openclassroom.projet.business.services.impl.StockServiceImpl;
import org.openclassroom.projet.consumer.DaoFactory;
import org.openclassroom.projet.consumer.repository.StockRepository;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Stock;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


/**
 * Test class for the {@link Stock stock} management service.
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 05/09/2021
 */
@ExtendWith(MockitoExtension.class)
class StockServiceTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    @InjectMocks
    private static StockService classUnderTest;
    
    @Mock
    private DaoFactory      daoFactory;
    @Mock
    private StockRepository stockRepository;
    
    private Book book;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new StockServiceImpl();
    }
    
    @BeforeEach
    void initializeVariable () {
        book = new Book();
        book.setReference("reference");
    
        when(daoFactory.getStockRepository()).thenReturn(stockRepository);
    }
    
    @Test
    @DisplayName("As a system, I want to get the available quantity, When I provide a book reference")
    void givenAnAvailableBook_whenGettingAvailableQuantityBook_thenItReturnTheQuantityAvailable () {
        // GIVEN
        Stock stock1 = new Stock();
        stock1.setQuantity(3);
        stock1.setQuantityBorrowed(2);
    
        Stock stock2 = new Stock();
        stock2.setQuantity(16);
        stock2.setQuantityBorrowed(15);
    
        List<Stock> stocks = Arrays.asList(stock1, stock2);
    
        when(stockRepository.findAllByStockIdBookReference(anyString())).thenReturn(stocks);
        
        int expected = 2;
        
        // WHEN
        int result = classUnderTest.getAvailableQuantityBook(book);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    @DisplayName("As a system, I want to get an available quantity of zero, When there is no stock available for the " +
                 "book reference")
    void givenAnUnavailableBook_whenGettingAvailableQuantityBook_thenItReturnZero () {
        // GIVEN
        Stock stock1 = new Stock();
        stock1.setQuantity(3);
        stock1.setQuantityBorrowed(3);
        
        Stock stock2 = new Stock();
        stock2.setQuantity(16);
        stock2.setQuantityBorrowed(16);
        
        List<Stock> stocks = Arrays.asList(stock1, stock2);
        
        when(stockRepository.findAllByStockIdBookReference(anyString())).thenReturn(stocks);
        
        int expected = 0;
        
        // WHEN
        int result = classUnderTest.getAvailableQuantityBook(book);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    @DisplayName("As a system, I want to get an available quantity of zero, When I provide a non-existent reference")
    void givenANonExistentBook_whenGettingAvailableQuantityBook_thenItReturnZero () {
        // GIVEN
        when(stockRepository.findAllByStockIdBookReference(anyString())).thenReturn(null);
        
        int expected = 0;
        
        // WHEN
        int result = classUnderTest.getAvailableQuantityBook(book);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    @DisplayName("As a system, I want to increase the amount of book borrowed, When I borrow another one.")
    void givenANewBookToBorrow_whenIWantToAddItToTheBorrowedStock_thenItCallTheSetterOfQuantityBorrowedInStockObject () {
        // GIVEN
        Stock stock = new Stock();
        stock.setQuantityBorrowed(1);
        
        Stock spy = Mockito.spy(stock);
        
        when(stockRepository.findByStockId_LibaryNumberRefAndStockId_BookReference(anyInt(), anyString())).thenReturn(
                spy);
        when(stockRepository.save(any())).thenReturn(null);
        
        // WHEN
        classUnderTest.updateStock(1, book.getReference(), 1);
        
        // THEN
        verify(spy, times(1)).setQuantityBorrowed(2);
    }
    
    @Nested
    class getStockForBook {
        
        @Test
        @DisplayName("As a system, I want to retrieve a book, When I try to get it by its reference.")
        void givenAnExistingBookInALibrary_whenTryingToGetItByItsReference_thenItReturned () {
            // GIVEN
            Stock expected = new Stock();
            expected.setQuantity(1);
            expected.setQuantityBorrowed(1);
            
            when(stockRepository.findById(any())).thenReturn(Optional.of(expected));
            
            // WHEN
            Stock result = classUnderTest.getStockForBook(1, book.getReference());
            
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo(expected);
        }
        
        @Test
        @DisplayName("As a system, I want to get null value, When I try to get a non-existing book.")
        void givenABookNonExistingInALibrary_whenTryingToGetItByItsReference_thenItReturnNull () {
            // GIVEN
            when(stockRepository.findById(any())).thenReturn(Optional.empty());
            
            // WHEN
            Stock result = classUnderTest.getStockForBook(1, book.getReference());
            
            // THEN
            assertThat(result).isNull();
        }
        
    }

}
