package org.openclassroom.projet.business.unit;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openclassroom.projet.business.services.contract.BookService;
import org.openclassroom.projet.business.services.contract.LibraryService;
import org.openclassroom.projet.business.services.impl.BookServiceImpl;
import org.openclassroom.projet.business.services.impl.LibraryServiceImpl;
import org.openclassroom.projet.consumer.DaoFactory;
import org.openclassroom.projet.consumer.repository.BookRepository;
import org.openclassroom.projet.consumer.repository.LibraryRepository;
import org.openclassroom.projet.consumer.repository.StockRepository;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.library.Stock;
import org.openclassroom.projet.model.database.library.StockId;
import org.openclassroom.projet.model.enums.FunctionalExceptionEnum;
import org.openclassroom.projet.technical.exceptions.FunctionalException;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


/**
 * Test class for service {@link BookService}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 19-09-2021
 */
@ExtendWith(MockitoExtension.class)
class BookServiceTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    @InjectMocks
    private static BookService classUnderTest;
    
    @Mock
    private DaoFactory     daoFactory;
    @Mock
    private BookRepository bookRepository;
    
    private Book book;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void initialize () {
        classUnderTest = new BookServiceImpl();
    }
    
    @BeforeEach
    void initVariable () {
        book = new Book();
        book.setReference("reference");
        book.setSynopsis("synopsis");
        book.setPublisher("publisher");
        book.setMark(3);
        book.setLanguage("FR");
        book.setCategory("category");
        book.setAuthor("author");
        book.setTitle("title");
    }
    
    @Test
    @DisplayName("As a system, I want to retrieve the list of all books, When I try to get them.")
    void givenAnEmptyKeyword_whenTryingToGetTheListOfAllBooks_thenItReturnsTheRightListOfBooks () {
        // GIVEN
        String keyword = "";
        
        List<Book> expected = Collections.singletonList(book);
        
        when(daoFactory.getBookRepository()).thenReturn(bookRepository);
        when(bookRepository.findAll()).thenReturn(expected);
        
        // WHEN
        List<Book> result = classUnderTest.getBooks(keyword);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    @DisplayName("As a system, I want to retrieve a complete list of books, When I try to get this list y a keyword.")
    void givenAKeyword_whenTryingToGetTheListOfBookByKeyword_thenItReturnsTheRightListOfBooks () {
        // GIVEN
        String keyword = "e";
    
        Book book2 = new Book();
        book2.setReference("reference2");
        book2.setSynopsis("synopsis2");
        book2.setPublisher("publisher2");
        book2.setMark(3);
        book2.setLanguage("FR2");
        book2.setCategory("category2");
        book2.setAuthor("author2");
        book2.setTitle("title2");
        
        List<Book> expected = Arrays.asList(book, book2);
        
        when(daoFactory.getBookRepository()).thenReturn(bookRepository);
        when(bookRepository.findBooksByReferenceIgnoreCaseContaining(keyword)).thenReturn(expected);
        when(bookRepository.findBooksByTitleIgnoreCaseContaining(keyword)).thenReturn(expected);
        when(bookRepository.findBooksByAuthorIgnoreCaseContaining(keyword)).thenReturn(new ArrayList<>());
        when(bookRepository.findBooksByCategoryIgnoreCaseContaining(keyword)).thenReturn(expected);
        when(bookRepository.findBooksByPublisherIgnoreCaseContaining(keyword)).thenReturn(expected);
        when(bookRepository.findBooksByLanguageIgnoreCaseContaining(keyword)).thenReturn(new ArrayList<>());
        
        // WHEN
        List<Book> result = classUnderTest.getBooks(keyword);
        
        // THEN
        assertThat(result).containsExactlyInAnyOrderElementsOf(expected);
    }
}