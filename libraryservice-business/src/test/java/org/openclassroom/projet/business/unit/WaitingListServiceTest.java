package org.openclassroom.projet.business.unit;


import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openclassroom.projet.business.services.contract.WaitingListService;
import org.openclassroom.projet.business.services.impl.WaitingListServiceImpl;
import org.openclassroom.projet.consumer.DaoFactory;
import org.openclassroom.projet.consumer.repository.StockRepository;
import org.openclassroom.projet.consumer.repository.UsagerRepository;
import org.openclassroom.projet.consumer.repository.WaitingListRepository;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Stock;
import org.openclassroom.projet.model.database.service.WaitingList;
import org.openclassroom.projet.model.database.service.WaitingListId;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.model.enums.FunctionalExceptionEnum;
import org.openclassroom.projet.technical.exceptions.FunctionalException;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WaitingListServiceTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    @InjectMocks
    private static WaitingListServiceImpl waitingListService;
    
    @Mock
    private DaoFactory            daoFactory;
    @Mock
    private WaitingListRepository waitingListRepository;
    @Mock
    private StockRepository       stockRepository;
    @Mock
    private UsagerRepository      userRepository;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    public static void initialize () {
        waitingListService = new WaitingListServiceImpl();
    }
    
    @BeforeEach
    public void mocks () {
        when(daoFactory.getWaitingListRepository()).thenReturn(waitingListRepository);
    }
    
    // ==================================================== ADD ========================================================
    
    @Test
    @Tag("Add")
    @DisplayName("As a user, when I make a reservation for a book, I want it to be recorded in the database")
    public void givenABookReferenceAndAUserId_whenAddingReservation_thenTheReservationIsSaved ()
    throws FunctionalException {
        // GIVEN
        Usager user = new Usager();
        user.setId(1);
        user.setEmail("nom.prenom@email.fr");
    
        Book book = new Book();
        book.setReference("d8f6e2be-ce1a-42fc-8eb5-5a414dd6b2fc");
    
        WaitingList waitingList = new WaitingList(book, user);
    
        WaitingListService spyService = Mockito.spy(waitingListService);
        doNothing().when(spyService).isCurrentlyBooked(any(), any());
        doNothing().when(spyService).isWaitingListForBookTooLong(any());
        when(daoFactory.getUsagerRepository()).thenReturn(userRepository);
        when(userRepository.findByEmail(anyString())).thenReturn(user);
        when(waitingListRepository.save(any(WaitingList.class))).thenReturn(waitingList);
    
        // WHEN
        WaitingList returnedReservation = spyService.addReservation(book, user);
    
        // THEN
        assertThat(returnedReservation.getWaitingListId()).usingRecursiveComparison().isEqualTo(waitingList
                                                                                                        .getWaitingListId());
    }
    
    @Test
    @Tag("Add")
    @DisplayName("As a user, when I make a reservation for a book, I want it to be recorded in the database")
    public void givenABookCurrentlyBookedByUser_whenCheckIfBookIsCurrentlyBooked_thenTheReservationIsBlocked () {
        // GIVEN
        Usager user = new Usager();
        user.setId(1);
        
        Book book = new Book();
        book.setReference("d8f6e2be-ce1a-42fc-8eb5-5a414dd6b2fc");
        
        when(waitingListRepository
                     .existsWaitingListByWaitingListIdBookReferenceAndWaitingListIdUsagerId(anyString(), anyInt()))
                .thenReturn(true);
        
        // THEN
        assertThatThrownBy(() -> {
            // WHEN
            waitingListService.isCurrentlyBooked(book, user);
        }).isInstanceOf(FunctionalException.class).hasFieldOrPropertyWithValue("type", FunctionalExceptionEnum.CURRENTLY_BOOKED);
    }
    
    @Test
    @Tag("Add")
    @DisplayName("As a user, when I make a reservation for a book, I want it to be recorded in the database")
    public void givenABookReferenceWithWaitingList2TimesOfTotalNumber_whenCheckIfWaitingListForBookIsTooLong_thenTheReservationIsBlocked () {
        // GIVEN
        Usager user = new Usager();
        user.setId(1);
        
        Book book = new Book();
        book.setReference("d8f6e2be-ce1a-42fc-8eb5-5a414dd6b2fc");
    
        List<Stock> stocks = new ArrayList<>();
        Stock stock1 = new Stock();
        Stock stock2 = new Stock();
        
        stock1.setQuantity(4);
        stock2.setQuantity(5);
        
        stocks.add(stock1);
        stocks.add(stock2);
    
        when(waitingListRepository.countByWaitingListIdBookReference(anyString())).thenReturn(17);
        when(daoFactory.getStockRepository()).thenReturn(stockRepository);
        when(stockRepository.findAllByStockIdBookReference(anyString())).thenReturn(stocks);
        
        // THEN
        assertThatThrownBy(() -> {
            // WHEN
            waitingListService.isWaitingListForBookTooLong(book);
        }).isInstanceOf(FunctionalException.class).hasFieldOrPropertyWithValue("type", FunctionalExceptionEnum.WAITINGLIST_TOO_LONG);
    }
    
    // ================================================= DELETE ========================================================
    
    @Test
    @Tag("Delete")
    @DisplayName("As a system, When a user gets the book he reserved, I want him to be removed from the book waiting list.")
    public void givenAUserAndBook_whenTheUserGetTheBook_thenTheUserIsRemovedFromTheWaitingList () {
        // GIVEN
        Usager user = new Usager();
        user.setId(1);
        
        Usager otherUser = new Usager();
        otherUser.setId(2);
    
        Book book = new Book();
        book.setReference("d8f6e2be-ce1a-42fc-8eb5-5a414dd6b2fc");
        
        WaitingList notExpectedElement = new WaitingList();
        WaitingListId id  = new WaitingListId();
        id.setUsager(user);
        id.setBook(book);
        notExpectedElement.setWaitingListId(id);
    
        WaitingList expectedElement = new WaitingList();
        WaitingListId id2 = new WaitingListId();
        id2.setUsager(otherUser);
        id2.setBook(book);
        expectedElement.setWaitingListId(id2);
    
        List<WaitingList> databaseWaitingList = new ArrayList<>();
        databaseWaitingList.add(notExpectedElement);
        databaseWaitingList.add(expectedElement);
    
    
        when(waitingListRepository.findWaitingListsByWaitingListIdBookReference(anyString())).thenReturn(
                databaseWaitingList);
        when(waitingListRepository.saveAllAndFlush(any())).thenReturn(null);
        
        // WHEN
        List<WaitingList> returnedWaitingList = waitingListService.removeReservation(user, book);
        
        // THEN
        assertThat(returnedWaitingList).doesNotContain(notExpectedElement).usingRecursiveComparison();
    }
    
    @Test
    @Tag("Delete")
    @DisplayName("As a system, When , I want ")
    public void given_when_then () {
        // GIVEN
        Usager user = new Usager();
    
        Book book = new Book();
        book.setReference("d8f6e2be-ce1a-42fc-8eb5-5a414dd6b2fc");
    
        WaitingListId id  = new WaitingListId();
        id.setBook(book);
        
        WaitingList element1 = new WaitingList();
        user.setId(1);
        id.setUsager(user);
        element1.setWaitingListId(id);
        element1.setReservationDate(LocalDateTime.of(2019, Month.AUGUST, 10, 8, 10));
    
        WaitingList element2 = new WaitingList();
        user.setId(2);
        id.setUsager(user);
        element2.setWaitingListId(id);
        element2.setReservationDate(LocalDateTime.of(2021, Month.MARCH, 24, 10, 8));
    
        List<WaitingList> expected = Collections.singletonList(element2);
        List<WaitingList> waitingList = Arrays.asList(element1, element2);
        
        when(waitingListRepository.findWaitingListsByWaitingListIdBookReference(anyString())).thenReturn(waitingList);
        when(waitingListRepository.saveAllAndFlush(any())).thenReturn(null);
        
        // WHEN
        List<WaitingList> result = waitingListService.removeFirstReservation(book);
        
        // THEN
        assertThat(result).isEqualTo(expected).usingRecursiveComparison();
    }
    
    // ================================================= VERIFY ========================================================
    
    @Test
    @Tag("Verify")
    @DisplayName(
            "As a system, When I check if a user is within the 48h time limit, I want to return true in case the " +
            "time limit is not exceeded")
    public void givenAUserWhoIsStillOnSchedule_whenCheckIsUnderDelay_thenReturnTrue () throws FunctionalException {
        // GIVEN
        Usager user = new Usager();
        user.setId(1);
        
        Book book = new Book();
        book.setReference("d8f6e2be-ce1a-42fc-8eb5-5a414dd6b2fc");
        
        WaitingListId id = new WaitingListId();
        id.setBook(book);
        id.setUsager(user);
        
        WaitingList element = new WaitingList();
        element.setWaitingListId(id);
        element.setEmailSent(true);
        element.setSendingMailDate(LocalDateTime.now().minusHours(24));
        
        List<WaitingList> waitingList = Collections.singletonList(element);
        
        when(waitingListRepository.findWaitingListsByWaitingListIdBookReference(anyString())).thenReturn(waitingList);
        
        // WHEN
        boolean result = waitingListService.checkIsUnderDelay(book);
        
        // THEN
        assertThat(result).isTrue();
    }
    
    @Test
    @Tag("Verify")
    @DisplayName(
            "As a system, When I check if a user is within the 48h time limit, I want to return false in case the " +
            "time limit is exceeded")
    public void givenAUserWhoHasExceededTheTimeLimit_whenCheckIsUnderDelay_thenReturnFalse ()
    throws FunctionalException {
        // GIVEN
        Usager user = new Usager();
        user.setId(1);
        
        Book book = new Book();
        book.setReference("d8f6e2be-ce1a-42fc-8eb5-5a414dd6b2fc");
        
        WaitingListId id = new WaitingListId();
        id.setBook(book);
        id.setUsager(user);
        
        WaitingList element = new WaitingList();
        element.setWaitingListId(id);
        element.setEmailSent(true);
        element.setSendingMailDate(LocalDateTime.now().minusHours(49));
        
        List<WaitingList> waitingList = Collections.singletonList(element);
        
        when(waitingListRepository.findWaitingListsByWaitingListIdBookReference(anyString())).thenReturn(waitingList);
        
        // WHEN
        boolean result = waitingListService.checkIsUnderDelay(book);
        
        // THEN
        assertThat(result).isFalse();
    }
    
    @Test
    @Tag("Verify")
    @DisplayName(
            "As a system, When I check if a user is within the 48h time limit, I want to raise an exception when no " +
            "mail has been sent")
    public void givenBookWithNoEmailWasSentToTheFirstUserOfWaitingList_whenCheckIsUnderDelay_thenThrowFunctionalException () {
        // GIVEN
        Usager user = new Usager();
        user.setId(1);
        
        Book book = new Book();
        book.setReference("d8f6e2be-ce1a-42fc-8eb5-5a414dd6b2fc");
        
        WaitingListId id = new WaitingListId();
        id.setBook(book);
        id.setUsager(user);
        
        WaitingList element = new WaitingList();
        element.setWaitingListId(id);
        element.setEmailSent(false);
        
        List<WaitingList> waitingList = Collections.singletonList(element);
        
        when(waitingListRepository.findWaitingListsByWaitingListIdBookReference(anyString())).thenReturn(waitingList);
        
        // THEN
        assertThatThrownBy(() -> {
            // WHEN
            waitingListService.checkIsUnderDelay(book);
        }).isInstanceOf(FunctionalException.class).hasFieldOrPropertyWithValue("type",
                                                                               FunctionalExceptionEnum.EMAIL_NOT_SENT);
    }
    
    // =================================================== GET =========================================================
    
    @Test
    @Tag("Get")
    @DisplayName(
            "As a system, when I want to retrieve books from the waiting list of a user who has them, then I have to " +
            "get it")
    public void givenAnUserId_whenGettingHisReservationList_thenReturnTheCorrectReservationList () {
        // GIVEN
        Usager user = new Usager();
        user.setId(1);
        
        Book book = new Book();
        book.setReference("d8f6e2be-ce1a-42fc-8eb5-5a414dd6b2fc");
        
        WaitingListId id = new WaitingListId();
        id.setBook(book);
        id.setUsager(user);
        
        WaitingList element = new WaitingList();
        element.setWaitingListId(id);
        element.setEmailSent(false);
        
        List<WaitingList> waitingList = Collections.singletonList(element);
        
        when(waitingListRepository.findWaitingListsByWaitingListIdUsagerId(anyInt())).thenReturn(waitingList);
        
        // WHEN
        List<WaitingList> result = waitingListService.getReservationFor(1);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(waitingList);
        
    }
    
    @Test
    @Tag("Get")
    @DisplayName(
            "As a system, when I want to retrieve the books on the waiting list of a user who does not have any, then" +
            " I have to get an empty list")
    public void givenAnUserIdWithNoReservation_whenGettingHisReservationList_thenReturnAnEmptyArrayList () {
        // GIVEN
        List<WaitingList> expected = new ArrayList<>();
        when(waitingListRepository.findWaitingListsByWaitingListIdUsagerId(anyInt())).thenReturn(null);
        
        // WHEN
        List<WaitingList> result = waitingListService.getReservationFor(1);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
}
