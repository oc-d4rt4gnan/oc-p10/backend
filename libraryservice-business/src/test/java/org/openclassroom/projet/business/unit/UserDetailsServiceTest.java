package org.openclassroom.projet.business.unit;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openclassroom.projet.business.services.impl.UserDetailsServiceImpl;
import org.openclassroom.projet.consumer.repository.UsagerRepository;
import org.openclassroom.projet.model.database.usager.Usager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


/**
 * Test class for the {@link UserDetailsService}.
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 25/09/2021
 */
@ExtendWith(MockitoExtension.class)
class UserDetailsServiceTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    @InjectMocks
    private static UserDetailsService classUnderTest;
    
    @Mock
    private UsagerRepository usagerRepository;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new UserDetailsServiceImpl();
    }
    
    @Test
    @DisplayName("As a system, I want to get the userDetails object, When I try to get a user by its username")
    void givenAValidUsername_whenTryToGetAUserByItsUsername_thenTheRightUserIsReturned () {
        // GIVEN
        String username = "email";
        
        Usager user = new Usager();
        user.setEmail(username);
        
        when(usagerRepository.findByEmail(anyString())).thenReturn(user);
        
        // WHEN
        UserDetails result = classUnderTest.loadUserByUsername(username);
        
        // THEN
        assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo((UserDetails) user);
    }
    
    @Test
    @DisplayName("As a system, I want it throw a UsernameNotFoundException, When I try to get a user by a " +
                 "non-existing username.")
    void givenANonExistingUsername_whenTryToGetAUserByItsUsername_thenItThrowAUsernameNotFoundException () {
        // GIVEN
        String username = "nonExistingUsername";
        
        when(usagerRepository.findByEmail(anyString())).thenReturn(null);
        
        //THEN
        assertThatThrownBy(() -> {
            // WHEN
            classUnderTest.loadUserByUsername(username);
        }).isInstanceOf(UsernameNotFoundException.class).hasMessage("No user present with mail : nonExistingUsername");
    }
    
}
