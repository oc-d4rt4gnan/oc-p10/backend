package org.openclassroom.projet.business.services.impl;


import org.openclassroom.projet.business.services.AbstractService;
import org.openclassroom.projet.business.services.contract.WaitingListService;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.library.Stock;
import org.openclassroom.projet.model.database.service.WaitingList;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.model.enums.FunctionalExceptionEnum;
import org.openclassroom.projet.technical.exceptions.FunctionalException;
import org.openclassroom.projet.technical.exceptions.TechnicalException;
import org.openclassroom.projet.technical.utils.camparators.SortWaitingListByDate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Service implementation of the business module for the object {@link WaitingList}.
 */
@Named("waitingListService")
@Service
public class WaitingListServiceImpl extends AbstractService implements WaitingListService {
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public WaitingList addReservation (Book book, Usager user) throws FunctionalException {
        isCurrentlyBooked(book, user);
        isWaitingListForBookTooLong(book);
        Usager dbUser = getDaoFactory().getUsagerRepository().findByEmail(user.getEmail());
        WaitingList waitingList = new WaitingList(book, dbUser);
        return getDaoFactory().getWaitingListRepository().save(waitingList);
    }
    
    @Override
    public boolean checkIsUnderDelay (Book book) throws FunctionalException {
        // Retrieve the waiting list of a book
        List<WaitingList> waitingList = getDaoFactory().getWaitingListRepository()
                                                       .findWaitingListsByWaitingListIdBookReference(book.getReference());
    
        // Sort the waiting list in chronological order
        waitingList.sort(new SortWaitingListByDate());
        
        //
        WaitingList firstPlace = waitingList.get(0);
        if(!firstPlace.isEmailSent()) {
            throw new FunctionalException(FunctionalExceptionEnum.EMAIL_NOT_SENT);
        }
    
        // Returns true if it has been less than 48 hours since the email was sent
        return firstPlace.getSendingMailDate().plusHours(48).isAfter(LocalDateTime.now());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<WaitingList> getReservationFor (int userID) {
        List<WaitingList> reservedBooksList = getDaoFactory().getWaitingListRepository()
                                                             .findWaitingListsByWaitingListIdUsagerId(userID);
        if (reservedBooksList != null && reservedBooksList.size() > 0) {
            return reservedBooksList;
        } else {
            return new ArrayList<>();
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void informsTheFirstUserInTheList(String bookReference, int libraryId) throws TechnicalException {
        // We get the waiting list of the book
        List<WaitingList> waitingList =
                getDaoFactory().getWaitingListRepository().findWaitingListsByWaitingListIdBookReference(bookReference);
        if (!waitingList.isEmpty()) {
            // We sort the list in chronological order
            waitingList.sort(new SortWaitingListByDate());
    
            // We recover the elements necessary to send the email
            WaitingList element = waitingList.get(0);
            Usager user = element.getWaitingListId().getUsager();
            Book book = element.getWaitingListId().getBook();
            Library library = getDaoFactory().getLibraryRepository().findByNumberRef(libraryId);
    
            // We send the email
            getMailService().sendMailOfNewAvailabilityOfBook(user, book, library);
    
            // We update the waiting list
            element.setEmailSent(true);
            element.setSendingMailDate(LocalDateTime.now());
            waitingList.set(0, element);
            getDaoFactory().getWaitingListRepository().saveAllAndFlush(waitingList);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void isCurrentlyBooked (Book book, Usager user) throws FunctionalException {
        String bookReference = book.getReference();
        int userId = user.getId();
        if (getDaoFactory().getWaitingListRepository()
                           .existsWaitingListByWaitingListIdBookReferenceAndWaitingListIdUsagerId(bookReference,
                                                                                                  userId)) {
            throw new FunctionalException(FunctionalExceptionEnum.CURRENTLY_BOOKED);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void isWaitingListForBookTooLong(Book book) throws FunctionalException {
        String bookReference = book.getReference();
        int waitingListSize =
                getDaoFactory().getWaitingListRepository().countByWaitingListIdBookReference(bookReference);
        List<Stock> stocks = getDaoFactory().getStockRepository().findAllByStockIdBookReference(bookReference);
        int quantityMax = stocks.stream().map(Stock::getQuantity).reduce(0, Integer::sum);
        if (waitingListSize + 1 >= quantityMax * 2) {
            throw new FunctionalException(FunctionalExceptionEnum.WAITINGLIST_TOO_LONG);
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<WaitingList> removeReservation(Usager user, Book book) {
    
        List<WaitingList> waitingList = getDaoFactory().getWaitingListRepository()
                                                       .findWaitingListsByWaitingListIdBookReference(book.getReference());
    
        waitingList = waitingList.stream().filter(element -> element.getWaitingListId().getUsager().getId() !=
                                                             user.getId()).collect(Collectors.toList());
    
        getDaoFactory().getWaitingListRepository().saveAllAndFlush(waitingList);
    
        return waitingList;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<WaitingList> removeFirstReservation(Book book) {
        // Retrieve the waiting list of a book
        List<WaitingList> waitingList = getDaoFactory().getWaitingListRepository()
                                                       .findWaitingListsByWaitingListIdBookReference(book.getReference());
        
        // Sort the waiting list in chronological order
        waitingList.sort(new SortWaitingListByDate());
        
        // Removes the oldest user from the list
        List<WaitingList> newList = new ArrayList<>();
        for (int i = 0; i < waitingList.size(); i++) {
            if (i != 0) {
                newList.add(waitingList.get(i));
            }
        }
        
        // Updates the list in the database and returns it
        getDaoFactory().getWaitingListRepository().saveAllAndFlush(newList);
        return newList;
    }
}
