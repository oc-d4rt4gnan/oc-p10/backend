package org.openclassroom.projet.business.services.impl;


import org.openclassroom.projet.business.services.AbstractService;
import org.openclassroom.projet.business.services.contract.LibraryService;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.library.Stock;
import org.openclassroom.projet.model.enums.FunctionalExceptionEnum;
import org.openclassroom.projet.technical.exceptions.FunctionalException;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Service implementation of the business module for the object {@link Library}.
 */
@Service
public class LibraryServiceImpl extends AbstractService implements LibraryService {
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<Library> getLibraries () {
        return getDaoFactory().getLibraryRepository().findAll();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Library getLibrary (int numberReference) {
        return getDaoFactory().getLibraryRepository().findByNumberRef(numberReference);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Library getLibraryWhereAreTheReturnedBook (Book book) throws FunctionalException {
        List<Stock> stocks = getDaoFactory().getStockRepository().findAllByStockIdBookReference(book.getReference());
        Library library = null;
        for (Stock stock : stocks) {
            int available = stock.getQuantity() - stock.getQuantityBorrowed();
            if (available > 0) {
                library = getDaoFactory().getLibraryRepository().findByNumberRef(stock.getStockId()
                                                                                      .getLibaryNumberRef());
            }
        }
        
        if (library != null) {
            return library;
        } else {
            throw new FunctionalException(FunctionalExceptionEnum.NO_BOOK_AVAILABLE);
        }
    }
    
}
