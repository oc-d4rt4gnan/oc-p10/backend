package org.openclassroom.projet.business.services.impl;


import org.openclassroom.projet.business.services.contract.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;


/**
 * Implementation of Spring's control inversion for the Business module.
 */
@Named("serviceFactory")
public class ServiceFactoryImpl implements ServiceFactory {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    @Autowired
    private BookService      bookService;
    
    @Autowired
    private BorrowingService borrowingService;
    
    @Autowired
    private CommentService   commentService;
    
    @Autowired
    private LibraryService   libraryService;
    
    @Autowired
    private StockService     stockService;
    
    @Autowired
    private UserService      userService;
    
    @Autowired
    private WaitingListService      waitingListService;
    
    
    // =================================================================================================================
    //                                                  GETTERS
    // =================================================================================================================
    
    /**
     * {@inheritDoc}
     */
    @Override
    public BookService getBookService () {
        return bookService;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public BorrowingService getBorrowingService () {
        return borrowingService;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public CommentService getCommentService () {
        return commentService;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public LibraryService getLibraryService () {
        return libraryService;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public StockService getStockService () {
        return stockService;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public UserService getUserService () {
        return userService;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public WaitingListService getWaitingListService () {
        return waitingListService;
    }
    
}
