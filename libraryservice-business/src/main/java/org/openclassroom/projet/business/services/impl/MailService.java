package org.openclassroom.projet.business.services.impl;


import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.technical.exceptions.TechnicalException;
import org.openclassroom.projet.technical.spring.properties.MailConfig;
import org.openclassroom.projet.technical.utils.mails.MailUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 * Service implementation of the business module for email sending methods.
 */
@Service
public class MailService {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    @Value("${client.url}")
    private String     clientUrl;
    @Value("${client.confirmemail.url}")
    private String     confirmEmailUrl;
    @Value("${client.newpassword.url}")
    private String     newPasswordUrl;
    private MailConfig mailConfig = new MailConfig();
    private MailUtils  mailUtils  = new MailUtils();
    
    
    // =================================================================================================================
    //                                               PUBLIC METHODS
    // =================================================================================================================
    
    /**
     * Sends a new account verification email.
     *
     * @param usager
     *         - The {@link Usager user} to whom the mail is to be sent.
     * @param newToken
     *         - The new token to replace the old one.
     *
     * @return If the email was indeed sent.
     */
    public boolean resendConfirmationEmail (Usager usager, String newToken) throws TechnicalException {
        return mailUtils.sendMailSMTP(usager.getEmail(),
                            "Resend : Confirm your account",
                            createVerificationEmailContent(newToken),
                            true);
    }
    
    /**
     * Sending an account verification email.
     *
     * @param usager
     *         - The {@link Usager user} to whom the mail is to be sent.
     * @param token
     *         - A string contained in the link sent by email to identify the account to be validated.
     */
    public void sendConfirmationEmail (Usager usager, String token) throws TechnicalException {
        mailUtils.sendMailSMTP(usager.getEmail(), "Confirm your account", createVerificationEmailContent(token), true);
    }
    
    /**
     * Sends an email reminder regarding an expired {@link org.openclassroom.projet.model.database.service.Borrowing
     * borrowing}.
     *
     * @param usager
     *         - The {@link Usager user} to whom the mail is to be sent.
     */
    public void sendReminderEmail (Usager usager) throws TechnicalException {
        String toAddress = usager.getEmail();
        String title = "Reminder : Borrowing expired !";
        String content
                = "Your borrowing has expired please extend it if it's not already done or return quickly your book !";
    
        mailUtils.sendMailSMTP(toAddress, title, content, true);
    }
    
    /**
     * Sending an email containing a link to change your password when you have forgotten it.
     *
     * @param usager
     *         - The {@link Usager user} to whom the mail is to be sent.
     * @param token
     *         - A string contained in the link sent by e-mail to identify the account whose password needs to be
     *         changed.
     *
     * @return If the email was indeed sent.
     */
    public boolean sendResetPasswordEmail (Usager usager, String token) throws TechnicalException {
        return mailUtils.sendMailSMTP(usager.getEmail(),
                                      "Forgotten password",
                                      createResetPasswordEmailContent(token),
                                      true);
    }
    
    public void sendMailOfNewAvailabilityOfBook (Usager user, Book book, Library library) throws TechnicalException {
        String template = mailConfig.getTemplate("book_available.html");
        String content = mailUtils.fillBookAvailabilityTemplate(template, user, book, library);
        String title = String.format("Your Book %s is available !", book.getTitle());
        mailUtils.sendMailSMTP(user.getEmail(), title, content, false);
    }
    
    
    // =================================================================================================================
    //                                              PRIVATE  METHODS
    // =================================================================================================================
    
    // Allows you to create the content of the account verification email by adding the personalized link containing the
    // identification token.
    private String createVerificationEmailContent (String token) {
        String lineBreak = "\n\n";
        String before = "Thanks for signing up on our service! You must follow this link to activate your account:";
        String after = "Have fun, and don't hesitate to contact us with your feedback." + lineBreak +
                       "The Library Team";
        
        return before + lineBreak + clientUrl + confirmEmailUrl + "?token=" + token + lineBreak + after;
    }
    
    // Allows you to create the content of the password reset email by adding the personalized link containing the
    // identification token.
    private String createResetPasswordEmailContent (String token) {
        String lineBreak = "\n\n";
        String before = "To reset your password, follow this link and change it:";
        String after = "Have fun, and don't hesitate to contact us with your feedback." + lineBreak +
                       "The Library Team";
        
        return before + lineBreak + clientUrl + newPasswordUrl + "?token=" + token + lineBreak + after;
    }
    
}
