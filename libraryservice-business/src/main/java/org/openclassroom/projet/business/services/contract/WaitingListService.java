package org.openclassroom.projet.business.services.contract;


import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Stock;
import org.openclassroom.projet.model.database.service.WaitingList;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.technical.exceptions.FunctionalException;
import org.openclassroom.projet.technical.exceptions.TechnicalException;

import java.util.List;


/**
 * Business module interface for the {@link WaitingList} object
 */
public interface WaitingListService {
    
    /**
     * Allows you to add a reservation for a {@link Usager user} to the {@link WaitingList waiting list} of a
     * {@link Book book}.
     *
     * @param book - The {@link Book book} to which we must add a reservation
     * @param user - The {@link Usager user} wishing to reserve the {@link Book book}
     *
     * @return {@link WaitingList The reservation}
     *
     * @throws FunctionalException : If the book is already on loan for this user
     */
    WaitingList addReservation (Book book, Usager user) throws FunctionalException;
    
    
    
    /**
     * Checks that the first {@link Usager user} on the {@link WaitingList waiting list} for a {@link Book book} has
     * not exceeded the 48-hour deadline
     *
     * @param book
     *         : The book containing the waiting list
     *
     * @return true if the user received the email less than 48 hours ago
     *
     * @throws FunctionalException
     *         if no email has been sent for this waiting list
     */
    boolean checkIsUnderDelay (Book book) throws FunctionalException;
    
    
    
    /**
     * Retrieves the {@link WaitingList list of reserved books} of an {@link Usager user}
     *
     * @param userID : The user identifier
     *
     * @return the list of reserved books
     */
    List<WaitingList> getReservationFor (int userID);
    
    
    
    /**
     * Allows you to send an email to the first {@link Usager user} on the {@link WaitingList waiting list} of the
     * returned {@link Book book} and to update the {@link WaitingList waiting list}.
     *
     * @param bookReference : The reference of the returned book
     * @param libraryId : The number of the library where the book was returned
     *
     * @throws TechnicalException if there is a problem sending emails or updating the waiting list
     */
    void informsTheFirstUserInTheList (String bookReference, int libraryId) throws TechnicalException;
    
    
    /**
     * Checks if the {@link Book book} is already on loan for this {@link Usager user}
     *
     * @param book - The {@link Book book} to which we must add a reservation
     * @param user - The {@link Usager user} wishing to reserve the {@link Book book}
     *
     * @throws FunctionalException : If the book is already on loan for this user
     */
    void isCurrentlyBooked (Book book, Usager user) throws FunctionalException;
    
    
    /**
     * Checks the size of the {@link WaitingList waiting list} against the size of the {@link Stock inventory}
     *
     * @param book - The {@link Book book} to which we must add a reservation
     *
     * @throws FunctionalException : If the waiting list is greater than 2 times the stock size
     */
    void isWaitingListForBookTooLong (Book book) throws FunctionalException;
    
    
    /**
     * Allows you to remove a {@link Usager user} from a {@link WaitingList waiting list}
     *
     * @param user : The user to be removed from the waiting list
     * @param book : The book containing the waiting list where is the user to be deleted
     *
     * @return the waiting list without the deleted user
     */
    List<WaitingList> removeReservation (Usager user, Book book);
    
    
    
    /**
     * Allows you to remove the first {@link Usager user} from a {@link WaitingList waiting list}
     *
     * @param book
     *         : The book containing the waiting list where is the first user to be deleted
     *
     * @return the waiting list without the deleted user
     */
    List<WaitingList> removeFirstReservation (Book book);
}
