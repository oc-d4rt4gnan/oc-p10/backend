package org.openclassroom.projet.business.services.contract;


import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.technical.exceptions.FunctionalException;

import java.util.List;


/**
 * Business module interface for the {@link Library} object
 */
public interface LibraryService {
    
    /**
     * Retrieves the list of {@link Library libraries} covered by the application.
     *
     * @return A list of {@link Library}
     */
    List<Library> getLibraries ();
    
    
    
    /**
     * Getter of the {@link Library} object by its reference number.
     *
     * @param numberReference
     *         - The library identifier.
     *
     * @return The {@link Library} associated with the number passed in parameter.
     */
    Library getLibrary (int numberReference);
    
    
    
    /**
     * Allows you to find the {@link Library library} where the {@link Book book} was returned
     *
     * @param book : the book wanted
     *
     * @return the library where the book is located
     *
     * @throws FunctionalException if the book is not available in any library
     */
    Library getLibraryWhereAreTheReturnedBook (Book book) throws FunctionalException;
    
}
