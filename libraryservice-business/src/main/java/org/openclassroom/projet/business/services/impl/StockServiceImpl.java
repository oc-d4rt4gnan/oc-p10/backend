package org.openclassroom.projet.business.services.impl;


import org.openclassroom.projet.business.services.AbstractService;
import org.openclassroom.projet.business.services.contract.StockService;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Stock;
import org.openclassroom.projet.model.database.library.StockId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


/**
 * Service implementation of the business module for the object {@link Stock}.
 */
@Service
public class StockServiceImpl extends AbstractService implements StockService {
    
    /**
     * {@inheritDoc}
     */
    @Override
    public int getAvailableQuantityBook (Book book) {
        Optional<List<Stock>> stocks = Optional.ofNullable(getDaoFactory().getStockRepository()
                                                                          .findAllByStockIdBookReference(book.getReference()));
        return stocks.map(stockList -> stockList.stream()
                                                .mapToInt(stock -> stock.getQuantity() - stock.getQuantityBorrowed())
                                                .reduce(0, Integer::sum)).orElse(0);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Stock getStockForBook (int libraryId, String bookReference) {
        Optional<Stock> stockOptional = getDaoFactory().getStockRepository().findById(new StockId(libraryId,
                                                                                                  bookReference));
        return stockOptional.orElse(null);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void updateStock (int libraryId, String referenceBook, int value) {
        Stock stock = getDaoFactory().getStockRepository().findByStockId_LibaryNumberRefAndStockId_BookReference(
                libraryId,
                referenceBook);
        stock.setQuantityBorrowed(stock.getQuantityBorrowed() + value);
        getDaoFactory().getStockRepository().save(stock);
    }
    
}
