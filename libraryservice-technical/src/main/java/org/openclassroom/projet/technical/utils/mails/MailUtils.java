package org.openclassroom.projet.technical.utils.mails;


import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.model.enums.TechnicalExceptionEnum;
import org.openclassroom.projet.technical.exceptions.TechnicalException;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;


/**
 * Utility class providing help for sending mail
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @version 1.0
 * @see "org.openclassroom.projet.business.services.impl.MailService"
 */
@Component
public class MailUtils {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private Properties customMailProperties;
    
    
    // =================================================================================================================
    //                                                CONSTRUCTORS
    // =================================================================================================================
    
    public MailUtils () {
    }
    
    public MailUtils (Properties customMailProperties) {
        this.customMailProperties = customMailProperties;
    }
    
    
    // =================================================================================================================
    //                                               PUBLIC METHODS
    // =================================================================================================================
    
    /**
     * The basic function that allows you to send mail via JavaMail.
     *
     * @param toAddress
     *         : The recipient's email address
     * @param title
     *         : The subject of the email
     * @param content
     *         : The content of the email
     * @param debug
     *         : Display more logs if true
     *
     * @return true if the email is sent
     *
     * @throws TechnicalException
     *         if the email is not sent
     */
    public boolean sendMailSMTP (String toAddress, String title, String content, boolean debug)
    throws TechnicalException {
        Properties props = new Properties();
    
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.smtp.host", "mail.gmx.com");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.user", "mailing.libraryservice@gmx.fr");
        props.setProperty("mail.password", "LibraryServicePassword");
    
        // Unsecured method only for development - Add certificate for production
        props.setProperty("mail.smtp.ssl.trust", "mail.gmx.com");
        
        String username = props.getProperty("mail.user");
        String password = props.getProperty("mail.password");
        
        try {
            Session session = Session.getInstance(props);
            
            Message message = new MimeMessage(session);
            // Header
            message.addHeader("Content-type", "text/HTML; charset=UTF-8");
            message.addHeader("format", "flowed");
            message.addHeader("Content-Transfer-Encoding", "8bit");
            // Sender
            message.setFrom(new InternetAddress(username));
            // Receiver
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
            // Content
            message.setSubject(title);
            message.setText(content);
            message.setSentDate(new Date());
            
            // Debug and sending
            session.setDebug(debug);
            Transport.send(message, username, password);
        } catch (Exception exception) {
            throw new TechnicalException(TechnicalExceptionEnum.CANT_SEND_EMAIL);
        }
        
        return true;
    }
    
    /**
     * Allows you to change the tags of the availability mail template of a {@link Book book}, by variable text.
     *
     * @param template
     *         : Book availability email template
     * @param user
     *         : The user receiving the email
     * @param book
     *         : The book concerned
     * @param library
     *         : The library to which the book was returned
     *
     * @return The template filled with the appropriate information
     */
    public String fillBookAvailabilityTemplate (String template, Usager user, Book book, Library library) {
        String content = template.replace("[PRENOM]", user.getFirstName());
        content = content.replace("[TITRE]", book.getTitle());
        content = content.replace("[NOM_BIBLIOTHEQUE]", library.getName());
        content = content.replace("[ADRESSE]", library.getAddress());
        
        return content;
    }
    
}
