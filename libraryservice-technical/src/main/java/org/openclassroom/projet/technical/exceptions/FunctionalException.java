package org.openclassroom.projet.technical.exceptions;


import org.openclassroom.projet.model.enums.FunctionalExceptionEnum;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;


/**
 * Bean functional exception
 */
@Component
public class FunctionalException extends Exception {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private ApplicationContext      context;
    private Environment             properties;
    private String                  message;
    private FunctionalExceptionEnum type;
    
    
    // =================================================================================================================
    //                                                CONSTRUCTORS
    // =================================================================================================================
    
    public FunctionalException () {
    }
    
    public FunctionalException (FunctionalExceptionEnum type) {
        this.type       = type;
        this.context    = new GenericApplicationContext();
        this.properties = context.getEnvironment();
        this.message    = properties.getProperty(type.getCode());
    }
    
    
    // =================================================================================================================
    //                                              GETTERS / SETTERS
    // =================================================================================================================
    
    public FunctionalExceptionEnum getType () {
        return type;
    }
    
    @Override
    public String getMessage () {
        return message;
    }
    
}
