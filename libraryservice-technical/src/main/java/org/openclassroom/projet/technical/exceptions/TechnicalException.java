package org.openclassroom.projet.technical.exceptions;


import org.openclassroom.projet.model.enums.TechnicalExceptionEnum;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;


/**
 * Bean technical exception
 */
@Component
public class TechnicalException extends Exception {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private ApplicationContext     context;
    private Environment            properties;
    private String                 message;
    private TechnicalExceptionEnum type;
    
    
    // =================================================================================================================
    //                                                CONSTRUCTORS
    // =================================================================================================================
    
    public TechnicalException () {
    }
    
    public TechnicalException (TechnicalExceptionEnum type) {
        this.type       = type;
        this.context    = new GenericApplicationContext();
        this.properties = context.getEnvironment();
        this.message    = properties.getProperty(type.getCode());
    }
    
    
    // =================================================================================================================
    //                                              GETTERS / SETTERS
    // =================================================================================================================
    
    public TechnicalExceptionEnum getType () {
        return type;
    }
    
    @Override
    public String getMessage () {
        return message;
    }
    
}
