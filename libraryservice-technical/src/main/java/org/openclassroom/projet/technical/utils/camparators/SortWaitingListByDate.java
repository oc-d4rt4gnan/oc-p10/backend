package org.openclassroom.projet.technical.utils.camparators;


import org.openclassroom.projet.model.database.service.WaitingList;

import java.time.LocalDateTime;
import java.util.Comparator;


/**
 * {@link WaitingList Waiting list} {@link Comparator comparator} class by {@link LocalDateTime date} of reservation
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 28-08-2021
 */
public class SortWaitingListByDate implements Comparator<WaitingList> {
    
    @Override
    public int compare (WaitingList element1, WaitingList element2) {
        return element1.getReservationDate().compareTo(element2.getReservationDate());
    }
    
}
