package org.openclassroom.projet.technical.spring.properties;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * JavaMail sending configuration class.
 */
@Configuration
@PropertySource("classpath:mail.properties")
public class MailConfig {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    @Autowired
    private Environment env;
    
    
    // =================================================================================================================
    //                                                   BEANS
    // =================================================================================================================
    
    /**
     * Allows you to configure the parameters necessary to send an email with JavaMail such as the identifiers, the
     * port, ...
     * All configurations are done in the associated .properties file.
     *
     * @return A {@link Properties} object containing the configurations made as properties.
     */
    @Bean
    public Properties customProperties () {
        Properties props = new Properties();
        
        props.setProperty("mail.transport.protocol", env.getProperty("mail.protocol"));
        props.setProperty("mail.smtp.host", env.getProperty("mail.host"));
        props.setProperty("mail.smtp.auth", env.getProperty("mail.auth"));
        props.setProperty("mail.smtp.port", env.getProperty("mail.port"));
        props.setProperty("mail.smtp.socketFactory.port", env.getProperty("mail.port"));
        props.setProperty("mail.smtp.socketFactory.class", env.getProperty("mail.socket"));
        props.setProperty("mail.user", env.getProperty("mail.user"));
        props.setProperty("mail.password", env.getProperty("mail.password"));
        
        // Unsecured method only for development - Add certificate for production
        props.setProperty("mail.smtp.ssl.trust", env.getProperty("mail.host"));
        
        return props;
    }
    
    
    // =================================================================================================================
    //                                                   TEMPLATES
    // =================================================================================================================
    
    public String getTemplate(String name) {
        // load from the the module where a given class is
        try {
            InputStream is = Class.forName("org.openclassroom.projet.technical.spring.properties.MailConfig")
                                  .getResourceAsStream("/template/" + name);
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            int result = bis.read();
            while(result != -1) {
                byte b = (byte)result;
                buf.write(b);
                result = bis.read();
            }
            
            return buf.toString();
        } catch (ClassNotFoundException | IOException e) {
            throw new RuntimeException(e);
        }
    }
    
}
