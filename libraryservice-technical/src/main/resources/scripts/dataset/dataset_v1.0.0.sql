/* 
	ADDING BOOKS
 */
insert into book 
	(reference, image_url, title, author, synopsis, category, publisher, language, mark)
values
	/* Livres en Français */
	('DWVKBC', 'https://m.media-amazon.com/images/I/71YzKee8HnL._AC_UY218_ML3_.jpg', 'Le consentement', 'Vanessa Springora', 
	'Au milieu des années 80, élevée par une mère divorcée, V. comble par la lecture le vide laissé par un père aux abonnés absents. À treize ans, dans un dîner, elle rencontre G., un écrivain dont elle ignore la réputation sulfureuse. Dès le premier regard, elle est happée par le charisme de cet homme de cinquante ans aux faux airs de bonze, par ses œillades énamourées et l’attention qu’il lui porte. Plus tard, elle reçoit une lettre où il lui déclare son besoin «  impérieux  » de la revoir. Omniprésent, passionné, G. parvient à la rassurer : il l’aime et ne lui fera aucun mal. Alors qu’elle vient d’avoir quatorze ans, V. s’offre à lui corps et âme. Les menaces de la brigade des mineurs renforcent cette idylle dangereusement romanesque. Mais la désillusion est terrible quand V. comprend que G. collectionne depuis toujours les amours avec des adolescentes, et pratique le tourisme sexuel dans des pays où les mineurs sont vulnérables. Derrière les apparences flatteuses de l’homme de lettres, se cache un prédateur, couvert par une partie du milieu littéraire. V. tente de s’arracher à l’emprise qu’il exerce sur elle, tandis qu’il s’apprête à raconter leur histoire dans un roman. Après leur rupture, le calvaire continue, car l’écrivain ne cesse de réactiver la souffrance de V. à coup de publications et de harcèlement.
	' || chr(10) || '«  Depuis tant d’années, mes rêves sont peuplés de meurtres et de vengeance. Jusqu’au jour où la solution se présente enfin, là, sous mes yeux, comme une évidence  : prendre le chasseur à son propre piège, l’enfermer dans un livre  », écrit-elle en préambule de ce récit libérateur.
	' || chr(10) || 'Plus de trente ans après les faits, Vanessa Springora livre ce texte fulgurant, d’une sidérante lucidité, écrit dans une langue remarquable. Elle y dépeint un processus de manipulation psychique implacable et l’ambiguïté effrayante dans laquelle est placée la victime consentante, amoureuse. Mais au-delà de son histoire individuelle, elle questionne aussi les dérives d’une époque, et la complaisance d’un milieu aveuglé par le talent et la célébrité.', 
	'Littérature Française', 'Grasset', 'Français', 0),
	
	('F4BOEX', 'https://m.media-amazon.com/images/I/61t729I1VPL._AC_UY218_ML3_.jpg', 'Tous les hommes n''habitent pas le monde de la même façon', 'Jean-Paul Dubois', 
	'Cela fait deux ans que Paul Hansen purge sa peine dans la prison provinciale de Montréal. Il y partage une cellule avec Horton, un Hells Angel incarcéré pour meurtre.
	' || chr(10) || 'Retour en arrière: Hansen est superintendant a L''Excelsior, une résidence où il déploie ses talents de concierge, de gardien, de factotum, et – plus encore – de réparateur des âmes et consolateur des affligés. Lorsqu''il n''est pas occupé à venir en aide aux habitants de L''Excelsior ou à entretenir les bâtiments, il rejoint Winona, sa compagne. Aux commandes de son aéroplane, elle l''emmène en plein ciel, au-dessus des nuages. Mais bientôt tout change. Un nouveau gérant arrive à L''Excelsior, des conflits éclatent. Et l''inévitable se produit.
	' || chr(10) || 'Une église ensablée dans les dunes d''une plage, une mine d''amiante à ciel ouvert, les méandres d''un fleuve couleur argent, les ondes sonores d''un orgue composent les paysages variés où se déroule ce roman.
	' || chr(10) || 'Histoire d''une vie, Tous les hommes n''habitent pas le monde de la même façon est l''un des plus beaux livres de Jean-Paul Dubois. On y découvre un écrivain qu''animent le sens aigu de la fraternité et un sentiment de révolte à l''égard de toutes les formes d''injustice.', 
	'Littérature Française', 'L''Olivier', 'Français', 4),
	
	('UEGU93', 'https://m.media-amazon.com/images/I/713Oat7+fpL._AC_UY218_ML3_.jpg', 'La panthère des neiges', 'Sylvain Tesson', 
	'"Tesson ! Je poursuis une bête depuis six ans, dit Munier. Elle se cache sur les plateaux du Tibet. J''y retourne cet hiver, je t''emmène. Qui est-ce ? La panthère des neiges. Une ombre magique ! Je pensais qu''elle avait disparu, dis-je. C''est ce qu''elle fait croire".', 
	'Récits de voyages', 'Gallimard', 'Français', 5),
	
	('D6FDSR', 'https://m.media-amazon.com/images/I/91Yq339lSyL._AC_UY218_ML3_.jpg', 'Astérix - La fille de Vercingétorix', 'René Goscinny, Albert Uderzo', 
	'Effervescence et chamboulements en perspective. La fille du célèbre chef gaulois Vercingétorix, traquée par les Romains, trouve refuge dans le village des irréductibles gaulois, seul endroit dans la Gaule occupée à pouvoir assurer sa protection. Et le moins que l''on puisse dire, c''est que la présence de cette ado pas comme les autres va provoquer moults bouleversements intergénérationnels...
	 ' || chr(10) || 'La fille de Vercingétorix est le fruit de la quatrième collaboration entre le scénariste Jean-Yves Ferri et le dessinateur Didier Conrad. Le duo, toujours à pied d''oeuvre pour imaginer de nouvelles aventures, s''inscrit dans le fabuleux univers créé par René Goscinny et Albert Uderzo.', 
	'Bande Dessinée', 'Editions Albert René', 'Français', 3),
	
	('9DXU4I', 'https://m.media-amazon.com/images/I/918gAiQcIOL._AC_UY218_ML3_.jpg', 'Miroir de nos peines', 'Pierre Lemaitre', 
	'Avril 1940. Louise, trente ans, court, nue, sur le boulevard du Montparnasse. Pour comprendre la scène tragique qu''elle vient de vivre, elle devra plonger dans la folie d''une période sans équivalent dans l''histoire où la France toute entière, saisie par la panique, sombre dans le chaos, faisant émerger les héros et les salauds, les menteurs et les lâches... Et quelques hommes de bonne volonté.
	' || chr(10) || 'Il fallait toute la verve et la générosité d''un chroniqueur hors pair des passions françaises pour saisir la grandeur et la décadence d''un peuple broyé par les circonstances.
	' || chr(10) || 'Secret de famille, grands personnages, puissance du récit, rebondissements, burlesque et tragique... Le talent de Pierre Lemaitre, prix Goncourt pour Au revoir là-haut, est ici à son sommet.', 
	'Romans', 'Albin Michel', 'Français', 4),
	
	('JSJ4FS', 'https://m.media-amazon.com/images/I/81VbZunq1CL._AC_UY218_ML3_.jpg', 'Sorceleur, Tome 1: Le Dernier Vœu', 'Andrzej Sapkowski', 
	'Geralt de Riv est un personnage étrange, une bizarrerie de la nature, un mutant qui, grâce à la magie et à un long entraînement, mais aussi grâce à un mystérieux élixir, est devenu un meurtrier parfait. Ses cheveux blancs, ses yeux nyctalopes et son manteau noir effrayent et fascinent. Il parcourt des contrées pittoresques en gagnant sa vie comme chasseur de monstres. En ces temps obscurs, ogres, goules et vampires pullulent, et les magiciens sont des manipulateurs experts. Contre ces menaces, il faut un tueur à gages à la hauteur. Car Geralt est plus qu un guerrier ou un mage.C est un Sorcereur. Il est unique.Au cours de ses aventures, il rencontrera une autoritaire mais généreuse prêtresse, un troubadour paillard au grand c ur, et une magicienne capricieuse aux charmes vénéneux. Amis d un jour, amours dune nuit. Mais au bout de sa quête, peut-être pourra-t-il prononcer son dernier v u : retrouver son humanité perdue...', 
	'Romans Fantastique', 'Bragelonne', 'Français', 2),
	
	('3IZSM1', 'https://m.media-amazon.com/images/I/81jfJUEh2AL._AC_UY218_ML3_.jpg', 'Les quatre accords toltèques : La voie de la liberté personnelle', 'Miguel Ruiz', 
	'Découvrez ou redécouvrez Les quatre accords toltèques, et prenez comme des millions de lecteurs en France et à travers le monde, la voie de la liberté personnelle. 
	' || chr(10) || 'Dans ce livre, Don Miguel révèle la source des croyances limitatrices qui nous privent de joie et créent des souffrances inutiles. Il montre en des termes très simples comment on peut se libérer du conditionnement collectif - le "rêve de la planète", basé sur la peur - afin de retrouver la dimension d''amour inconditionnel qui est à notre origine et constitue le fondement des enseignements toltèques que Castenada fut le premier à faire découvrir au grand public. Don Miguel révèle ici 4 clés simples pour transformer sa vie et ses relations, tirées de la sagesse toltèque. Leur application au quotidien permet de transformer rapidement notre vie en une expérience de liberté, de vrai bonheur et d''amour.
	' || chr(10) || 'Les quatre accords toltèques : 1 - Que ta parole soit impeccable ; 2 - Ne réagis à rien de façon personnelle ; 3 - Ne fais aucune supposition ; 4 - Fais toujours de ton mieux.
	' || chr(10) || 'Nouvelle édition tant attendue en format poche de ce best-seller ! 1 million d''exemplaires vendus en France de ce texte de référence en développement personnel. UN EVENEMENT !', 
	'Psychologie et Spirituel', 'JOUVENCE', 'Français', 1),
	
	('ZYA778', 'https://m.media-amazon.com/images/I/71uFN8LvWwL._AC_UY218_ML3_.jpg', 'UNE MISSION FORTNITE DONT TU ES LE HÉROS - TOME 1 - 99 contre 1', 'THiLO', 
	'99 contre 1
	' || chr(10) || 'Tu es  Bob  « Cold Blood »  Cooper, le meilleur combattant de ton clan. Tu es une véritable machine de guerre, et tu te distingues par ton sang-froid et ton esprit vif.   
	' || chr(10) || 'Ton chef de clan, Marcellus, t’a donné une carte, un chargeur de munitions un peu spécial… et une mission secrète. Tu vas devoir prendre le contrôle de  Dark  Dagalur. Sans quoi, l’humanité n’aura plus jamais accès à ce territoire. Mais tu seras seul. Seul, contre 99 hommes. Fais les bons choix si tu veux être à la hauteur de ta réputation.', 
	'Bande Dessinée', 'Play Bac', 'Français', 0),
	
	('XNR5H9', 'https://m.media-amazon.com/images/I/41zVHWA-uyL._AC_UY218_ML3_.jpg', 'Conversations être anges', 'Emiliâme Améologue', 
	'Un livre d’épanouissement personnel INÉDIT ! Tout a un sens ! Découvre enfin les coulisses de la vie ! " Parfois, la vie t’envoie des épreuves. Et si elles étaient plutôt des cadeaux que tu juges mal emballés pour te faire retrouver ton chemin ? Je te dévoile l’étonnante histoire que m’a racontée mon mari après avoir rendu l’âme.Au travers de ces conversations étranges, j’ai eu le privilège de découvrir ma vie vue de ses yeux, ou plutôt de son âme, et comme il me l’est réclamé, je te la livre ! L’âme agit de l’au-delà te donne enfin la possibilité de te cacher dans les coulisses de la vie pour te révéler ses secrets avec humour et le sens de ta venue sur Terre !Acceptes-tu de les découvrir au risque de changer ta vie pour toujours ?" Emiliâme est écrivaine, âméologue, conférencière et organisatrice de voyages transformateurs. Elle a déjà accompagné des milliers de personnes à se reconnecter à leur âme et à changer leur vie. Elle est l’auteure de livres inspirants, comme La fille qui donnait à boire à son poisson, L’apprenti sage du désert, Je devais mourir vieille, Conversations être anges, Je t''âme, etc. "', 
	'Psychologie et Spirituel', 'AFNIL', 'Français', 4),
	
	('8CNN21', 'https://m.media-amazon.com/images/I/716f1Rtx1CL._AC_UY218_ML3_.jpg', 'Père riche, père pauvre - Edition 20e anniversaire', 'Robert t Kiyosaki', 
	'Père riche, Père pauvre – Le meilleur ouvrage de tous les temps en matière de finances personnelles !
	' || chr(10) || 'Père riche, Père pauvre – un livre qui :
	' || chr(10) || 'Brise le mythe selon lequel il faut gagner beaucoup d''argent pour devenir riche.
	' || chr(10) || 'Remet en question cette croyance voulant que votre maison est un actif.
	' || chr(10) || 'Explique aux parents pourquoi ils ne doivent pas se fier au système d''éducation pour enseigner les rudiments de l''argent à leurs enfants.
	' || chr(10) || 'Vous enseigne quoi enseigner à vos enfants afin qu''ils connaissent plus tard la réussite financière.
	' || chr(10) || 'Cette édition comprend 9 sections de séances d''étude que vous pourrez utiliser comme guides pendant vos lectures, vos relectures, vos discussions et votre étude de cet ouvrage avec vos amis.', 
	'Guide Professionel', 'Un monde different', 'Français', 4),
	
	('MRPG6J', 'https://m.media-amazon.com/images/I/81Z0O1kHwsL._AC_UY218_ML3_.jpg', 'Blake & Mortimer - tome 26 - Vallée des Immortels', 'Sente Yves, Berserik Teun', 
	'Inquiets pour Mortimer qui a été kidnappé par les hommes du général Xi-Li, le capitaine Blake, accompagné de l''agente nationaliste chinoise Ylang Ti, se lance à la recherche de son ami. Se servant des indices que Mortimer a eu l''intelligence de semer sur son chemin, ils remontent peu à peu la piste qui devrait les conduire jusqu''à lui. De leur côté, Mortimer et Han-Dié, archéologue nationaliste qui a trahi son gouvernement pour vendre des documents historiques au général Xi-Li, sont emmenés de force vers le repaire du seigneur de guerre. Mortimer profite de ce voyage pour en apprendre davantage sur les documents qui apporteront la preuve à Xi-Li qu''il est bel et bien le descendant et l''héritier de Shi-Huangdi, le premier empereur de Chine.... dont la légende dit qu''il aurait été emmené à la fin de sa vie dans la Vallée des Immortels. Arrivé au camp de prisonniers, Mortimer retrouve Nasir, prisonnier de Xi-Li et à l''agonie. Déterminé à sauver son ami grâce à une « perle de vie » miraculeuse, Mortimer s''évade en compagnie de Mister Chou à la recherche de la fameuse Vallée des Immortels où ces perles seraient cachées. Et si ce mystérieux endroit n''était pas qu''un mythe ? Blake parvindra-t-il à rejoindre Mortimer à temps ? Nos deux héros pourront-ils sauver Nasir ? Surtout, parviendront-ils à sauver Hong Kong à temps des projets de destruction ourdis par le général Xi-LI et l''étrange ingénieur Chase... alias, le colonel Olrik ?', 
	'Bande dessinée', 'Blake Mortimer', 'Français', 1),
	
	('XRBPH8', 'https://m.media-amazon.com/images/I/81HdHL9x30L._AC_UY218_ML3_.jpg', 'La couleur des émotions - Un livre tout animé', 'Anna Llenas', 
	'Le monstre des couleurs se sent tout barbouillé aujourd''hui. Ses émotions sont sens dessus dessous ! Il ne comprends pas ce qui lui arrive. Réussira-t-il à mettre de l''ordre dans son coeur et à retrouver son équilibre ?', 
	'Livre Jeunesse', 'Editions Quatre Fleuves', 'Français', 5),
	
	('RTNXAV', 'https://m.media-amazon.com/images/I/912ruEbM1sL._AC_UY218_ML3_.jpg', 'Sorceleur, Tome 2: L''Épée de la providence', 'Andrzej Sapkowski', 
	'Geralt de Riv, le mutant aux cheveux d''albâtre, n''en a pas fini avec sa vie errante de tueur de monstres légendaires. Fidèle aux règles de la corporation maudite des sorceleurs et à l''enseignement qui lui a été prodigué, Geralt assume sa mission sans faillir dans un monde hostile et corrompu qui ne laisse aucune place à l''espoir. Mais la rencontre avec la petite Ciri, l''Enfant élue, va donner un sens nouveau à l''existence de ce héros solitaire. Geralt cessera-t-il enfin de fuir devant la mort pour affronter la providence et percer à jour son véritable destin ?', 
	'Romans Fantastique', 'Bragelonne', 'Français', 4),
	
	('G26FIY', 'https://m.media-amazon.com/images/I/41HtOsNN4WL._AC_UY218_ML3_.jpg', 'Sapiens: Une brève histoire de l''humanité', 'Yuval Noah Harari', 
	'L homo Sapiens sera la vedette de la rentrée littéraire puisqu il s est imposé par sa capacité à fictionner, donc à créer des récits mythologiques, des dieux, des lois, du réseau...
	' || chr(10) || 'Il y a 100 000 ans, la Terre était habitée par au moins six espèces différentes d hominidés. Une seule a survécu. Nous, les Homo Sapiens.
	' || chr(10) || 'Comment notre espèce a-t-elle réussi à dominer la planète ? Pourquoi nos ancêtres ont-ils uni leurs forces pour créer villes et royaumes ? Comment en sommes-nous arrivés à créer les concepts de religion, de nation, de droits de l homme ? À dépendre de l argent, des livres et des lois ? À devenir esclaves de la bureaucratie, des horaires, de la consommation de masse ? Et à quoi ressemblera notre monde dans le millénaire à venir ?
	' || chr(10) || 'Véritable phénomène d édition, traduit dans une trentaine de langues, Sapiens est un livre audacieux, érudit et provocateur. Professeur d Histoire à l Université hébraïque de Jérusalem, Yuval Noah Harari mêle l Histoire à la Science pour remettre en cause tout ce que nous pensions savoir sur l humanité : nos pensées, nos actes, notre héritage... et notre futur.
	' || chr(10) || '« Sapiens s est rapidement imposé partout dans le monde, parce qu il aborde les plus grandes questions de l histoire moderne dans une langue limpide et précise.»
	' || chr(10) || 'Jared Diamond, prix Pulitzer, auteur d Effondrement', 
	'Livre Documentaire', 'Albin Michel', 'Français', 1),
	
	('F81KZP', 'https://m.media-amazon.com/images/I/91S0vPiCAmL._AC_UY218_ML3_.jpg', 'One Piece - Édition originale - Tome 93', 'Eiichiro Oda', 
	'Conviée au château d’Orochi en même temps que la grande courtisane Komurasaki, Robin est prise la main dans le sac par les espions de son hôte alors qu’elle est à la recherche d’informations. La voilà dos au mur ! Quant à Chapeau de paille, il cherche un moyen de s’évader de la carrière où il est retenu prisonnier depuis qu’il a été battu par Kaido. Les aventures de Luffy à la poursuite du One Piece continuent !', 
	'Bande dessinée', 'Glénat Manga', 'Français', 5),
	
	('MUJHY0', 'https://m.media-amazon.com/images/I/81KQFKzJBEL._AC_UY218_ML3_.jpg', 'Victime 2117 : La huitième enquête du département V', 'Jussi Adler-Olsen, Caroline Berg', 
	'Le journal en parle comme de la « victime 2117 » : une réfugiée qui, comme les deux mille cent seize autres qui l''ont précédée cette année, a péri en Méditerranée dans sa tentative désespérée de rejoindre l''Europe.
	' || chr(10) || 'Mais pour Assad, qui oeuvre dans l''ombre du Département V de Copenhague depuis dix ans, cette mort est loin d''être anonyme. Elle le relie à son passé et fait resurgir de douloureux souvenirs.
	' || chr(10) || 'Il est temps pour lui d''en finir avec les secrets et de révéler à Carl Mørck et à son équipe d''où il vient et qui il est. Au risque d''entraîner le Département V dans l''oeil du cyclone.
	' || chr(10) || 'Qui est Assad ? Victime 2117 est la réponse. Cette enquête est son histoire.', 
	'Romans Policier', 'Albin Michel', 'Français', 0),
	
	('31J4C9', 'https://m.media-amazon.com/images/I/6135XeX3cnL._AC_UY218_ML3_.jpg', 'Les cinq blessures qui empêchent d''être soi-même', 'Lise BOURBEAU', 
	'Apprendre à être soi-même.', 
	'Psychologie et Spirituel', 'Pocket', 'Français', 1),
	
	('7GHACO', 'https://m.media-amazon.com/images/I/81yFvu4r46L._AC_UY218_ML3_.jpg', 'Harry Potter, I : Harry Potter à l''école des sorciers', 'J. K. Rowling', 
	'Le jour de ses onze ans, Harry Potter, un orphelin élevé par un oncle et une tante qui le détestent, voit son existence bouleversée. Un géant vient le chercher pour l''emmener à Poudlard, une école de sorcellerie ! Voler en balai, jeter des sorts, combattre les trolls : Harry se révèle un sorcier doué. Mais un mystère entoure sa naissance et l''effroyable V., le mage dont personne n''ose prononcer le nom.', 
	'Romans Fantastique', 'Folio Junior', 'Français', 0),
	
	('V62V6Q', 'https://m.media-amazon.com/images/I/51Ejg5F1sNL._AC_UY218_ML3_.jpg', 'Réfléchissez et devenez riche', 'Napoleon Hill', 
	'Vous trouverez dans ce livre ce que personne ne vous enseignera jamais ailleurs : comment assimiler et appliquer les principes qui vous permettront de faire fortune et d''atteindre les buts que vous vous êtes fixés. Napoleon Hill parvient avec brio à démontrer comment vos pensées et vos croyances peuvent changer le cours de votre vie. Fruit d''une recherche de plus de vingt ans sur la richesse et le talent, Réfléchissez et devenez riche développe treize principes universels qui stimuleront votre confiance et vous mèneront vers une réussite garantie.', 
	'Psychologie et Spirituel', 'J''AI LU', 'Français', 2),
	
	('O69MQG', 'https://m.media-amazon.com/images/I/41nDvTXx8rL._AC_UY218_ML3_.jpg', 'Le tout petit roi', 'Patrick Honnoré', 
	'Une très belle fable sur le bonheur et la quiétude que ce petit album qui nous vient du Japon ! L''histoire d''un tout petit roi pour qui tout est trop grand : son château, son armée... Jusqu''à ce qu''il rencontre une très très grande dame. Ou l''art et la satisfaction de trouver « chaussure à son pied », au-delà des apparences et des vanités.', 
	'Livre Jeunesse', 'Editions Milan', 'Français', 4),
	
	('A84LZ6', 'https://m.media-amazon.com/images/I/71dTOwYolsL._AC_UY218_ML3_.jpg', 'Le livre qui t''explique enfin tout sur les parents', ' Françoize Boucher', 
	'Découvre enfin pourquoi tes parents sont des crétures exceptionnelles. Tu comprendras que même lorsqu''ils te gonflent, c''est pour ton plus grand bonheur, car sous une apparence ordinaire, les parents sont des créatures exceptionnelles !', 
	'Livre Jeunesse', 'Nathan', 'Français', 1),
	
	('68NT01', 'https://m.media-amazon.com/images/I/612IC3oNG6L._AC_UY218_ML3_.jpg', 'Les bruits de la ferme - Mon livre sonore à toucher', 'Sam Taplin, Federica Iossa, Matt Durber', 
	'Une promenade en images, en musique et du bout des doigts dans une ferme animée. Les jeunes enfants ne résisteront pas au plaisir d''appuyer sur les puces sonores pour découvrir les cris des animaux et les bruits de la ferme.
	' || chr(10) || 'Une promenade en images, en musique et du bout des doigts dans une ferme animée. Les jeunes enfants ne résisteront pas au plaisir d''appuyer sur les puces sonores pour découvrir les cris des animaux et les bruits de la ferme.
	' || chr(10) || 'Avec différentes découpes à suivre du doigt.
	' || chr(10) || 'Mon livre sonore à toucher : une série de livres à la fois tactiles et sonores qui rencontrent un grand succès.
	' || chr(10) || 'Dans la même collection : Les bruits de la nuit, Les bruits du jardin et Les bruits de la jungle.', 
	'Livre Jeunesse', 'Usborne', 'Français', 5),
	
	('059270', 'https://m.media-amazon.com/images/I/71+v927RoGL._AC_UY218_ML3_.jpg', 'Just 17 (Just Seventeen)', 'Emma Green', 
	'Une lycéenne rebelle. Un professeur hipster.
	' || chr(10) || 'Un amour interdit.
	' || chr(10) || 'Onze ans les séparent.
	' || chr(10) || 'Mais la morale ne peut rien contre l’amour.
	' || chr(10) || 'Elle est bien plus que son élève.
	' || chr(10) || 'Il lui est formellement interdit.
	' || chr(10) || 'Elle a tout à apprendre.
	' || chr(10) || 'Il a tant à perdre…
	' || chr(10) || 'Elle n’a que 17 ans.
	' || chr(10) || 'Mais elle sait ce qu’elle veut : lui.
	' || chr(10) || '***
	' || chr(10) || '– N’avance plus.
	' || chr(10) || '– Pourquoi ?
	' || chr(10) || '– Tu n’es qu’une ado qui fantasme sur son prof. Je ne joue pas à ça, moi.
	' || chr(10) || '– Tu dis ça pour me blesser. Ou pour tester ma réaction. Mais je n’arrêterai pas…
	' || chr(10) || '– Il le faut, Lemon.
	' || chr(10) || 'À sa voix qui devient un souffle, je devine qu’il tente de résister. De jouer au prof, à l’adulte, au type raisonnable qui ne va pas craquer. Mais son regard dit tant d’autres choses.
	' || chr(10) || 'Alors je continue. Je mange la distance entre nous, comme affamée. Mon cœur s’emballe, une douce chaleur se propage sous ma peau, la pulpe de mes doigts se met à fourmiller.
	' || chr(10) || 'Je le veux.
	' || chr(10) || '***', 
	'Romans Sentimental', 'Éditions Addictives', 'Français', 2),
	
	('XKSSIP', 'https://m.media-amazon.com/images/I/81aeYl0TpDL._AC_UY218_ML3_.jpg', 'Les Indes fourbes', 'Alain Ayroles, Juanjo Guarnido', 
	'Fripouille sympathique, don Pablos de Ségovie fait le récit de ses aventures picaresques dans cette Amérique qu''on appelait encore les Indes au siècle d''or. Tour à tour misérable et richissime, adoré et conspué, ses tribulations le mèneront des bas-fonds aux palais, des pics de la Cordillère aux méandres de l''Amazone, jusqu''à ce lieu mythique du Nouveau Monde : l''Eldorado !', 
	'Bande dessinée', 'Delcourt', 'Français', 2),
	
	('OB2ZY5', 'https://m.media-amazon.com/images/I/91l8WafRMtL._AC_UY218_ML3_.jpg', 'Sorceleur, Tome 3: Le Sang des elfes', 'Andrzej Sapkowski', 
	'Le royaume de Cintra a été entièrement détruit. Seule la petite princesse Ciri a survécu. Alors qu''elle tente de fuir la capitale, elle croise le chemin de Geralt de Riv. Pressentant chez l''enfant des dons exceptionnels, il la conduit à Kaer Morhen, l''antre des sorceleurs. Initiée aux arts magiques, Ciri y révèle bien vite sa véritable nature et l''ampleur de ses pouvoirs. Mais la princesse est en danger. Un mystérieux sorcier est à sa recherche. Il est prêt à tout pour s''emparer d''elle et n''hésitera pas à menacer les amis du sorceleur pour arriver à ses fins...', 
	'Romans Fantastique', 'Bragelonne', 'Français', 4),
	
	/* Livres en Anglais */ 
	('8R230I', 'https://m.media-amazon.com/images/I/810bMQcewQL._AC_UY218_ML3_.jpg', 'H. P. Lovecraft: The Complete Fiction', ' H. P. Lovecraft, Book Center', 
	'CONTENTS:
	' || chr(10) || 'The Nameless City
	' || chr(10) || 'The Festival
	' || chr(10) || 'The Colour Out of Space
	' || chr(10) || 'The Call of Cthulhu
	' || chr(10) || 'The Dunwich Horror
	' || chr(10) || 'The Whisperer in Darkness
	' || chr(10) || 'The Dreams in the Witch House
	' || chr(10) || 'The Haunter of the Dark
	' || chr(10) || 'The Shadow Over Innsmouth
	' || chr(10) || 'Discarded Draft of "The Shadow Over Innsmouth"
	' || chr(10) || 'The Shadow Out of Time
	' || chr(10) || 'At the Mountains of Madness
	' || chr(10) || 'The Case of Charles Dexter Ward
	' || chr(10) || 'Azathoth
	' || chr(10) || 'Beyond the Wall of Sleep
	' || chr(10) || 'Celephaïs
	' || chr(10) || 'Cool Air
	' || chr(10) || 'Dagon
	' || chr(10) || 'Ex Oblivione
	' || chr(10) || 'Facts Concerning the Late Arthur Jermyn and His Family
	' || chr(10) || 'From Beyond
	' || chr(10) || 'He
	' || chr(10) || 'Herbert West-Reanimator
	' || chr(10) || 'Hypnos
	' || chr(10) || 'In the Vault
	' || chr(10) || 'Memory
	' || chr(10) || 'Nyarlathotep
	' || chr(10) || 'Pickman''s Model
	' || chr(10) || 'The Book
	' || chr(10) || 'The Cats of Ulthar
	' || chr(10) || 'The Descendant
	' || chr(10) || 'The Doom That Came to Sarnath
	' || chr(10) || 'The Dream-Quest of Unknown Kadath
	' || chr(10) || 'The Evil Clergyman
	' || chr(10) || 'The Horror at Red Hook
	' || chr(10) || 'The Hound
	' || chr(10) || 'The Lurking Fear
	' || chr(10) || 'The Moon-Bog
	' || chr(10) || 'The Music of Erich Zann
	' || chr(10) || 'The Other Gods
	' || chr(10) || 'The Outsider
	' || chr(10) || 'The Picture in the House
	' || chr(10) || 'The Quest of Iranon
	' || chr(10) || 'The Rats in the Walls
	' || chr(10) || 'The Shunned House
	' || chr(10) || 'The Silver Key
	' || chr(10) || 'The Statement of Randolph Carter
	' || chr(10) || 'The Strange High House in the Mist
	' || chr(10) || 'The Street
	' || chr(10) || 'The Temple
	' || chr(10) || 'The Terrible Old Man
	' || chr(10) || 'The Thing on the Doorstep
	' || chr(10) || 'The Tomb
	' || chr(10) || 'The Transition of Juan Romero
	' || chr(10) || 'The Tree
	' || chr(10) || 'The Unnamable
	' || chr(10) || 'The White Ship
	' || chr(10) || 'What the Moon Brings
	' || chr(10) || 'Polaris
	' || chr(10) || 'The Very Old Folk
	' || chr(10) || 'Ibid
	' || chr(10) || 'Old Bugs
	' || chr(10) || 'Sweet Ermengarde, or, The Heart of a Country Girl
	' || chr(10) || 'A Reminiscence of Dr. Samuel Johnson
	' || chr(10) || 'The History of the Necronomicon', 
	'Fiction', 'Oregan Publishing', 'Anglais', 0),
	
	('BDVGXM', 'https://m.media-amazon.com/images/I/71tuo9YISuL._AC_UY218_ML3_.jpg', 'The Curious Incident of the Dog in the Night-time', 'Mark Haddon', 
	'The Curious Incident of the Dog in the Night-Time is a murder mystery novel like no other. The detective, and narrator, is Christopher Boone. Christopher is fifteen and has Asperger''s, a form of autism. He knows a very great deal about maths and very little about human beings. He loves lists, patterns and the truth. He hates the colours yellow and brown and being touched. He has never gone further than the end of the road on his own, but when he finds a neighbour''s dog murdered he sets out on a terrifying journey which will turn his whole world upside down.', 
	'Romans d''Aventure', 'Vintage', 'Anglais', 1),
	
	('7VMPPJ', 'https://m.media-amazon.com/images/I/91H4dBqesCL._AC_UY218_ML3_.jpg', 'Animal Farm', 'GEORGE ORWELL', 
	'Animal Farm, by George Orwell - author of 1984, one of Britain''s most popular novels - is a brilliant political satire and a powerful and affecting story of revolutions and idealism, power and corruption. "All animals are equal. But some animals are more equal than others."Mr Jones of Manor Farm is so lazy and drunken that one day he forgets to feed his livestock. The ensuing rebellion under the leadership of the pigs Napoleon and Snowball leads to the animals taking over the farm. Vowing to eliminate the terrible inequities of the farmyard, the renamed Animal Farm is organised to benefit all who walk on four legs. But as time passes, the ideals of the rebellion are corrupted, then forgotten. And something new and unexpected emerges. . . Animal Farm - the history of a revolution that went wrong - is George Orwell''s brilliant satire on the corrupting influence of power."Remains our great satire of the darker face of modern history" Malcolm Bradbury "Animal Farm has seen off all the opposition. It''s as valid as today as it was fifty years ago" Ralph SteadmanGeorge Orwell (Eric Arthur Blair) was an accomplished social, political and literary commentator and essayist known for his non-fiction works The Road to Wigan Pier and Homage to Catalonia. His most famous novels, Animal Farm and 1984 have influenced a generation of twentieth century political satirists and dystopian novelists. Animal Farm received the W. H. Smith and Penguin Books Great Reads of the century award in 1995; this edition of Orwell''s seminal novella is introduced by Professor Peter Davidson.', 
	'Romans Politique', 'Penguin', 'Anglais', 4),
	
	('HLY0Z9', 'https://m.media-amazon.com/images/I/51o-Sy4oz5L._AC_UY218_ML3_.jpg', 'Of Mice and Men', 'John Steinbeck', 
	'Streetwise George and his big, childlike friend Lennie are drifters, searching for work in the fields and valleys of California. They have nothing except the clothes on their back, and a hope that one day they''ll find a place of their own and live the American dream. But dreams come at a price. Gentle giant Lennie doesn''t know his own strength, and when they find work at a ranch he gets into trouble with the boss''s daughter-in-law. Trouble so bad that even his protector George may not be able to save him ...', 
	'Fiction', 'Penguin', 'Anglais', 2),
	
	('DP2A2M', 'https://m.media-amazon.com/images/I/91jnHs93UmL._AC_UY218_ML3_.jpg', 'The Last Wish: Introducing the Witcher - Now a major Netflix show', 'Andrzej Sapkowski', 
	'Introducing Geralt the Witcher - revered and hated - who holds the line against the monsters plaguing humanity in the bestselling series that inspired the Witcher video games and a major Netflix show.
	' || chr(10) || 'Geralt of Rivia is a Witcher, a man whose magic powers and lifelong training have made him a brilliant fighter and a merciless assassin.
	' || chr(10) || 'Yet he is no ordinary killer: he hunts the vile fiends that ravage the land and attack the innocent.
	' || chr(10) || 'But not everything monstrous-looking is evil; not everything fair is good . . . and in every fairy tale there is a grain of truth.
	' || chr(10) || 'Andrzej Sapkowski, winner of the World Fantasy Lifetime Achievement award, started an international phenomenon with his Witcher series. The Last Wish is the perfect introduction to this one-of-a-kind fantasy world.', 
	'Romans Fantastique', 'Gollancz', 'Anglais', 3),
	
	('A3GITZ', 'https://m.media-amazon.com/images/I/7109Y4hi9ML._AC_UY218_ML3_.jpg', 'Thinking, Fast and Slow', 'Daniel Kahneman', 
	'The New York Times Bestseller, acclaimed by author such as Freakonomics co-author Steven D. Levitt, Black Swan author Nassim Nicholas Taleb and Nudge co-author Richard Thaler, Thinking Fast and Slow offers a whole new look at the way our minds work, and how we make decisions.Why is there more chance we''ll believe something if it''s in a bold type face? Why are judges more likely to deny parole before lunch? Why do we assume a good-looking person will be more competent? The answer lies in the two ways we make choices: fast, intuitive thinking, and slow, rational thinking. This book reveals how our minds are tripped up by error and prejudice (even when we think we are being logical), and gives you practical techniques for slower, smarter thinking. It will enable to you make better decisions at work, at home, and in everything you do.', 
	'Psychologie et Spirituel', 'Penguin', 'Anglais', 5),
	
	('02JZ2C', 'https://m.media-amazon.com/images/I/9125VJHKYCL._AC_UY218_ML3_.jpg', 'Little Women', 'Louisa May Alcott', 
	'Discover this beautiful and charming classic book behind the new major film.
	' || chr(10) || '"Rich or poor, we will keep together and be happy in one another"
	' || chr(10) || 'Christmas won''t be the same this year for Meg, Jo, Beth and Amy, as their father is away fighting in the Civil War, and the family has fallen on hard times. But although they may be poor, life for the four March sisters is rich with colour, as they play games, put on wild theatricals, make new friends, argue, grapple with their vices, learn from their mistakes, nurse each other through sickness and disappointments, and get into all sorts of trouble.
	' || chr(10) || 'BACKSTORY: Learn all about the author''s life and how it inspired her famous story, and find out which of the March sisters you most resemble!', 
	'Fiction', 'Vintage Children''s Classics', 'Anglais', 4),
	
	('DC79B4', 'https://m.media-amazon.com/images/I/81DrYlGKrhL._AC_UY218_ML3_.jpg', 'Brain Training: 8-in-1 Bundle to Master Memory, Speed Reading, Concentration, Accelerated Learning, Study Skills, Mind Mapping, Mental Models & Neuroplasticity', 'Troye Bates', 
	'BRAIN TRAINING - 8 MANUSCRIPTS IN 1 BOOK:
	' || chr(10) || '1)
	' || chr(10) || 'HOW TO IMPROVE MEMORY:
	' || chr(10) || '7 Steps to Master Memory Improvement, Memorization Techniques & Photographic Memory.
	' || chr(10) || '2)
	' || chr(10) || 'HOW TO READ FASTER:
	' || chr(10) || '7 Steps to Master Speed Reading Techniques, Reading Comprehension & Fast Reading.
	' || chr(10) || '3)
	' || chr(10) || 'HOW TO FOCUS YOUR MIND:
	' || chr(10) || '7 Steps to Master Concentration Techniques, Attention Management & Staying Focused.
	' || chr(10) || '4)
	' || chr(10) || 'HOW TO LEARN FASTER:
	' || chr(10) || '7 Steps to Master Accelerated Learning Techniques, Learning Strategies & Fast Self-learning.
	' || chr(10) || '5)
	' || chr(10) || 'HOW TO STUDY EFFECTIVELY:
	' || chr(10) || '7 Steps to Master Effective Study Skills, Student Success, Note Taking & Exam Preparation.
	' || chr(10) || '6)
	' || chr(10) || 'HOW TO MIND MAP:
	' || chr(10) || '7 Steps to Master Mind Mapping Techniques, Note-taking, Creative Thinking & Brainstorming Skills.
	' || chr(10) || '7)
	' || chr(10) || 'HOW TO THINK DIFFERENTLY:
	' || chr(10) || '7 Steps to Master Mental Models, Critical Thinking, Decision Making & Problem Solving.
	' || chr(10) || '8)
	' || chr(10) || 'HOW TO REWIRE YOUR BRAIN:
	' || chr(10) || '7 Steps to Master Neuroplasticity, Mind Hacking, Think Habits & Practical Neuroscience.
	' || chr(10) || 'TRAIN YOUR BRAIN TODAY!', 
	'Psychologie et Spirituel', 'Lulu.com', 'Anglais', 3),
	
	('Z8C9X2', 'https://m.media-amazon.com/images/I/81avoNZGdtL._AC_UY218_ML3_.jpg', 'Ryan''s Christmas: A DCI Ryan Mystery', 'LJ Ross', 
	'FROM THE #1 INTERNATIONAL BESTSELLING AUTHOR OF HOLY ISLAND AND IMPOSTOR
	' || chr(10) || 'Christmas can be murder…
	' || chr(10) || 'After a busy year fighting crime, DCI Ryan and his team of murder detectives are enjoying a festive season of goodwill, mulled wine and, in the case of DS Phillips, a stottie cake or two—that is, until a freak snowstorm forces their car off the main road and into the remote heart of Northumberland. Their Christmas spirit is soon tested when they’re forced to find shelter inside England’s most haunted castle, where they’re the uninvited guests at a ‘Candlelit Ghost Hunt’. It’s all fun and games—until one of the guests is murdered. It seems no mortal hand could have committed the crime, so Ryan and Co. must face the spectres living inside the castle walls to uncover the grisly truth, before another ghost joins their number…
	' || chr(10) || 'Murder and mystery are peppered with romance and humour in this fast-paced crime whodunnit set amidst the spectacular Northumbrian landscape.
	' || chr(10) || '“LJ Ross keeps company with the best mystery writers” – The Times
	' || chr(10) || '“A literary phenomenon” – Evening Chronicle
	' || chr(10) || '“LJ Ross is the Queen of Kindle” – Sunday Telegraph', 
	'Romans Policier', 'Amazon Media EU', 'Anglais', 5),
	
	('4CLC14', 'https://m.media-amazon.com/images/I/71FxgtFKcQL._AC_UY218_ML3_.jpg', 'To Kill a Mockingbird', 'Harper Lee', 
	'The unforgettable novel of a childhood in a sleepy Southern town and the crisis of conscience that rocked it, To Kill A Mockingbird became both an instant bestseller and a critical success when it was first published in 1960. It went on to win the Pulitzer Prize in 1961 and was later made into an Academy Award-winning film, also a classic.
	' || chr(10) || 'Compassionate, dramatic, and deeply moving, To Kill A Mockingbird takes readers to the roots of human behavior - to innocence and experience, kindness and cruelty, love and hatred, humor and pathos. Now with over 18 million copies in print and translated into forty languages, this regional story by a young Alabama woman claims universal appeal. Harper Lee always considered her book to be a simple love story. Today it is regarded as a masterpiece of American literature.', 
	'Romans Fantastique', 'Grand Central Publishing', 'Anglais', 4),
	
	('ZD3V9R', 'https://m.media-amazon.com/images/I/715S4NQB1oL._AC_UY218_ML3_.jpg', 'English Grammar in Use with Answers: A Self-Study Reference and Practice Book for Intermediate learners of English', 'Raymond Murphy', 
	'English Grammar in Use Fourth edition is an updated version of the world''s best-selling grammar title. It has a fresh, appealing new design and clear layout, with revised and updated examples, but retains all the key features of clarity and accessibility that have made the book popular with millions of learners and teachers around the world. This "with answers" version is ideal for self-study.', 
	'Livre Documentaire', 'Cambridge University Press', 'Anglais', 1),
	
	('4XAWX5', 'https://m.media-amazon.com/images/I/71fsQxIlggL._AC_UY218_ML3_.jpg', 'Sapiens: A Brief History of Humankind', 'Yuval Noah Harari', 
	'**THE MILLION COPY BESTSELLER**
	' || chr(10) || 'What makes us brilliant? What makes us deadly? What makes us Sapiens? This bestselling history challenges everything we know about being human.
	' || chr(10) || 'Earth is 4.5 billion years old. In just a fraction of that time, one species among countless others has conquered it: us.
	' || chr(10) || 'In this bold and provocative book, Yuval Noah Harari explores who we are, how we got here and where we’re going.
	' || chr(10) || '‘I would recommend Sapiens to anyone who’s interested in the history and future of our species’ Bill Gates
	' || chr(10) || '‘Interesting and provocative… It gives you a sense of how briefly we’ve been on this Earth’ Barack Obama
	' || chr(10) || '**ONE OF THE GUARDIAN''S 100 BEST BOOKS OF THE 21st CENTURY**', 
	'Livre Documentaire', 'Vintage', 'Anglais', 0),
	
	('843GIA', 'https://m.media-amazon.com/images/I/8130rINea+L._AC_UY218_ML3_.jpg', 'Enemies', 'Tijan', 
	'Stone Reeves was my neighbor, and I''ve hated him since sixth grade.
	' || chr(10) || 'Gorgeous and charismatic, he became the town''s football god, while I became the town''s invisible girl.
	He went to a Division 1 school for football, while my father was fired by his father.
	His team won the National Championship, while my mother died the same day.
	' || chr(10) || 'He was a first round pick for the NFL ...
	... while I made the worst decision of my life.
	' || chr(10) || 'Now I''m in Texas trying to pick up the pieces of my life.
	But, Stone is here.
	Stone is everywhere.
	' || chr(10) || 'It doesn''t matter that disaster has struck my life again.
	It doesn''t matter that he''s the one trying to console me.
	It doesn''t matter that he''s the nation''s newest football obsession.
	' || chr(10) || 'Because for me, he always has been and always will be my enemy.
	' || chr(10) || '** Enemies is a 100k enemies-to-lovers football romance standalone!', 
	'Romans Sentimental', 'Amazon Media EU', 'Anglais', 4),
	
	('Y7OWIE', 'https://m.media-amazon.com/images/I/91VokXkn8hL._AC_UY218_ML3_.jpg', 'Rich Dad Poor Dad', 'Robert T. Kiyosaki', 
	'April 2017 marks 20 years since Robert Kiyosaki''s Rich Dad Poor Dad first made waves in the Personal Finance arena.
	It has since become the #1 Personal Finance book of all time... translated into dozens of languages and sold around the world.
	Rich Dad Poor Dad is Robert''s story of growing up with two dads -- his real father and the father of his best friend, his rich dad -- and the ways in which both men shaped his thoughts about money and investing. The book explodes the myth that you need to earn a high income to be rich and explains the difference between working for money and having your money work for you.
	' || chr(10) || '20 Years... 20/20 Hindsight
	In the 20th Anniversary Edition of this classic, Robert offers an update on what we''ve seen over the past 20 years related to money, investing, and the global economy. Sidebars throughout the book will take readers "fast forward" -- from 1997 to today -- as Robert assesses how the principles taught by his rich dad have stood the test of time.
	' || chr(10) || 'In many ways, the messages of Rich Dad Poor Dad, messages that were criticized and challenged two decades ago, are more meaningful, relevant and important today than they were 20 years ago.
	' || chr(10) || 'As always, readers can expect that Robert will be candid, insightful... and continue to rock more than a few boats in his retrospective.
	' || chr(10) || 'Will there be a few surprises? Count on it.
	' || chr(10) || 'Rich Dad Poor Dad...
	- Explodes the myth that you need to earn a high income to become rich
	- Challenges the belief that your house is an asset
	- Shows parents why they can''t rely on the school system to teach their kids about money
	- Defines once and for all an asset and a liability
	- Teaches you what to teach your kids about money for their future financial success', 
	'Psychologie et Spirituel', 'Plata Publishing', 'Anglais', 4),
	
	('ZLU1NO', 'https://m.media-amazon.com/images/I/A1491oGTn5L._AC_UY218_ML3_.jpg', 'Devious Lies: A Standalone Enemies-to-Lovers Romance', 'Parker S. Huntington', 
	'“A sizzling, chemistry-filled enemies-to-lovers romance, which is both crackaliciously addictive and angst-central. I couldn''t get enough of Nash and Em. One of my favorite books of the year." - L.J. Shen, USA Today Bestselling Author
	' || chr(10) || 'From USA Today bestselling author Parker S. Huntington comes an enemies-to-lovers, slow-burn romance full of revenge and a dash of fate.
	' || chr(10) || '“She could enjoy her pretty, perfect world a little longer. Soon enough, everything she owned would be mine.”
	' || chr(10) || 'I had a plan to escape the friend zone.
	Step one: sneak into Reed’s room.
	Step two: sleep with him.
	' || chr(10) || 'But when the lights turned on, it wasn’t familiar blue eyes I saw.
	These were dark, angry, and full of demons.
	And they belonged to Reed’s much older brother.
	' || chr(10) || 'Four years later, Nash Prescott is no longer the help’s angry son.
	I’m no longer the town’s prized princess.
	' || chr(10) || 'At twenty-two, I’m broke, in need of a job.
	At thirty-two, he’s a billionaire, in need of revenge.
	' || chr(10) || 'Who cares if my family ruined his?
	Who cares if he looks at me with pure loathing?
	Who cares if every task he assigns me is designed to torture?
	' || chr(10) || 'I need the money.
	Simple as that.
	' || chr(10) || 'I’ll suffer his cruelty in silence, knowing there’s one thing he wants more than revenge…
	Me.
	' || chr(10) || 'Note: Devious Lies is a 145,000-word standalone. If you love banter and angst, this book is for you! Welcome to Eastridge. Enter if you dare.', 
	'Romans Sentimental', 'PSH Publishing', 'Anglais', 1),
	
	('LCAALA', 'https://m.media-amazon.com/images/I/813cb0CcJ5L._AC_UY218_ML3_.jpg', 'How To Win Friends And Influence People.', 'Dale Carnegie', 
	'You can go after the job you want...and get it! You can tade the job you have...and improve it! You can take my situation you''re in...and maket it work for you!', 
	'Guide Professionel', 'Pocket Books', 'Anglais', 1),
	
	('HVICQZ', 'https://m.media-amazon.com/images/I/61SpcaD4poL._AC_UY218_ML3_.jpg', 'The Handmaid''s Tale', 'Margaret Atwood', 
	'** THE SUNDAY TIMES NO. 1 BESTSELLER **
	' || chr(10) || 'Discover the dystopian novel that started a phenomenon before you read the 2019 Booker Prize-winning sequel The Testaments
	' || chr(10) || 'NOW AN AWARD-WINNING TV SERIES STARRING ELISABETH MOSS
	' || chr(10) || 'Offred is a Handmaid in The Republic of Gilead, a religious totalitarian state in what was formerly known as the United States. She is placed in the household of The Commander, Fred Waterford – her assigned name, Offred, means ‘of Fred’. She has only one function: to breed. If Offred refuses to enter into sexual servitude to repopulate a devastated world, she will be hanged. Yet even a repressive state cannot eradicate hope and desire. As she recalls her pre-revolution life in flashbacks, Offred must navigate through the terrifying landscape of torture and persecution in the present day, and between two men upon which her future hangs.
	' || chr(10) || 'Masterfully conceived and executed, this haunting vision of the future places Margaret Atwood at the forefront of dystopian fiction.
	' || chr(10) || '‘A fantastic, chilling story. And so powerfully feminist’ Bernadine Evaristo, author of GIRL, WOMAN, OTHER
	' || chr(10) || '‘This novel seems ever more vital in the present day’ Observer
	' || chr(10) || 'READ THE TESTAMENTS, THE BOOKER PRIZE-WINNING SEQUEL TO THE HANDMAID’S TALE, TODAY', 
	'Fiction', 'Vintage', 'Anglais', 4),
	
	('QWM28D', 'https://m.media-amazon.com/images/I/61aepmwQioL._AC_UY218_ML3_.jpg', 'Middle England', 'Jonathan Coe', 
	'', 
	'Livre Comique', 'Klett Sprachen GmbH', 'Anglais', 0),
	
	('S0PMF4', 'https://m.media-amazon.com/images/I/71-qZ2Z754L._AC_UY218_ML3_.jpg', '1984', 'George Orwell', 
	'Written more than 70 years ago, 1984 was George Orwell’s chilling prophecy about the future. And while 1984 has come and gone, his dystopian vision of a government that will do anything to control the narrative is timelier than ever...
	' || chr(10) || '• Nominated as one of America’s best-loved novels by PBS’s The Great American Read •
	' || chr(10) || '“The Party told you to reject the evidence of your eyes and ears. It was their final, most essential command.”
	' || chr(10) || 'Winston Smith toes the Party line, rewriting history to satisfy the demands of the Ministry of Truth. With each lie he writes, Winston grows to hate the Party that seeks power for its own sake and persecutes those who dare to commit thoughtcrimes. But as he starts to think for himself, Winston can’t escape the fact that Big Brother is always watching...
	' || chr(10) || 'A startling and haunting novel, 1984 creates an imaginary world that is completely convincing from start to finish. No one can deny the novel’s hold on the imaginations of whole generations, or the power of its admonitions—a power that seems to grow, not lessen, with the passage of time.', 
	'Romans Politique', 'Signet', 'Anglais', 0),
	
	('KLEW5P', 'https://m.media-amazon.com/images/I/51kQAgaiu3L._AC_UY218_ML3_.jpg', 'Heart of a Lion: The International Edition', 'Memphis Depay, Simon Zwartkruis', 
	'Everyone has an opinion on Memphis Depay, but few are familiar with his intense and difficult life story. Now the time has come for him to tell that story in his own words. About a childhood full of aggression and fear, with the ball as a lifebuoy. About a turbulent journey through different schools and host families and football clubs. His breakthrough at PSV, the fiasco in Manchester, his revival in Lyon and on the Dutch national team, the unending commotion about his appearance and image, his musical ambitions, his small circle of trusted confidants, and his relationship with God: it''s all here in this vulnerable, yet powerful and inspiring biography.', 
	'Romans Sportif', 'VIP', 'Anglais', 3),
	
	('XVGHOA', 'https://m.media-amazon.com/images/I/81zZiTgpcYL._AC_UY218_ML3_.jpg', 'My Name is Eva: An absolutely gripping and emotional historical novel', 'Suzanne Goldring', 
	'You can pay a terrible price for keeping a promise…
	' || chr(10) || 'Evelyn Taylor-Clarke sits in her chair at Forest Lawns Care Home in the heart of the English countryside, surrounded by residents with minds not as sharp as hers. It would be easy to dismiss Evelyn as a muddled old woman, but her lipstick is applied perfectly, and her buttons done up correctly. Because Evelyn is a woman with secrets and Evelyn remembers everything. She can never forget the promise she made to the love of her life, to discover the truth about the mission that led to his death, no matter what it cost her…
	' || chr(10) || 'When Evelyn’s niece Pat opens an old biscuit tin to find a photo of a small girl with a red ball entitled ‘Liese, 1951’ and a passport in another name, she has some questions for her aunt. And Evelyn is transported back to a place in Germany known as ‘The Forbidden Village,’ where a woman who called herself Eva went where no one else dared, amongst shivering prisoners, to find the man who gambled with her husband’s life…
	' || chr(10) || 'A gripping, haunting and compelling read about love, courage and betrayal set in the war-battered landscape of Germany. Fans of The Letter, The Alice Network and The Nightingale will be hooked.
	' || chr(10) || 'Readers are hooked on My Name is Eva:
	' || chr(10) || '‘Could not put this book down, and heaven help anyone that tried to disturb my reading !!…I absolutely loved this book !…I laughed, I cried, I cheered , I sympathized all because of Evelyn…I could so picture the setting and as Evelyn sets out to fool everyone, I thought you go girl !!...I don''t want to say anything else but what a fantastic read… I can''t recommend this book enough !!’ Goodreads Reviewer, 5 stars
	' || chr(10) || '‘What a magnificent read! Eva is amazing. One of the best characters in a book EVER! What a fantastic, beautiful, heart-wrenching tale, incredibly told. I absolutely loved every single page. I sat for the last 20% of the book in tears, sad but happy tears. An absolutely beautiful book.’ Kim the Bookworm, 5 stars
	' || chr(10) || '‘A poignant and evocative story of love, betrayal and bravery that kept me page turning and completely engrossed from start to finish. Loved it and would definitely recommend.’ NetGalley Reviewer, 5 stars
	' || chr(10) || '‘A phenomenal story of courage, love, murder and all the atrocities that go with war. Eva is an extraordinary character, strong, loyal, smart, funny, loving, and brave.A phenomenal read!!’ Goodreads Reviewer, 5 stars
	' || chr(10) || '‘This may be my new favorite book!!!! I absolutely love the premise of the heroine faking dementia in her retirement home to cover up her knowledge of (and possible involvement in) questionable activities centering around WWII events. The tempo of this novel was perfect--kept me wondering until the very last page!’ Goodreads Reviewer, 5 stars
	' || chr(10) || '‘Absolutely loved this book and its riveting plot!... The author has successfully penned a debut novel that I would highly recommend without any hesitation. An excellent debut novel from Suzanne Goldring and I look forward to reading more of her work .’ Goodreads Reviewer, 5 stars
	' || chr(10) || '‘This book was excellent! Totally kept my attention and I wanted to find out what would become of the main characters. Highly recommended.’ Goodreads Reviewer, 5 stars
	' || chr(10) || '‘Everything about this book is amazing. I love the main character Eva, the way the author integrates the past with the present, and the emotional plot that kept me hooked from the beginning to the end. I’ve read over 30 historical fiction novels in the past year and this one is definitely in my top 10.’ Netgalley Reviewer, 5 stars', 
	'Fiction', 'Bookouture', 'Anglais', 4),
	
	('IDN592', 'https://m.media-amazon.com/images/I/81+NuVptTyL._AC_UY218_ML3_.jpg', 'Hysteria: An Alexander Gregory Thriller', 'LJ Ross', 
	'FROM THE INTERNATIONAL BESTSELLING AUTHOR OF THE DCI RYAN MYSTERIES
	' || chr(10) || 'In a beautiful world, murder is always ugly…
	' || chr(10) || 'Recently returned from his last case in Ireland, elite forensic psychologist and criminal profiler Dr Alexander Gregory receives a call from the French police that he can’t ignore. It’s Paris fashion week and some of the world’s most beautiful women are turning up dead, their faces slashed in a series of frenzied attacks while the world’s press looks on.
	' || chr(10) || 'Amidst the carnage, one victim has survived but she’s too traumatised to talk. Without her help, the police are powerless to stop the killer before he strikes again – can Gregory unlock the secrets of her mind, before it’s too late?
	' || chr(10) || 'Murder and mystery are peppered with dark humour in this fast-paced thriller set amidst the spectacular Parisian landscape.
	' || chr(10) || '“LJ Ross keeps company with the best mystery writers” – The Times
	' || chr(10) || '“A literary phenomenon” – Evening Chronicle
	' || chr(10) || '“LJ Ross is the Queen of Kindle” – Sunday Telegraph', 
	'Romans Sentimental', 'Amazon Media EU', 'Anglais', 0),
	
	('XCAUAQ', 'https://m.media-amazon.com/images/I/91+AYjcI78L._AC_UY218_ML3_.jpg', 'Educated: The international bestselling memoir', 'Tara Westover', 
	'‘An amazing story, and truly inspiring. The kind of book everyone will enjoy. IT’S EVEN BETTER THAN YOU’VE HEARD.’ – Bill Gates
	' || chr(10) || 'Selected as a book of the year by AMAZON, THE TIMES, SUNDAY TIMES, GUARDIAN, NEW YORK TIMES, ECONOMIST, NEW STATESMAN, VOGUE, IRISH TIMES, IRISH EXAMINER and RED MAGAZINE
	THE MULTI-MILLION COPY BESTSELLER
	' || chr(10) || 'A Book of the Decade, 2010-2020 (Independent)
	________________________
	' || chr(10) || 'Tara Westover and her family grew up preparing for the End of Days but, according to the government, she didn’t exist. She hadn’t been registered for a birth certificate. She had no school records because she’d never set foot in a classroom, and no medical records because her father didn’t believe in hospitals.
	' || chr(10) || 'As she grew older, her father became more radical and her brother more violent. At sixteen, Tara knew she had to leave home. In doing so she discovered both the transformative power of education, and the price she had to pay for it.
	________________________
	' || chr(10) || '· From one of TIME magazine''s 100 most influential people of 2019
	' || chr(10) || '· Shortlisted for the 2018 BAMB Readers'' Awards
	' || chr(10) || '· Recommended as a summer read by Barack Obama, Antony Beevor, India Knight, Blake Morrison and Nina Stibbe', 
	'Biographie', 'Cornerstone Digital', 'Anglais', 5),
	
	('3TRCA0', 'https://m.media-amazon.com/images/I/51bTEZgsyrL._AC_UY218_ML3_.jpg', 'The Great Gatsby', 'F. Scott Fitzgerald', 
	'Now the subject of a major new film from director Baz Luhrmann (Romeo+Juliet, Moulin Rouge!), starring Leonardo DiCaprio and Carey Mulligan, The Great Gatsby is F. Scott Fitzgerald''s brilliant fable of the hedonistic excess and tragic reality of 1920s America. This Penguin Classics edition is edited with an introduction and notes by Tony Tanner.
	' || chr(10) || 'Young, handsome and fabulously rich, Jay Gatsby is the bright star of the Jazz Age, but as writer Nick Carraway is drawn into the decadent orbit of his Long Island mansion, where the party never seems to end, he finds himself faced by the mystery of Gatsby''s origins and desires. Beneath the shimmering surface of his life, Gatsby is hiding a secret: a silent longing that can never be fulfilled. And soon, this destructive obsession will force his world to unravel.
	In The Great Gatsby, Fitzgerald brilliantly captures both the disillusionment of post-war America and the moral failure of a society obsessed with wealth and status. But he does more than render the essence of a particular time and place, for - in chronicling Gatsby''s tragic pursuit of his dream - Fitzgerald re-creates the universal conflict between illusion and reality.
	' || chr(10) || 'Like Jay Gatsby, F. Scott Fitzgerald (1896–1940) has acquired a mythical status in American literary history, and his masterwork The Great Gatsby is considered by many to be the ''great American novel''. In 1920 he married Zelda Sayre, dubbed ''the first American Flapper'', and their traumatic marriage and Zelda''s gradual descent into insanity became the leading influence on his writing. As well as many short stories, Fitzgerald wrote five novels This Side of Paradise, The Great Gatsby, The Beautiful and the Damned, Tender is the Night and, incomplete at the time of his death, The Last Tycoon. After his death The New York Times said of him that ''in fact and in the literary sense he created a "generation" ''.
	' || chr(10) || '''A classic, perhaps the supreme American novel''
	John Carey, Sunday Times Books of the Century', 
	'Fiction', 'Penguin', 'Anglais', 3),
	
	('77D0CT', 'https://m.media-amazon.com/images/I/91D4YvdC0dL._AC_UY218_ML3_.jpg', 'Brave New World', 'Aldous Huxley', 
	'Aldous Huxley’s 1932 dystopian classic Brave New World predicts – with eerie clarity – a terrifying vision of the future, which feels ever closer to our own reality.
	' || chr(10) || '''The best science fiction book ever, definitely the most prescient…’ Yuval Noah Harari, author of Sapiens and Homo Deus
	' || chr(10) || '‘A masterpiece of speculation... As vibrant, fresh, and somehow shocking as it was when I first read it’ Margaret Atwood
	' || chr(10) || 'Far in the future, the World Controllers have created the ideal society. Through clever use of genetic engineering, brainwashing and recreational sex and drugs all its members are happy consumers. Bernard Marx seems alone harbouring an ill-defined longing to break free. A visit to one of the few remaining Savage Reservations where the old, imperfect life still continues, may be the cure for his distress...
	' || chr(10) || 'Huxley''s ingenious fantasy of the future sheds a blazing light on the present and is considered to be his most enduring masterpiece.
	' || chr(10) || 'WITH INTRODUCTIONS BY MARGARET ATWOOD AND DAVID BRADSHAWVerwechslungsgeschichte
	' || chr(10) || 'A grave warning... Provoking, stimulating, shocking and dazzling'' Observer', 
	'Littérature', 'Vintage', 'Anglais', 1),
	
	/* Livres en Allemand */
	('11Z8SD', 'https://m.media-amazon.com/images/I/81nydCOUSZL._AC_UY218_ML3_.jpg', 'Die Frau auf Nordstrand', 'Anna Johannsen', 
	'Ein sehr persönlicher Fall für die Inselkommissarin von Bestsellerautorin Anna Johannsen.
	' || chr(10) || 'Kurz vor Urlaubsende wird die LKA-Kommissarin Lena Lorenzen von ihrem Kollegen Ole Kotten zu Hilfe gerufen. Auf Nordstrand, einer Halbinsel vor Husum, ist eine Leiche gefunden worden. Die Autopsie ergibt, dass es sich um die aus Thailand stammende Lawan Yao Yun handelt, die vor mehreren Monaten von ihrem deutschen Ehemann als vermisst gemeldet wurde.
	' || chr(10) || 'Von Anfang an steht der Ehemann unter Verdacht. Doch selbst als immer mehr Indizien gegen ihn sprechen, ist Lena nicht davon überzeugt, dass er der Täter ist. Ihre Suche nach weiteren Ermittlungsansätzen entwickelt sich zu einem Wettlauf mit der Zeit ...', 
	'Romans Policier', 'Edition M', 'Allemand', 5),
	
	('1S1FM6', 'https://m.media-amazon.com/images/I/61uyF1fyDWL._AC_UY218_ML3_.jpg', 'Der Besuch Der Alten Dame', 'F. Durrenmatt', 
	'Claire Zachanassian, amerikanische Multimillionärin, kehrt in ihr Heimatdorf Güllen zurück, um sich zu rächen.', 
	'Fiction', 'Diogenes Verlag AG,Switzerland', 'Allemand', 4),
	
	('NC9B2A', 'https://m.media-amazon.com/images/I/41fIRZY0TPL._AC_UY218_ML3_.jpg', 'The Handmaid''s Tale', 'Margaret Atwood', 
	'', 
	'Fiction', 'Klett', 'Allemand', 4),
	
	('RQXQZ9', 'https://m.media-amazon.com/images/I/61QjUsl8GrL._AC_UY218_ML3_.jpg', 'Der kleine Prinz', 'Antoine de Saint-Exupéry', 
	'', 
	'Littérature Française', 'Anaconda', 'Allemand', 2),
	
	('ZA141Z', 'https://m.media-amazon.com/images/I/5105KSw5woL._AC_UY218_ML3_.jpg', 'Der Prinz und der Bottelknabe', 'Kirsten Boie', 
	'Warum ist Kevin Bottel nicht als Calvin Prinz geboren worden und Calvin Prinz als Kevin Bottel ?
	' || chr(10) || 'Eine Verwechslungsgeschichte mit Biss, zugleich eine Persiflage auf die Zufâlligkeiten des Schicksals.', 
	'Romans', 'Oetinger Friedrich GmbH', 'Allemand', 4),
	
	('EB0Z8G', 'https://m.media-amazon.com/images/I/51d9JmdoCBL._AC_UY218_ML3_.jpg', 'Leben des Galilei', 'Bertolt Brecht', 
	'Königs Erläuterungen Textanalyse und Interpretation mit ausführlicher Inhaltsangabe und Abituraufgaben
	' || chr(10) || 'Das spart dir lästiges Recherchieren und kostet weniger Zeit zur Vorbereitung.
	' || chr(10) || 'In einem Band bieten dir die neuen Königs Erläuterungen ALLES, was du zur Vorbereitung auf Referat, Klausur, Abitur oder Matura benötigst.', 
	'Romans Dramatique', 'Suhrkamp Verlag', 'Allemand', 1),
	
	('S6K8AW', 'https://m.media-amazon.com/images/I/516olXKHRqL._AC_UY218_ML3_.jpg', 'Der Vorleser: Romans', 'Bernhard Schlink', 
	'Sie ist reizbar, rätselhaft und viel älter als er... und sie wird seine erste Leidenschaft. Sie hütet verzweifelt ein Geheimnis. Eines Tages ist sie spurlos verschwunden. Erst Jahre später sieht er sie wieder. Die fast kriminalistische Erforschung einer sonderbaren Liebe und bedrängenden Vergangenheit.', 
	'Romans Policier', 'Diogenes Verlag AG,Switzerland', 'Allemand', 2),
	
	('HJMILL', 'https://m.media-amazon.com/images/I/51OoBpXItVL._AC_UY218_ML3_.jpg', 'Paule Glück. Das Jahrhundert in Geschichten', 'Klaus Kordon', 
	'"Klaus Kordon kann, was nicjt mehr selbstverständlich ist: eine Geschichte erzählen. Er tut es spannend ..."
	Basler Zeitung
	' || chr(10) || '1904: Jeden Morgen trägt Paule Zeitungen aus, um für seine Familie etwas hinzu zu verdienen. Doch dann wird der Vater arbeitslos und Paule muss in der Fabrik als Heizer anfangen.
	' || chr(10) || '1941: Für Wolf ändert sich vieles, als er plötzlich den gelben Stern tragen muss.
	' || chr(10) || '1984: Gabi und Katja gehen beide in Berlin zur Schule, die eine in Berlin-Ost, die andere in Berlin-West - in zwei völlig verschiedenen Welten.
	' || chr(10) || 'In sienen Geschichten spannt Klaus Kordon einen weiten Bogen über das 20. Jahr-hundert un lässt Zeigeschichte lebendig werden.',
	'Livre Documentaire', 'Beltz GmbH, Julius', 'Allemand', 3),
	
	('N0FQ0N', 'https://m.media-amazon.com/images/I/81Rt3nPa00L._AC_UY218_ML3_.jpg', 'Ruhm; Ein Romans in neun Geschichten', 'Daniel Kehlmann', 
	'Ruhm – Ein Romans in neun Geschichten ist ein 2009 auf Deutsch erschienenes erzählerisches Werk von Daniel Kehlmann. Die neun lose miteinander verbundenen Geschichten des Romans kreisen um Fragen der Kommunikation mit Mobiltelefon, Computer und Internet. Ihre Figuren tauchen auf und verschwinden wieder, verändern oder vertauschen ihre Identität, werden vergessen. Der im Titel angesprochene Ruhm zieht sich als Leitmotiv durch die meisten Kurzgeschichten des Romans. Bis Oktober 2011 hat sich der Romans allein in Deutschland über 700.000 Mal verkauft.', 
	'Romans Moderne', 'Rowohlt Taschenbuch Verlag GmbH', 'Allemand', 2),
	
	('FVBXD8', 'https://m.media-amazon.com/images/I/9172mxWK1EL._AC_UY218_ML3_.jpg', 'Die Sonnenschwester: Romans', 'Lucinda Riley', 
	'Reich, berühmt und bildschön: das ist Elektra d’Aplièse, die als Model ein glamouröses Leben in New York führt. Doch der Schein trügt – in Wahrheit ist sie eine verzweifelte junge Frau, die im Begriff ist, ihr Leben zu ruinieren. Da taucht eines Tages ihre Großmutter Stella auf, von deren Existenz Elektra nichts wusste. Sie ist ein Adoptivkind und kennt ihre Wurzeln nicht. Als Stella ihr die berührende Lebensgeschichte der jungen Amerikanerin Cecily Huntley-Morgan erzählt, öffnet sich für Elektra die Tür zu einer neuen Welt. Denn Cecily lebte in den 1940er Jahren auf einer Farm in Afrika – wo einst Elektras Schicksal seinen Anfang nahm …', 
	'Romans', '', 'Allemand', 3),
	
	('OZ3WSV', 'https://m.media-amazon.com/images/I/81oaSygbSsL._AC_UY218_ML3_.jpg', 'Der Mann auf der Hallig', 'Anna Johannsen', 
	'Ein außergewöhnlicher Fall für die Inselkommissarin Lena Lorenzen von #1-Kindle-Bestsellerautorin Anna Johannsen.
	' || chr(10) || 'Auf einer Sandbank vor Hallig Hooge wird die Leiche von Klaas Rieckert gefunden, der offensichtlich vor dem Ertrinken an Armen und Beinen gefesselt wurde. Die Obduktion bringt ein überraschendes Ergebnis: Die DNA des Halligbewohners wurde vor zehn Jahren im Rahmen eines aufsehenerregenden Falles im Polizeisystem registriert und weist ihn als mutmaßlichen Dreifachmörder aus.
	' || chr(10) || 'Obwohl Lena Lorenzen gerade mit ihrem Lebensgefährten Erck ihr privates Glück genießen könnte, will sie den Fall unbedingt selbst übernehmen. Sie ist überzeugt, dass eine große SoKo in der kleinen Welt der Hallig wenig Chancen hat, an relevante Informationen zu kommen. Nach den ersten Befragungen der Angehörigen muss sich die Inselkommissarin schnell entscheiden, welchen Ermittlungsansatz sie verfolgen will: Liegt der Schlüssel zur Aufklärung des Mordes im engen Beziehungsgeflecht auf Hooge oder in der dunklen Vergangenheit des Opfers auf dem Festland?', 
	'Romans Policier', 'Edition M', 'Allemand', 4),
	
	('AMVNBC', 'https://m.media-amazon.com/images/I/41UoZ9Rx+xL._AC_UY218_ML3_.jpg', 'Der Verdacht', 'Friedrich Durrenmatt', 
	'Kommissär Hans Bärlach, am Ende seiner Polizeikarriere angekommen und an Krebs leidend, erholt sich im Krankenhaus Salem von einer Operation. Dort wird er Zeuge, wie sein Freund, der Arzt Samuel Hungertobel, beim Anblick eines Fotos im Magazin Life erbleicht und leicht nervös wird. Der Abgebildete soll der deutsche Arzt Nehle sein, der im Konzentrationslager Stutthof bei Danzig grausame Operationen an Häftlingen vorgenommen hatte, ohne sie zu narkotisieren, und sich 1945 umgebracht hat. Hungertobel erklärt schließlich, eine große Ähnlichkeit zwischen Nehle und seinem Studienkollegen Fritz Emmenberger festgestellt zu haben, der während des Krieges angeblich in Chile weilte.
	' || chr(10) || 'Bärlach schöpft schließlich den Verdacht, dass Nehle und Emmenberger entweder die Rollen getauscht haben oder aber ein und dieselbe Person sein müssen. Er prüft Interpol-Dokumente und medizinische Artikel von Nehle bzw. Emmenberger, die damals in Chile publiziert worden waren. Ein Bekannter Bärlachs, der Jude Gulliver, war im Konzentrationslager Stutthof ein Opfer des Arztes. Zu Besuch im Krankenzimmer erzählt er von diesen Erlebnissen, während sie zusammen die Nacht durchzechen. Am wahrscheinlichsten erscheint Bärlach daraufhin, dass Emmenberger unter Nehles Namen die Verbrechen im Konzentrationslager begangen hat, nach dem Krieg in die Schweiz zurückgekehrt ist und nun unbehelligt die Privatklinik Sonnenstein bei Zürich führt. Diesen Verdacht lässt er von dem Journalisten Fortschig in dessen Zeitung „Apfelschuss“ veröffentlichen und begibt sich selbst unter dem falschen Namen Kramer als Patient in Emmenbergers Klinik, in der Hoffnung, Emmenberger psychisch unter Druck setzen zu können, bis der sich selbst verraten würde. Emmenberger stellt sich tatsächlich als der Täter heraus, doch Bärlachs Plan geht nicht auf, denn geschwächt, wie er ist, und gefangen in der Klinik, verliert Bärlach jegliche Kontrolle über die Situation. Emmenberger erweist sich als völlig skrupellos und wird von absolut ergebenen Mitarbeitern unterstützt. Bärlach erfährt, dass er Fortschig hat töten lassen und nun auch plant, Bärlach mittels einer seiner grausamen Operationen zu beseitigen. Doch im letzten Moment greift Gulliver ein, indem er Emmenberger tötet. Bärlach entkommt mit Gullivers Hilfe aus der Klinik Sonnenstein und wird von seinem Freund Hungertobel wieder zurück nach Bern gebracht.', 
	'Romans Policier', 'Diogenes Verlag AG,Switzerland', 'Allemand', 3),
	
	('0267TL', 'https://m.media-amazon.com/images/I/41ZDuSl-D+L._AC_UY218_ML3_.jpg', 'Das Parfum: Die Geschichte Eines Möders', 'Patrick Süskind', 
	'Ein rares Meisterwerk zeitgenössischer Prosa, eine dicht gesponnene, psychologisch raffiniert umgesetzte Erzählung, die an die frühen Stücke von Patricia Highsmith erinnert, in ihrer Kunstfertigkeit aber an die Novellistik großer europäischer Erzähltradition anknüpft.', 
	'Romans Dramatique', 'Diogenes Verlag AG,Switzerland', 'Allemand', 4),
	
	('318HRM', 'https://m.media-amazon.com/images/I/51bFK2-229L._AC_UY218_ML3_.jpg', 'Der hase mit der roten nase', 'Helme Heine', 
	'Es war einmal ein Hase mit einer roten Nase und einnem blauen Ohr.
	Das kommt ganz selten vor.', 
	'Livre Jeunesse', 'Beltz, Julius, GmbH & Co. KG', 'Allemand', 1),
	
	('O2K6BP', 'https://m.media-amazon.com/images/I/A1F6-ZGCdZL._AC_UY218_ML3_.jpg', 'Die Akte Vaterland: Gereon Raths vierter Fall', 'Volker Kutscher', 
	'Mysteriöse Mordserie führt Gereon Rath bis nach Masuren
	' || chr(10) || 'Juli 1932, die Berliner Polizei steht vor einem Rätsel: Ein Mann liegt tot im Lastenaufzug von »Haus Vaterland«, dem legendären Vergnügungstempel am Potsdamer Platz, und alles deutet darauf hin, dass er dort ertrunken ist.Kommissar Gereon Rath ist wenig erfreut über den neuen Fall, denn er hat schon genug Ärger. Seine Ermittlungen gegen einen mysteriösen Auftragsmörder, der die Stadt in Atem hält, treten seit Wochen auf der Stelle, seine große Liebe Charlotte »Charly« Ritter kehrt von einem Studienjahr in Paris zurück und fängt als Kommissaranwärterin am Alex an – ausgerechnet in der Mordkommission, was die Dinge nicht einfacher macht.Der Tote vom Potsdamer Platz scheint Teil einer Mordserie zu sein, deren Spur weit nach Osten führt. Während Charly als Küchenhilfe ins Haus Vaterland eingeschleust wird, ermittelt Rath in einer masurischen Kleinstadt nahe der polnischen Grenze und gerät in eine fremde Welt. Er macht Bekanntschaft mit wortkargen Ostpreußen, schwarzgebranntem Schnaps und den Tücken der Natur. Die Widerstände gegen den Ermittler aus Berlin wachsen, als er ein lang gehütetes Geheimnis aufzudecken droht.Volker Kutscher entwirft erneut eine packende und komplexe Geschichte vor dem Hintergrund der historischen Ereignisse. Während Straßenschlachten zwischen Nazis und Kommunisten immer mehr Todesopfer fordern, putscht Reichskanzler von Papen die demokratische Regierung Preußens aus dem Amt und mit ihr die Spitze der Berliner Polizei. Damit verschärft sich die Lage auch für Gereon Rath, der sich bisher der Protektion durch Polizeivizepräsident Bernhard Weiß sicher sein konnte …', 
	'Romans Policier', 'Amazon Media EU', 'Allemand', 3),
	
	('S70K5S', 'https://m.media-amazon.com/images/I/71K6cUy5PBL._AC_UY218_ML3_.jpg', 'Der Tod des Senators - Ein Hamburg-Krimi: Ein Fall für Brock', 'Hans-Jürgen Raben', 
	'Gerd Eggert, Wirtschaftssenator von Hamburg, wird von einem unbekannten Scharfschützen aus sehr großer Entfernung bei der Ausstellungseröffnung eines russischen Künstlers getötet, der bei diesem Anschlag ebenfalls verwundet wird. Da gefundene Beweismittel eindeutig Richtung Russland als Verantwortlichen für diesen Mord zeigen, stellt sich die Frage, ob Eggert wirklich das eigentliche Ziel war oder Andrej Sokolow, der junge regimekritische Künstler.
	' || chr(10) || 'Keine leichte Aufgabe für Hauptkommissar Cornelius Brock und sein Team, unter großem Druck der Politiker den oder die Schuldigen zu finden. Man geht schnell davon aus, dass der Schütze im Auftrag gehandelt hat. Aber wer ist dieser Auftraggeber und was sein Motiv?
	' || chr(10) || 'Und dann bekommen die Ermittler unverhoffte Unterstützung aus Russland, doch statt einer Lösung merklich näher zu kommen, werden weitere Fragen aufgeworfen, die den Fall immer verworrener machen und als unlösbar erscheinen lassen – für Brock eine unvorstellbare Möglichkeit …', 
	'Romans Policier', 'Uksak E-Books', 'Allemand', 1),
	
	('WQC3HR', 'https://m.media-amazon.com/images/I/81eAwzzQwzL._AC_UY218_ML3_.jpg', 'Die Kälte im Herzen: Schwedenkrimi', 'Ana Dee', 
	'Ein Inselhotel, eine tote Architektin und reichlich Stoff für Spekulationen.
	' || chr(10) || 'Linnea Bergström erhält von Kian Bensson, dem Chef der Redaktion, den Auftrag, über eine Hoteleröffnung der Luxusklasse zu berichten. Ludvig Stolt, der Hotelier, stellt ihr eine Suite zur Verfügung und hofft auf positive Resonanz. Doch die erhofften erholsamen Tage entpuppen sich als Albtraum und kurz darauf wird die Architektin tot aufgefunden.
	Linnea versucht, die Zusammenhänge herauszufinden und begibt sich dabei mehr als einmal in Lebensgefahr. Erst nach und nach wird offensichtlich, mit welch gefährlichem Gegner sie es tatsächlich zu tun hat.
	Kriminalroman mit mystischen Elementen.', 
	'Romans Policier', 'Amazon Media EU', 'Allemand', 1),
	
	('E1SKWQ', 'https://m.media-amazon.com/images/I/91YW1h0JJTL._AC_UY218_ML3_.jpg', 'Momo: Schulausgabe', 'Michael Ende', 
	'Weltbestseller, Klassiker, Kultbuch – für Mädchen und Jungen ab 12 Jahren
	' || chr(10) || 'Momo, ein kleines struppiges Mädchen, lebt am Rande einer Großstadt in den Ruinen eines Amphitheaters. Sie besitzt nichts als das, was sie findet oder was man ihr schenkt, und eine außergewöhnliche Gabe: Sie hört Menschen zu und schenkt ihnen Zeit. Doch eines Tages rückt das gespenstische Heer der grauen Herren in die Stadt ein. Sie haben es auf die kostbare Lebenszeit der Menschen abgesehen und Momo ist die Einzige, die der dunklen Macht der Zeitdiebe noch Einhalt gebieten kann ...
	' || chr(10) || 'Michael Endes Märchen-Romans voller Poesie und Herzenswärme über den Zauber der Zeit
	' || chr(10) || 'Weltweiter Bestseller – übersetzt in 46 Sprachen, über 10 Millionen verkaufte Bücher!', 
	'Romans', 'Thienemann Verlag in der Thienemann-Esslinger Verl', 'Allemand', 4),
	
	('LAOIOV', 'https://m.media-amazon.com/images/I/91gILL2JmjL._AC_UY218_ML3_.jpg', 'Harry Potter Und der Stein der Weisen', 'J. K. Rowling', 
	'Er konnte nicht wissen, dass in ebendiesem Moment überall im Land geheime Versammlungen stattfanden. Gläser erhoben wurden und gedämpfte Stimmen sagten: "Auf Harry Potter - den Jengen, der lebt!"
	' || chr(10) || 'Eigentlich hatte Harry geglaubt, er sei ein ganz normaler Junge. Zumindest bis zu seinem elften Geburtstag. Da erfährt er, sich an der Schule für Hexerei und Zauberei einfinden soil. Und Warren? Weil Harry ein Zauberer ist. Und so wird für Harry das erste Jarh in der Schule das spannendste, aufregendtse und lustigste in seinem Leben. Er stürzt von einem Abenteuer in die nächste ungeheuerliche Geschichte, muss gegen Bestien, Mitsschüler und Fabelwesen kämpfen. Da ist es gut, dass er schon Freunde gefunden hat, dir ihm im Kampf gegen die dunklen Mächte zur Seite stehen.', 
	'Romans Fantastique', 'Carlsen Verlag GmbH', 'Allemand', 5),
	
	('EG2BBA', 'https://m.media-amazon.com/images/I/518bD46szlL._AC_UY218_ML3_.jpg', 'Marina', ' Carlos Ruiz Zafón', 
	'"Einfach grandios!"
	BZ am Sonntag
	' || chr(10) || 'Als Óscar Drai das Mädchen Marina trifft, ahnt er nicht, dass sie sein Leben für immer verändern wird. Mit ihrem Vater lebt sie in einer alten Villa wie in einer vergangenen Zeit. Marina bringt Óscar auf die Spur einer mysteriösen Dame in Schwarz.
	Erstmals beschwört Carlos Ruiz Zafón in "Marina" sein unnachahmliches Barcelona herauf, eine Stadt voller Magie und Leidenschaft.
	' || chr(10) || '"Ein Romans wie ein Labyrinth. Hinter jeder Seite erwartet einen ein neuer Fortgang der Geschichte.
	Sie werden ''Marina'' in atemberaubender Geschwindigkeit lesen."
	Alex Deugler, deuglers-buchkritik.de
	' || chr(10) || '"Man kann dieses Buch sogar dem später verfassten ''Schatten des Windes'' vorziehen."
	Christoph Haas, Süddeutsche Zeitung
	' || chr(10) || '"Carlos Ruiz Zafón erzählt mit viel Poesie die dramatische Geschichte eines jungen Mannes, der um sein Glück und seine große Liebe kämpft."
	Emotion', 
	'Romans Sentimental', 'FISCHER Taschenbuch', 'Allemand', 2),
	
	('3HXIPL', 'https://m.media-amazon.com/images/I/71UiI9PHHmL._AC_UY218_ML3_.jpg', 'Inselnebel', 'Rieke Husmann', 
	'Ingeborg Wittmeier meldet ihre achtzehnjährige Tochter Antonia als vermisst, die nach einem Wochenendbesuch bei ihrer Freundin nicht zurückgekehrt ist. Hauptkommissarin Hella Brandt und ihr junger Kollege Lars Mattes erkennen nach den ersten Zeugenbefragungen, dass es sich bei Antonia nicht um eine jugendliche Ausreißerin handeln kann.
	Mit Hochdruck arbeiten sie sich an dem Fall, der sie auf die ostfriesische Insel Norderney führt. Hier nehmen die Ermittlungen schnell Fahrt auf und fordern Hella Brandts ganze Aufmerksamkeit, die privat mit den dunklen Seiten ihrer Vergangenheit konfrontiert wird und obendrein befürchtet, schwanger zu sein.', 
	'Romans Policier', 'Amazon Media EU', 'Allemand', 4),
	
	('NSKTPT', 'https://m.media-amazon.com/images/I/81WcwEulByL._AC_UY218_ML3_.jpg', 'Der Trümmermörder: Kriminalroman', 'Cay Rademacher', 
	'Kommissar Staves erster Fall
	' || chr(10) || 'Hamburg 1947: Die Stadt liegt in Trümmern, und es ist einer der kältesten Winter des Jahrhunderts. Die Menschen versuchen irgendwie zu überleben. Da wird mitten in der Trümmerlandschaft eine Leiche entdeckt: eine junge Frau, nackt, kein Hinweis auf den Mörder. Oberinspektor Stave hat kaum Hoffnung, den Fall aufzuklären, auch wenn ihm Lothar Maschke von der Sitte und Lieutenant MacDonald von der britischen Verwaltung zur Seite gestellt werden. Bald werden weitere Tote entdeckt, und Stave ist für jede Hilfe dankbar, die er auf der Suche nach einem grausamen Mörder bekommt.
	' || chr(10) || 'Cay Rademacher lässt in einem hochspannenden authentischen Kriminalfall das Hamburg des Hungerwinters 1946/47 lebendig werden.', 
	'Romans Policier', 'Amazon Media EU', 'Allemand', 5),
	
	('T6CQ4N', 'https://m.media-amazon.com/images/I/51wuw4oI3KL._AC_UY218_ML3_.jpg', 'Poppers: Das Handbuch zur schwulen Sexdroge', 'Micha Schulze', 
	'Um Poppers ranken sich viele Geheimnisse und Gerüchte. Kaum einer weiß, woraus es eigentlich besteht, warum es so wirkt, wie es wirkt, und wie gefährlich es tatsächlich ist. Bekannt in der schwulen Welt, dringt es aber auch zunehmend in Hetero Kreise ein. Die wenigsten kennen die Gesetzeslage: Darf man Poppers in Deutschland, Österreich und der Schweiz erwerben und besitzen, ohne sich strafbar zu machen? Und kann man vom „Raumduft“-Schnüffeln eigentlich süchtig werden? Das Handbuch beantwortet all diese Fragen, klärt über die verschiedenen Poppers-Sorten auf, warnt vor Gefahren und Wechselwirkungen mit anderen Rauschmitteln, gibt Tipps für den verantwortungsvollen Gebrauch, erklärt, ob und wie man es selbst herstellen kann, und nennt die günstigsten Bezugsquellen. In dem Buch kommen Ärzte, Apotheker und Sexualwissenschaftler ebenso zu Wort wie schwule Promis, die offen und tabulos über ihre Poppers-Erfahrungen berichten. Mit Beiträgen und Statements von den Comiczeichnern Ralf König und Stefan Zeh, Cazzo-Star Tim Vincent, Mr. Gay Germany 2004 Suat Bahceci, Pornoproduzent Alexander Roessner, Georg Roth alias Sister George und dem Fotografen Henning von Berg."', 
	'Livre Documentaire', 'Production House GmbH Abteilung Himmelstuermer', 'Allemand', 0),
	
	('IBHQBJ', 'https://m.media-amazon.com/images/I/41+IZ-dHIZL._AC_UY218_ML3_.jpg', 'Der geteilte Himmel', 'Christa Wolf', 
	'Ende August 1961: In einem kleinen Krankenhauszimmer erwacht Rita Seidel aus ihrer Ohnmacht. Und mit dem Erwachen wird auch die Vergangenheit wieder lebendig. Da ist die Erinnerung an den Betriebsunfall und vor allem die Erinnerung an Manfred Herrfurth. Zwei Jahre sind vergangen, seit sie dem Chemiker in die Stadt folgte, um an seiner Seite und mit ihm gemeinsam ein glückliches Leben zu beginnen. Wann hat die Trennung begonnen? Hat sie die ersten Anzeichen einer Entfremdung übersehen? Denken, Grübeln, Fiebern - Tage und Nächte hindurch! Ich gebe Dir Nachricht, wenn Du kommen sollst. Ich lebe nur für den Tag, da Du wieder bei mir bist. Manfred ist von einem Chemikerkongreß in Westberlin nicht zurückgekehrt in dem festen Glauben, daß Rita ihm folgen wird. Sie muß eine Entscheidung treffen, die sie in eine tiefe Krise stürzt.', 
	'Fiction', 'Deutscher Taschenbuch Verlag GmbH & Co', 'Allemand', 3),
	
	('KYZQLT', '', 'Hure unterm Hakenkreuz', 'Iris Krumbiegel', 
	'Mila Stern, die junge Tänzerin, hat nur einen Wunsch. Sie möchte auf der ganz großen Bühne stehen und am Staatstheater aufgenommen werden. Doch gerade als sich dieser Traum zu erfüllen beginnt, kommen die Nationalsozialisten an die Macht. Plötzlich scheint die Zukunft eines jüdischen Mädchens keinen Wert mehr zu besitzen.
	Mila, die inzwischen in Berlin lebt, wird alles genommen. Ihre Eltern, ihre Karriere und sogar ihre Unschuld. Mit dem Gefühl, nichts mehr zu verlieren zu haben, entscheidet sie sich zu einem schwerwiegenden Schritt. Sie möchte, dass die Menschen, die ihr das Wertvollste geraubt haben, dafür bezahlen und möglicherweise kann sie so auch ihr Leben retten.', 
	'Livre Documentaire', 'Amazon Media EU', 'Allemand', 5),
	
	/* Livres en Espagnol */
	('G36IN2', 'https://m.media-amazon.com/images/I/41TBbh1e11L._AC_UY218_ML3_.jpg', 'Bakhita', 'Véronique Olmi', 
	'Ella desconoce cómo se llama. No sabe en qué lengua sueña. Recuerda palabras en árabe, en turco, en italiano, habla además varios dialectos. Muchos provienen de Sudán y uno, de Venecia. La gente dice: «Un galimatías». Habla un galimatías y se la entiende mal. Hay que decírselo todo de nuevo con palabras diferentes, palabras que no conoce… Le han pedido a menudo que relate su vida, y ella la ha contado una y otra vez, desde el principio. Es el principio, tan terrible, lo que les interesa. Se la ha contado en su galimatías, y así es como ha vuelto su memoria: diciendo, en orden cronológico, lo que era tan lejano y doloroso… Su memoria volvió con el relato, pero su nombre nunca pudo recordarlo. Jamás supo cómo se llamaba. Sin embargo, eso no es lo más importante, porque quién era de niña, cuando llevaba el nombre que le puso su padre, nunca lo olvidó. En su interior guarda, como un tesoro de la infancia, a la pequeña que fue. Aquella niña que debería haber muerto en la esclavitud sobrevivió; aquella niña era y sigue siendo lo que jamás nadie logró arrebatarle. Véronique Olmi es una reconocida novelista, dramaturga y actriz francesa. Con esta obra ha obtenido el Premio FNAC de novela y ha sido finalista de los premios Femina y Goncourt 2017.', 
	'Romans Dramatique', 'Ediciones Sígueme', 'Espagnol', 2),
	
	('3M1CSP', 'https://m.media-amazon.com/images/I/81itKzgbe2L._AC_UY218_ML3_.jpg', 'After', 'Anna Todd', 
	'Tessa Young se enfrenta a su primer año en la universidad. Acostumbrada a una vida estable y ordenada, su mundo se tambalea cuando conoce a Hardin, un chico tan guapo como cruel, inquietante, lleno de tatuajes, y de aparente mala vida. Desde el primer momento se odian. Pertenecen a dos mundos distintos, pero pronto se harán más que amigos y nada volverá a ser igual. Hardin y Tessa deberán enfrentar muchas pruebas para estar juntos. La inocencia, el despertar a la vida, el descubrimiento del sexo... las huellas de un amor tan poderoso como la fuerza del destino. La historia de un amor infinito.', 
	'Romans Sentimental', 'Planeta Publishing', 'Espagnol', 1),
	
	('N6DQJL', 'https://m.media-amazon.com/images/I/81rmt4sEIfL._AC_UY218_ML3_.jpg', 'Asterios Polyp', 'David Mazzucchelli', 
	'La obra más importante de uno de los grandes talentos de la novela gráfica en su vertiente más artística e innovadora.
	' || chr(10) || '¿QUIÉN ES ASTERIOS POLYP? Arquitecto, profesor, escritor, marido... aunque todo eso pertenece al pasado. Ahora, el día en que cumple medio siglo, se ha convertido en una sombra de sí mismo. No obstante, cuando esa misma noche se desata una tormenta, la caída de un rayo lo embarca en un viaje trascendental.
	' || chr(10) || 'Reconocido como uno de los grandes autores del cómic, David Mazzucchelli ha creado con mano maestra una historia cautivante que explora los límites de la novela gráfica y la lleva hacia territorios insospechados.
	' || chr(10) || '** Dos Premios Eisner 2010
	' || chr(10) || '** Los Angeles Times Book Prize 2009
	' || chr(10) || '** Escogida como uno de los Mejores Libros del Año 2009 por The New York Times
	' || chr(10) || 'La crítica ha dicho...
	«Esta historia de amor, ambición y oportunidades perdidas, tan absorbente como idiosincrática, supone el regreso de uno de los maestros modernos de la narración gráfica.»
	The Miami Herald
	' || chr(10) || '«Es de un virtuosismo excepcional: divertido, desgarrador y estimulante para la mente.»
	San Francisco Chronicle
	' || chr(10) || '«Una pieza de entretenimiento deslumbrante, construida a la perfección [...].»
	The New York Times Book Review
	' || chr(10) || '«Los recursos visuales de Mazzucchelli no se limitan a ilustrar la narración: comunican por sí mismos todo un marco conceptual. Todos y cada uno de los elementos tienen su razón de ser, y analizarlos uno a uno recompensará diez veces el esfuerzo.»
	The Portland Mercury
	' || chr(10) || '«Se trata de un libro de envergadura, provocador, que mezcla la riqueza de la novela tradicional con el mejor arte moderno.»
	The Boston Globe
	' || chr(10) || '«La obra maestra de Mazzucchelli no es en absoluto una lectura fácil... pero sí trascendente.»
	The Austin Chronicle', 
	'Fiction', 'Salamandra', 'Espagnol', 5),
	
	('BGKHXW', 'https://m.media-amazon.com/images/I/91T05xND0ZL._AC_UY218_ML3_.jpg', 'El Laberinto de los Espíritus', 'Carlos Ruiz Zafón', 
	'En la Barcelona de finales de los años 50, Daniel Sempere ya no es aquel niño que descubrió un libro que habría de cambiarle la vida entre los pasadizos del Cementerio de los Libros Olvidados. El misterio de la muerte de su madre Isabella ha abierto un abismo en su alma del que su esposa Bea y su fiel amigo Fermín intentan salvarle.
	' || chr(10) || 'Justo cuando Daniel cree que está a un paso de resolver el enigma, una conjura mucho más profunda y oscura de lo que nunca podría haber imaginado despliega su red desde las entrañas del Régimen. Es entonces cuando aparece Alicia Gris, un alma nacida de las sombras de la guerra, para conducirlos al corazón de las tinieblas y desvelar la historia secreta de la familia… aunque a un terrible precio.
	' || chr(10) || 'El Laberinto de los Espíritus es un relato electrizante de pasiones, intrigas y aventuras. A través de sus páginas llegaremos al gran final de la saga iniciada con La Sombra del Viento, que alcanza aquí toda su intensidad y calado, a la vez que dibuja un gran homenaje al mundo de los libros, al arte de narrar historias y al vínculo mágico entre la literatura y la vida.', 
	'Fiction', 'Editorial Planeta', 'Espagnol', 0),
	
	('E3E834', 'https://m.media-amazon.com/images/I/511fkoUhe1L._AC_UY218_ML3_.jpg', 'Simplemente Tini', 'Martina Stoessel', 
	'Aquí encontrarán todo sobre mí; eso que tuve ganas de escribir para ustedes porque sé que tenemos una conexión especial. Es mi mejor intento por abrir mi corazón y mostrarme un poco más. Muchos me recordarán sólo por interpretar a Violetta y siempre será un honor, pero soy Martina y me encantaría que me conozcan como soy,', 
	'Romans', 'Planeta Argentina', 'Espagnol', 4),
	
	('OJWW4C', 'https://m.media-amazon.com/images/I/41iVubQjs0L._AC_UY218_ML3_.jpg', 'Veinte Poemas De Amor Y Una Cancion.....', 'PABLO NERUDA', 
	'Una de las obras más célebres del poeta Pablo Neruda.
	' || chr(10) || '«Me gusta cuando callas porque estás como ausente.
	Distante y dolorosa como si hubieras muerto.
	Una palabra entonces, una sonrisa bastan.
	Y estoy alegre, alegre de que no sea cierto.»
	' || chr(10) || 'Reseña:
	«Neruda significa un hombre nuevo en la América, una sensibilidad con la cual abre todo capítulo emocional americano. Su alta categoría arranca de su rotunda diferenciación.»
	Gabriela Mistral', 
	'Poésie', 'Nuevas Ediciones de Bolsillo', 'Espagnol', 0),
	
	('V0YE4F', 'https://m.media-amazon.com/images/I/81YB5rHmwrL._AC_UY218_ML3_.jpg', 'Contes', 'Charles Perrault', 
	'Charles Perrault (París, 12 de enero de 1628-ibídem, 16 de mayo de 1703) fue un escritor francés, principalmente reconocido por haber dado forma literaria a cuentos clásicos infantiles como Piel de asno, Pulgarcito, Barba Azul, Cenicienta, La bella durmiente, Caperucita Roja y El gato con botas, atemperando en muchos casos la crudeza de las versiones orales.', 
	'Fiction', 'CreateSpace Independent Publishing Platform', 'Espagnol', 2),
	
	('2B4NXA', 'https://m.media-amazon.com/images/I/81s9HrM0-7L._AC_UY218_ML3_.jpg', 'Hernani', 'Victor Hugo', 
	'El romanticismo llegó tarde a Francia. Hernani (1830), de Victor Hugo (1802-85), creó una gran tormenta de protestas pero luego ganó la aceptación. Esta fue la obra que marcó el triunfo del romanticismo sobre el clasicismo. Fue Hugo quien dirigió un grupo de jóvenes poetas y artistas que virtualmente emprendieron la guerra contra los tradicionalistas en la noche de apertura de esta obra, creando un escándalo que garantizó el éxito del trabajo y el éxito eventual del movimiento.', 
	'Littérature Française', 'CreateSpace Independent Publishing Platform', 'Espagnol', 1),
	
	('RFBX67', 'https://m.media-amazon.com/images/I/71k3KwMlt8L._AC_UY218_ML3_.jpg', 'El tiempo entre costuras', 'MARIA DUEÑAS', 
	'La joven modista Sira Quiroga abandona Madrid en los meses previos al alzamiento, arrastrada por el amor desbocado hacia un hombre a quien apenas conoce. Juntos se instalan en Tánger, una ciudad mundana, exótica y vibrante donde todo lo impensable puede hacerse realidad. Incluso, la traición y el abandono.
	' || chr(10) || 'Sola y acuciada por deudas ajenas, Sira se traslada a Tetuán, la capital del Protectorado español en Marruecos. Con argucias inconfesables y ayudada por amistades de reputación dudosa, forja una nueva identidad y logra poner en marcha un selecto atelier en el que atiende a clientas de orígenes remotos y presentes insospechados.
	' || chr(10) || 'A partir de entonces, con la contienda española recién terminada y la europea a punto de comenzar, el destino de la protagonista queda ligado a un puñado de personajes históricos entre los que destacan Juan Luis Beigbeder ―el enigmático y escasamente conocido ministro de Asuntos Exteriores del primer franquismo―, su amante, la excéntrica Rosalinda Fox, y el agregado naval Alan Hillgarth, jefe de la inteligencia británica en España durante la segunda guerra mundial. Entre todos ellos la empujarán hacia un arriesgado compromiso en el que las telas, las puntadas y los patrones de su oficio se convertirán en la fachada visible de algo mucho más turbio y peligroso.
	' || chr(10) || 'Escrita en una prosa espléndida, El tiempo entre costuras avanza con ritmo imparable por los mapas, la memoria y la nostalgia, transportándonos hasta los legendarios enclaves coloniales del norte de África, al Madrid proalemán de la inmediata posguerra y a una Lisboa cosmopolita repleta de espías, oportunistas y refugiados sin rumbo.
	' || chr(10) || 'El tiempo entre costuras es una aventura apasionante en la que los talleres de alta costura, el glamour de los grandes hoteles, las conspiraciones políticas y las oscuras misiones de los servicios secretos se funden con la lealtad hacia aquellos a quienes queremos y con el poder irrefrenable del amor.', 
	'Fiction', 'Ediciones Temas de Hoy', 'Espagnol', 4),
	
	('9RO8ZT', 'https://m.media-amazon.com/images/I/71TIryVqOpL._AC_UY218_ML3_.jpg', 'Cronica de una muerte anunciada', 'GABRIEL GARCIA MARQUEZ', 
	'«El día en que lo iban a matar, Santiago Nasar se levantó a las 5.30 de la mañana para esperar el buque en que llegaba el obispo.»
	' || chr(10) || 'Acaso sea Crónica de una muerte anunciada la obra más «realista» de Gabriel García Márquez, pues se basa en un hecho histórico acontecido en la tierra natal del escritor. Cuando empieza la novela, ya se sabe que los hermanos Vicario van a matar a Santiago Nasar -de hecho, ya le han matado- para vengar el honor ultrajado de su hermana Ángela, pero el relato termina precisamente en el momento en que Santiago Nasar muere.
	' || chr(10) || 'El tiempo cíclico, tan utilizado por García Márquez en sus obras, reaparece aquí minuciosamente descompuesto en cada uno de sus momentos, reconstruido prolija y exactamente por el narrador, que va dando cuenta de lo que sucedió mucho tiempo atrás, que avanza y retrocede en su relato y hasta llega mucho tiempo después para contar el destino de los supervivientes. La acción es, a un tiempo, colectiva y personal, clara y ambigua, y atrapa al lector desde un principio, aunque este conozca el desenlace de la trama. La dialéctica entre mito y realidad se ve potenciada aquí, una vez más, por una prosa tan cargada de fascinación que la eleva hasta las fronteras de la leyenda.
	' || chr(10) || 'Otros autores opinan...
	«Un genio.»
	Julio Cortázar
	' || chr(10) || '«La suya es una devoción sin límites por las letras, desorbitada, febril, insistente, insomne entrega a las secretas maravillas de la palabra escrita.»
	Álvaro Mutis
	' || chr(10) || '«Su mundo era el mío, traducido al español. No es extraño que me enamorara de él, no por su magia, sino por su realismo.»
	Salman Rushdie
	' || chr(10) || '«La voz garciamarquiana alcanza aquí un nivel en el que resulta a la vez clásica y coloquial, opalescente y pura, capaz de alabar y maldecir, de reír y llorar, de fabular y cantar, de despegar y volar cuando es necesario.»
	Thomas Pynchon', 
	'Romans', 'Nuevas Ediciones de Bolsillo', 'Espagnol', 3),
	
	('FGGM5D', 'https://m.media-amazon.com/images/I/81kEqozyudL._AC_UY218_ML3_.jpg', 'La Reina del Sur', 'Arturo Pérez-Reverte', 
	'Un thriller apasionante
	' || chr(10) || '«Sonó el teléfono y supo que la iban a matar. Lo supo con tanta certeza que se quedó inmóvil, la cuchilla en alto, el cabello pegado a la cara entre el vapor del agua caliente que goteaba en los azulejos. Bip-bip. Se quedó muy quieta, conteniendo el aliento como si la inmovilidad o el silencio pudieran cambiar el curso de lo que ya había ocurrido. Bip-bip. Estaba en la bañera, depilándose la pierna derecha, el agua jabonosa por la cintura, y su piel desnuda se erizó igual que si acabara de reventar el grifo del agua fría. Bip-bip. En el estéreo del dormitorio, los Tigres del Norte cantaban historias de Camelia la Tejana. La traición y el contrabando, decían, son cosas compartidas.»
	' || chr(10) || 'La crítica ha dicho...
	«El encuentro de John Le Carré con Gabriel García Márquez. Pérez-Reverte tiene un enorme grupo de seguidores que sigue creciendo.»
	The Wall Street Journal
	' || chr(10) || '«Es un contador de historias increíble.»
	USA Today
	' || chr(10) || '«Entretenida como pocas, hermosa por su construcción e inquietante por la grave problemática que late debajo de la aventura criminal. A esta radiografía del delito añade además el autor un canto a la propia literatura.»
	El Cultural
	' || chr(10) || '«Pasen y lean. El espectáculo está no solamente servido sino asegurado.»
	El País
	' || chr(10) || '«Junten el desencanto neorromántico de Barry Gifford con la maestría narrativa de Juan Marsé y añadan una banda sonora con el desgarro de Chavela Vargas. El resultado de semejante mezcolanza ya tiene título: La Reina del Sur.»
	El Periódico de Cataluña', 
	'Romans Policier', 'Debolsillo', 'Espagnol', 3),
	
	('01VJPX', 'https://m.media-amazon.com/images/I/61W99mX2M2L._AC_UY218_ML3_.jpg', 'El camino', 'Miguel Delibes', 
	'Daniel el Mochuelo intuye a sus once años que su camino está en la aldea, junto a sus amigos, sus gentes y sus pájaros. Pero su padre quiere que vaya a la ciudad a estudiar el Bachillerato. A lo largo de la noche que precede a la partida, Daniel, insomne, con un nudo en la garganta, evocará sus correrías con sus amigos —Roque el Moñigo y Germán el Tiñoso— a través de los campos descubriendo el cielo y la tierra, y revivirá las andanzas de la gente sencilla de la aldea. La simpatía humana con que esa mirada infantil nos introduce en el pueblo, haciéndonos conocer toda una impresionante galería de tipos y la fuerza con que a través de rasgos frecuentemente caricaturescos se nos presentan siempre netos y vivos es uno de los mayores aciertos de esta novela. 
	' || chr(10) || 'Feliz evocación de un tiempo cuyo encanto y fascinación advertimos cuando ya se nos ha escapado entre los dedos, El camino es, por su amalgama de nitidez realista, humor sutil, nostalgia contenida e irisación poética no sólo una de las mejores novelas de Miguel Delibes, sino también, como señalaba la crítica, «una de las obras maestras de la narrativa contemporánea».', 
	'Romans', 'Ediciones Destino', 'Espagnol', 1),
	
	('X4RZOP', 'https://m.media-amazon.com/images/I/813UU+ma1XL._AC_UY218_ML3_.jpg', 'Cuentos educar niños felices', 'Begoña Ibarrola Lopez de Davalillo, Jesús Gabán Bravo', 
	'Este libro se divide en 10 puntos que la autora considera fundamentales para educar niños felices: darles amor incondicional, desarrollar su autoestima, impulsar su autonomía, desarrollar la confianza en sí mismo, valorar su esfuerzo y constancia, vivir con honestidad y sinceridad, respetar su individualidad, saber aplicar límites y normas, aportarle seguridad y educarle en paz y tranquilidad.Cada apartado contiene una explicación sobre el punto que se está tratando, dos cuentos relacionados con él y una ficha para que los padres puedan trabajarla con sus hijos.', 
	'Livre Jeunesse', 'EDICIONES SM', 'Espagnol', 2),
	
	('X6SFMW', 'https://m.media-amazon.com/images/I/31uBk0dSNqL._AC_UY218_ML3_.jpg', 'Mujeres Que Corren Con los Lobos', ' CLARISSA PINKOLA ESTES', 
	'Dentro de toda mujer, incluso de la más reprimida, alienta una vida secreta, una fuerza poderosa llena de buenos instintos, creatividad apasionada y sabiduría eterna. Es la Mujer Salvaje, una especie en peligro de extinción que representa la esencia femenina instintiva. En Mujeres que corren con los lobos, la autora revela una serie de mitos interculturales, cuentos de hadas e historias para ayudar a las mujeres a recuperar su fuerza y su salud, atributos visionarios de esta esencia instintiva.', 
	'Psychologie et Spirituel', 'Suma de Letras Suma de Letras', 'Espagnol', 5),
	
	('710LGH', 'https://m.media-amazon.com/images/I/81vC9y-qVKL._AC_UY218_ML3_.jpg', 'El tango de la Guardia Vieja', 'Arturo Pérez-Reverte', 
	'Hay personajes que te miran una vez y se quedan para siempre en tu vida.
	El tango de la Guardia Vieja, la nueva novela de Arturo Pérez-Reverte
	' || chr(10) || 'La trepidante aventura de Max Costa y Mecha Inzunza les lleva a recorrer tres escenarios distintos del convulso siglo XX: pasarán en 1928 por Buenos Aires gracias a una extraña apuesta entre dos músicos, en Niza se verán envueltos en medio de una trama de espionaje durante los años de la Guerra Civil española y los primeros vientos de la segunda Guerra Mundial, para terminar en Sorrento, en los años sesenta, embarcados en una inquietante partida de ajedrez.
	' || chr(10) || 'El tango de la Guardia Vieja narra con pulso admirable una turbia y apasionada historia de amor, traiciones e intrigas, que se prolonga durante cuatro décadas a través de un siglo convulso y fascinante, bajo la luz crepuscular de una época que se extingue.
	' || chr(10) || 'Un libro que apasionará a los lectores habituales de Pérez-Reverte, pero que también atraerá a un nuevo público que gusta del espionaje y la estrategia, y sabrá apreciar esta particular historia de amor.
	' || chr(10) || 'RESEÑAS:
	' || chr(10) || '«Arturo Pérez-Reverte cambia de registro, aunque sin salir de su mundo personal, y escribe una gran historia de amor. [...] La novela es también un amplio y documentadísimo fresco de la historia de una Europa desvanecida, la de los años veinte, treinta y sesenta. [...] Junto al amor y los sentimientos, unas páginas de sorprendente erotismo con una fuerte carga sexual. Y también grandes dosis perezrevertianas de aventura.»
	El País
	' || chr(10) || '«La mejor novela de su autor.»
	Sergio Vila-Sanjuán, La Vanguardia
	' || chr(10) || '«Eros, vida, deseo y aventura, todo ello contado por Arturo Pérez-Reverte de la mejor forma posible.»
	Enrique Turpin, La Vanguardia
	' || chr(10) || '«Asombroso compendio de amor y aventuras... Épica de los cuerpos y belleza entre las sábanas...»
	Jacinto Antón, Babelia
	' || chr(10) || '«Un logro, una novela feliz.»
	Justo Navarro, Babelia
	' || chr(10) || 'CITAS DEL LIBRO:
	' || chr(10) || '«Una pareja de jóvenes apuestos, acuciados por pasiones urgentes como la vida, se mira a los ojos al bailar un tango aún no escrito, en el salón silencioso y desierto de un transatlántico que navega en la noche. Trazando sin saberlo, al moverse abrazados, la rúbrica de un mundo irreal cuyas luces fatigadas empiezan a apagarse para siempre.»
	' || chr(10) || 'Él: Max Costa
	«Mantuvo siempre el compás impecable en una pista, las manos serenas y ágiles fuera de ella, y en los labios la frase apropiada, la réplica oportuna, brillante. Eso lo hacía simpático a los hombres y admirado por las mujeres. En aquel entonces, además de los bailes de salón que le servían para ganarse la vida, dominaba como nadie el arte de crear fuegos artificiales con las palabras y dibujar melancólicos paisajes con los silencios.»
	' || chr(10) || 'Ella: Mecha Inzunza
	«Parecía salir de las páginas selectas de una revista ilustrada: lucía collar largo de perlas y pendientes a juego. Esbelta, tranquila, caminando firme sobre tacones altos en el suave balanceo de la nave, su cuerpo imprimía líneas rectas y prolongadas, casi interminables, a un vestido verde jade largo y ligero, que desnudaba sus brazos, hombros y espalda hasta la cintura.»
	' || chr(10) || '«Se desafiaron Mecha y él con la mirada. Qué diablos pretendes, era la pregunta silenciosa del bailarín mundano. El desdén de ella bastó como respuesta. Puedes jugar, decía el gesto. Pedir más cartas o retirarte. Dependerá de tu curiosidad o tu coraje. Y ya conoces el premio.
	@perezreverte
	#guardiavieja', 
	'Romans Sentimental', 'ALFAGUARA', 'Espagnol', 0),
	
	('JDZBSZ', 'https://m.media-amazon.com/images/I/91tVhGQ9tSL._AC_UY218_ML3_.jpg', '1984', 'George Orwell', 
	'Edición especial para escuelas de la emblemática novela de Orwell 1984. Incluye material didáctico a cargo de Maribel Cruzado Soria.
	' || chr(10) || '«No creo que la sociedad que he descrito en 1984 necesariamente llegue a ser una realidad, pero sí creo que puede llegar a existir algo parecido», escribía Orwell después de publicar su novela. Corría el año 1948, y la realidad se ha encargado de convertir esa pieza -entonces de ciencia ficción- en un manifiesto de la realidad.
	' || chr(10) || 'En el año 1984 Londres es una ciudad lúgubre en la que la Policía del Pensamiento controla de forma asfixiante la vida de los ciudadanos. Winston Smith es un peón de este engranaje perverso y su cometido es reescribir la historia para adaptarla a lo que el Partido considera la versión oficial de los hechos. Hasta que decide replantearse la verdad del sistema que los gobierna y somete.
	' || chr(10) || 'La crítica ha dicho...
	«Aquí ya no estamos solo ante lo que habitualmente reconocemos como "literatura" e identificamos con la buena escritura. Aquí estamos, repito, ante energía visionaria. Y no todas las visiones se refieren al futuro, o al Más Allá.»
	Umberto Eco
	' || chr(10) || '«Entre mis libros favoritos, lo leo una y otra vez.»
	Margaret Atwood
	' || chr(10) || '«No es difícil pensar que Orwell, en 1984, estuviera imaginando un futuro para la generación de su hijo, un mundo del que deseaba prevenirles.»
	Thomas Pynchon
	' || chr(10) || '«La libertad es una obligación tan dolorosa que siempre habrá quien prefiera rendirse. La virtud de libros como 1984 es su capacidad para recordarnos que la libertad de los seres humanos responsables no es igual a la de los animales.»
	Anthony Burgess
	' || chr(10) || '«Desde El proceso de Kafka ninguna obra fantástica ha alcanzado el horror lógico de 1984.»
	Arthur Koestler
	' || chr(10) || '«Un libro magnífico y profundamente interesante.»
	Aldous Huxley', 
	'Romans Politique', 'Debolsillo', 'Espagnol', 3),
	
	('FUSI9V', 'https://m.media-amazon.com/images/I/61rMRDP4trL._AC_UY218_ML3_.jpg', 'Carmilla', 'Ana Juan Sheridan Le Fanu', 
	'En una noche de su niñez, Laura despertó para descubrir que no estaba sola: desde un costado de su cama, el rostro bello y solemne de una mujer la contemplaba. Hechizada por la sonrisa de su visitante, Laura volvió a dormir. De pronto, dos agujas parecieron hundirse en su pecho... Ahora Laura es una joven, sensible y solitaria. Aislada en el viejo castillo de su padre, anhela compañía y amistad. Su padre ha ofrecido hospedaje a una viajera misteriosa, que podría ser la amiga que necesita. La atracción y el miedo se confunden en el corazón de Laura al descubrir el rostro de su huésped: es la aparición de su infancia.', 
	'Romans Fantastique', 'Fondo de Cultura Economica USA', 'Espagnol', 3),
	
	('8L6H9S', 'https://m.media-amazon.com/images/I/91AigDA1wcL._AC_UY218_ML3_.jpg', 'Un millon de gotas', 'Víctor del Árbol', 
	'Gonzalo Gil es un abogado metido en una vida que le resulta ajena, en una carrera malograda que trata de esquivar la constante manipulación de su omnipresente suegro, un personaje todopoderoso de sombra muy alargada. Pero algo va a sacudir esa monotonía.
	Tras años sin saber de ella, Gonzalo recibe la noticia de que su hermana Laura se ha suicidado en dramáticas circunstancias. Su muerte obliga a Gonzalo a tensar hasta límites insospechados el frágil hilo que sostiene el equilibrio de su vida como padre y esposo. Al involucrarse decididamente en la investigación de los pasos que han llevado a su hermana al suicidio, descubrirá que Laura es la sospechosa de haber torturado y asesinado a un maﬁoso ruso que tiempo atrás secuestró y mató a su hijo pequeño.
	Pero lo que parece una venganza es solo el principio de un tortuoso camino que va a arrastrar a Gonzalo a espacios inéditos de su propio pasado y del de su familia que tal vez hubiera preferido no afrontar.
	Tendrá que adentrarse de lleno en la fascinante historia de su padre, Elías Gil, el gran héroe de la resistencia contra el fascismo, el joven ingeniero asturiano que viajó a la URSS comprometido con los ideales de la revolución, que fue delatado, detenido y conﬁnado en la pavorosa isla de Nazino, y que se convirtió en personaje clave, admirado y temido, de los años más oscuros de nuestro país.
	' || chr(10) || 'Una gran historia de ideales traicionados, de vidas zarandeadas por un destino implacable, una visceral y profunda historia de amor perdurable y de venganza postergada; un intenso thriller literario que recorre sin dar respiro la historia europea.', 
	'Romans Policier', 'PLANETA', 'Espagnol', 1),
	
	('LT63UM', 'https://m.media-amazon.com/images/I/81F5cdWZeYL._AC_UY218_ML3_.jpg', 'Los besos en el pan', 'Almudena Grandes', 
	'¿Qué puede llegar a ocurrirles a los vecinos de un barrio cualquiera en estos tiempos difíciles? ¿Cómo resisten, en pleno ojo del huracán, parejas y personas solas, padres e hijos, jóvenes y ancianos, los embates de una crisis que «amenazó con volverlo todo del revés y aún no lo ha conseguido»? Los besos en el pan cuenta, de manera sutil y conmovedora, cómo transcurre la vida de una familia que vuelve de vacaciones decidida a que su rutina no cambie, pero también la de un recién divorciado al que se oye sollozar tras un tabique, la de una abuela que pone el árbol de Navidad antes de tiempo para animar a los suyos, la de una mujer que decide reinventarse y volver al campo para vivir de las tierras que alimentaron a sus antepasados… En la peluquería, en el bar, en las oficinas o en el centro de salud, muchos vecinos, protagonistas de esta delicada novela coral, vivirán momentos agridulces de una solidaridad inesperada, de indignación y de rabia, pero también de ternura y tesón. Y aprenderán por qué sus abuelos les enseñaron, cuando eran niños, a besar el pan.', 
	'Psychologie et Spirituel', 'Tusquets Editores', 'Espagnol', 0),
	
	('ELSUQP', 'https://m.media-amazon.com/images/I/817qGxbmmjL._AC_UY218_ML3_.jpg', 'Largo pétalo de mar', 'Isabel Allende', 
	'La fascinante nueva novela de Isabel Allende.
	' || chr(10) || 'En plena Guerra Civil española, el joven médico Víctor Dalmau, junto a su amiga pianista Roser Bruguera, se ven obligados a abandonar Barcelona, exiliarse y cruzar los Pirineos rumbo a Francia. A bordo del Winnipeg, un navío fletado por el poeta Pablo Neruda que llevó a más de dos mil españoles rumbo a Valparaíso, embarcarán en busca de la paz y la libertad que no tuvieron en su país. Recibidos como héroes en Chile -ese «largo pétalo de mar y nieve», en palabras del poeta chileno-, se integrarán en la vida social del país durante varias décadas hasta el golpe de Estado que derrocó al doctor Salvador Allende, amigo de Victor por su común afición al ajedrez. Víctor y Roser se encontrarán nuevamente desarraigados, pero como dice la autora: «si uno vive lo suficiente, todos los círculos se cierran».
	' || chr(10) || 'Un viaje a través de la historia del siglo XX de la mano de unos personajes inolvidables que descubrirán que en una sola vida caben muchas vidas y que, a veces, lo difícil no es huir sino volver.', 
	'Romans d''Aventure', 'PLAZA & JANES', 'Espagnol', 2),
	
	('GN148F', 'https://m.media-amazon.com/images/I/81lLe0rotlL._AC_UY218_ML3_.jpg', 'El Pollo Pepe', 'Nick Denchfield', 
	'El pollo Pepe está creciendo mucho porque es muy tragón. Un divertido libro con pop-ups para niños a partir de 1 año.', 
	'Livre Jeunesse', 'Ediciones Sm', 'Espagnol', 1),
	
	('UP4KKI', 'https://m.media-amazon.com/images/I/410WsmyOkVL._AC_UY218_ML3_.jpg', 'Inés del alma mia', 'Isabel Allende', 
	'Inés Suárez no es más que una humilde costurera extremeña hasta que, empeñada en encontrar a su esposo, perdido en sueños de gloria al otro lado del Atlántico, se embarca hacia América.', 
	'Fiction', 'Debolsillo', 'Espagnol', 1),
	
	('Y7PWAQ', 'https://m.media-amazon.com/images/I/71JEnJG5oRL._AC_UY218_ML3_.jpg', 'Como Agua Para Chocolate', 'Laura Esquivel', 
	'Terrenal, mágico y absolutamente encantador, este relato de la vida familiar en el México finisecular se convirtió, con la mezcla acertada de romance doloroso e ingenio agridulce, en un fenómeno de best-seller. La clásica historia de amor se sitúa en el rancho De la Garza, mientras la dueña tiránica Mamá Elena corta cebolla en la mesa de cocina durante sus últimos días de embarazo. Aún dentro del útero de su madre, la futura hija llora tan violentamente que causa un parto prematuro y la pequeña Tita nace entre las especies para preparar sopa de fideos. Este temprano encuentro con la comida pronto se convierte en una forma de vida. Tita se convierte en una chef maestra y, a lo largo de la historia, comparte puntos especiales de sus recetas favoritas con los lectores.
	' || chr(10) || 'La edición en español del best-seller Como agua para chocolate es, con toda razón, un notable éxito. Ahora, en esta edición en pasta blanda, miles de nuevos lectores podrán participar en el suntuoso, romántico y divertido relato de Tita, la extraordinaria cocinera que siempre pone algo extra especial en su salsa.', 
	'Fiction', 'Vintage Espanol', 'Espagnol', 5),
	
	('K5WWKM', 'https://m.media-amazon.com/images/I/41lKSKkRKIL._AC_UY218_ML3_.jpg', 'Naufragios', 'Alvar Nuñez Cabeza de Vaca', 
	'Alvar Núñez Cabeza de Vaca viajó por primera vez a América en 1527 con la desgraciada expedición de Panfilo de Narváez a la península de Florida, de la cual tardó diez años en volver tras haber protagonizado una increíble aventura, narrada en Naufragios. No obstante las penurias que había soportado regresó al Nuevo Mundo en 1540 como Adelantado del Río de la Plata; estuvo cinco años en el Paraguay, pero las intrigas locales pudieron más que su habilidad política y terminó de vuelta en España encadenado y procesado. Su segunda obra, Comentarios, es precisamente la versión que Alvar Núñez le dictó a Pedro Hernández, sobre todo lo sucedido durante la misión en el Paraguay. En 1551 se lo condenó al destierro en Oran, si bien posteriormente fue absuelto y rehabilitado. Alvar Núñez Cabeza de Vaca, el cronista descubridor de las cataratas del Iguazú, falleció en Valladolid en una fecha incierta entre 1556 y 1559.', 
	'Romans d''Aventure', 'CreateSpace Independent Publishing Platform', 'Espagnol', 1),
	
	('WOSL2D', 'https://m.media-amazon.com/images/I/81p8J+PYGyL._AC_UY218_ML3_.jpg', 'Mafalda. Todas las tiras', 'Quino', 
	'Las tiras de Mafalda en un solo volumen.
	' || chr(10) || 'Polémica, concienciada y encantadora. Así es el personaje más entrañable del catálogo de Lumen.
	' || chr(10) || 'Ahora, por primera vez, presentamos en un solo volumen todas las tiras de la niña argentina más famosa de todos los tiempos. De la primera a la última tira, podrás disfrutar de Mafalda, sus amigos y su familia.
	' || chr(10) || 'La crítica ha dicho...
	«Mafalda, esa niña que, a estas alturas, ya debería ser Patrimonio de la Humanidad. Básica para la vuelta a la rutina.»
	El País
	' || chr(10) || '«No tiene importancia lo que yo pienso sobre Mafalda. Lo importante es lo que Mafalda piensa de mí.»
	Julio Cortázar
	' || chr(10) || '«Mafalda es una heroína de nuestro tiempo.»
	Umberto Eco
	' || chr(10) || '«Creo que, junto con El Eternauta, Mafalda es la historieta más influyente. Es un icono argentino como el alfajor, Fangio, Gardel o el Obelisco. Mafalda está en el subconsciente argentino.»
	Liniers
	' || chr(10) || '«Afortunadamente, no me puedo imaginar a Mafalda sino como la graciosa niña que fue, es y será siempre. Los personajes de historieta tienen ese privilegio (enarbolado por Peter Pan) de no envejecer.»
	Roberto Fontanarrosa
	' || chr(10) || '«Mafalda es, digámoslo de una vez, más humana que muchos seres humanos.»
	Román Gubern', 
	'Bande Dessinée', 'LUMEN', 'Espagnol', 3);
	
	
	
	
/*
	ADDING LIBRARIES
*/
insert into library
	(name, address)
values
	('Bibliothèque Louise Walser-Gaillard (Chaptal)', '26 Rue Chaptal 75009 Paris'),
	('Librairimage', '53 Rue de la Rochefoucauld, 75009 Paris'),
	('Institut de France Fondation Dosne-Thiers', '27 Place St Georges, 75009 Paris'),
	('Médiathèque musicale de Paris (MMP)', '8 porte St-Eustache, 75001 Paris'),
	('Bibliothèque Assia Djebar', '1 Rue Reynaldo Hahn, 75020 Paris');




/*
	ADDING STOCK
*/
insert into stock
	(library_number_ref, book_reference, quantity, quantity_borrowed)
values
	(1, 'DWVKBC', 17, 0),
	(1, 'F4BOEX', 11, 0),
	(1, 'UEGU93', 5, 0),
	(1, 'D6FDSR', 6, 0),
	(1, '9DXU4I', 16, 0),
	(1, 'JSJ4FS', 11, 0),
	(1, '3IZSM1', 10, 0),
	(1, 'ZYA778', 9, 0),
	(1, 'XNR5H9', 19, 0),
	(1, '8CNN21', 20, 0),
	(1, 'MRPG6J', 11, 0),
	(1, 'XRBPH8', 20, 0),
	(1, 'RTNXAV', 15, 0),
	(1, 'G26FIY', 16, 0),
	(1, 'F81KZP', 20, 0),
	(1, 'MUJHY0', 10, 0),
	(1, '31J4C9', 17, 0),
	(1, '7GHACO', 11, 0),
	(1, 'V62V6Q', 18, 0),
	(1, 'O69MQG', 13, 0),
	(1, 'A84LZ6', 11, 0),
	(1, '68NT01', 15, 0),
	(1, '059270', 11, 0),
	(1, 'XKSSIP', 11, 0),
	(1, 'OB2ZY5', 15, 0),
	(1, '8R230I', 16, 0),
	(1, 'BDVGXM', 7, 0),
	(1, '7VMPPJ', 12, 0),
	(1, 'HLY0Z9', 12, 0),
	(1, 'DP2A2M', 10, 0),
	(1, 'A3GITZ', 12, 0),
	(1, '02JZ2C', 14, 0),
	(1, 'DC79B4', 10, 0),
	(1, 'Z8C9X2', 15, 0),
	(1, '4CLC14', 17, 0),
	(1, 'ZD3V9R', 15, 0),
	(1, '4XAWX5', 12, 0),
	(1, '843GIA', 19, 0),
	(1, 'Y7OWIE', 3, 0),
	(1, 'ZLU1NO', 18, 0),
	(1, 'LCAALA', 13, 0),
	(1, 'HVICQZ', 12, 0),
	(1, 'QWM28D', 4, 0),
	(1, 'S0PMF4', 10, 0),
	(1, 'KLEW5P', 14, 0),
	(1, 'XVGHOA', 8, 0),
	(1, 'IDN592', 11, 0),
	(1, 'XCAUAQ', 17, 0),
	(1, '3TRCA0', 7, 0),
	(1, '77D0CT', 5, 0),
	(1, '11Z8SD', 7, 0),
	(1, '1S1FM6', 6, 0),
	(1, 'NC9B2A', 16, 0),
	(1, 'RQXQZ9', 11, 0),
	(1, 'ZA141Z', 15, 0),
	(1, 'EB0Z8G', 7, 0),
	(1, 'S6K8AW', 3, 0),
	(1, 'HJMILL', 11, 0),
	(1, 'N0FQ0N', 4, 0),
	(1, 'FVBXD8', 17, 0),
	(1, 'OZ3WSV', 13, 0),
	(1, 'AMVNBC', 1, 0),
	(1, '0267TL', 3, 0),
	(1, '318HRM', 4, 0),
	(1, 'O2K6BP', 18, 0),
	(1, 'S70K5S', 8, 0),
	(1, 'WQC3HR', 7, 0),
	(1, 'E1SKWQ', 16, 0),
	(1, 'LAOIOV', 14, 0),
	(1, 'EG2BBA', 8, 0),
	(1, '3HXIPL', 2, 0),
	(1, 'NSKTPT', 19, 0),
	(1, 'T6CQ4N', 14, 0),
	(1, 'IBHQBJ', 4, 0),
	(1, 'KYZQLT', 10, 0),
	(1, 'G36IN2', 5, 0),
	(1, '3M1CSP', 18, 0),
	(1, 'N6DQJL', 13, 0),
	(1, 'BGKHXW', 10, 0),
	(1, 'E3E834', 18, 0),
	(1, 'OJWW4C', 20, 0),
	(1, 'V0YE4F', 6, 0),
	(1, '2B4NXA', 2, 0),
	(1, 'RFBX67', 16, 0),
	(1, '9RO8ZT', 11, 0),
	(1, 'FGGM5D', 13, 0),
	(1, '01VJPX', 18, 0),
	(1, 'X4RZOP', 15, 0),
	(1, 'X6SFMW', 8, 0),
	(1, '710LGH', 13, 0),
	(1, 'JDZBSZ', 18, 0),
	(1, 'FUSI9V', 12, 0),
	(1, '8L6H9S', 10, 0),
	(1, 'LT63UM', 10, 0),
	(1, 'ELSUQP', 16, 0),
	(1, 'GN148F', 7, 0),
	(1, 'UP4KKI', 15, 0),
	(1, 'Y7PWAQ', 12, 0),
	(1, 'K5WWKM', 6, 0),
	(1, 'WOSL2D', 1, 0),
	
	(2, 'DWVKBC', 5, 0),
	(2, 'F4BOEX', 13, 0),
	(2, 'UEGU93', 7, 0),
	(2, 'D6FDSR', 19, 0),
	(2, '9DXU4I', 5, 0),
	(2, 'JSJ4FS', 17, 0),
	(2, '3IZSM1', 11, 0),
	(2, 'ZYA778', 10, 0),
	(2, 'XNR5H9', 17, 0),
	(2, '8CNN21', 7, 0),
	(2, 'MRPG6J', 3, 0),
	(2, 'XRBPH8', 16, 0),
	(2, 'RTNXAV', 1, 0),
	(2, 'G26FIY', 13, 0),
	(2, 'F81KZP', 14, 0),
	(2, 'MUJHY0', 16, 0),
	(2, '31J4C9', 20, 0),
	(2, '7GHACO', 19, 0),
	(2, 'V62V6Q', 11, 0),
	(2, 'O69MQG', 2, 0),
	(2, 'A84LZ6', 11, 0),
	(2, '68NT01', 20, 0),
	(2, '059270', 18, 0),
	(2, 'XKSSIP', 14, 0),
	(2, 'OB2ZY5', 8, 0),
	(2, '8R230I', 11, 0),
	(2, 'BDVGXM', 9, 0),
	(2, '7VMPPJ', 19, 0),
	(2, 'HLY0Z9', 20, 0),
	(2, 'DP2A2M', 4, 0),
	(2, 'A3GITZ', 16, 0),
	(2, '02JZ2C', 6, 0),
	(2, 'DC79B4', 9, 0),
	(2, 'Z8C9X2', 4, 0),
	(2, '4CLC14', 15, 0),
	(2, 'ZD3V9R', 18, 0),
	(2, '4XAWX5', 4, 0),
	(2, '843GIA', 5, 0),
	(2, 'Y7OWIE', 2, 0),
	(2, 'ZLU1NO', 11, 0),
	(2, 'LCAALA', 4, 0),
	(2, 'HVICQZ', 8, 0),
	(2, 'QWM28D', 4, 0),
	(2, 'S0PMF4', 10, 0),
	(2, 'KLEW5P', 7, 0),
	(2, 'XVGHOA', 3, 0),
	(2, 'IDN592', 5, 0),
	(2, 'XCAUAQ', 3, 0),
	(2, '3TRCA0', 15, 0),
	(2, '77D0CT', 4, 0),
	(2, '11Z8SD', 8, 0),
	(2, '1S1FM6', 9, 0),
	(2, 'NC9B2A', 3, 0),
	(2, 'RQXQZ9', 20, 0),
	(2, 'ZA141Z', 17, 0),
	(2, 'EB0Z8G', 4, 0),
	(2, 'S6K8AW', 3, 0),
	(2, 'HJMILL', 6, 0),
	(2, 'N0FQ0N', 12, 0),
	(2, 'FVBXD8', 9, 0),
	(2, 'OZ3WSV', 10, 0),
	(2, 'AMVNBC', 16, 0),
	(2, '0267TL', 8, 0),
	(2, '318HRM', 13, 0),
	(2, 'O2K6BP', 19, 0),
	(2, 'S70K5S', 4, 0),
	(2, 'WQC3HR', 19, 0),
	(2, 'E1SKWQ', 13, 0),
	(2, 'LAOIOV', 2, 0),
	(2, 'EG2BBA', 17, 0),
	(2, '3HXIPL', 4, 0),
	(2, 'NSKTPT', 3, 0),
	(2, 'T6CQ4N', 14, 0),
	(2, 'IBHQBJ', 3, 0),
	(2, 'KYZQLT', 4, 0),
	(2, 'G36IN2', 6, 0),
	(2, '3M1CSP', 3, 0),
	(2, 'N6DQJL', 13, 0),
	(2, 'BGKHXW', 2, 0),
	(2, 'E3E834', 3, 0),
	(2, 'OJWW4C', 17, 0),
	(2, 'V0YE4F', 3, 0),
	(2, '2B4NXA', 5, 0),
	(2, 'RFBX67', 17, 0),
	(2, '9RO8ZT', 7, 0),
	(2, 'FGGM5D', 3, 0),
	(2, '01VJPX', 6, 0),
	(2, 'X4RZOP', 18, 0),
	(2, 'X6SFMW', 20, 0),
	(2, '710LGH', 19, 0),
	(2, 'JDZBSZ', 1, 0),
	(2, 'FUSI9V', 15, 0),
	(2, '8L6H9S', 8, 0),
	(2, 'LT63UM', 16, 0),
	(2, 'ELSUQP', 2, 0),
	(2, 'GN148F', 20, 0),
	(2, 'UP4KKI', 19, 0),
	(2, 'Y7PWAQ', 1, 0),
	(2, 'K5WWKM', 14, 0),
	(2, 'WOSL2D', 9, 0),
	
	(3, 'DWVKBC', 3, 0),
	(3, 'F4BOEX', 20, 0),
	(3, 'UEGU93', 10, 0),
	(3, 'D6FDSR', 12, 0),
	(3, '9DXU4I', 3, 0),
	(3, 'JSJ4FS', 10, 0),
	(3, '3IZSM1', 19, 0),
	(3, 'ZYA778', 8, 0),
	(3, 'XNR5H9', 15, 0),
	(3, '8CNN21', 7, 0),
	(3, 'MRPG6J', 12, 0),
	(3, 'XRBPH8', 1, 0),
	(3, 'RTNXAV', 20, 0),
	(3, 'G26FIY', 19, 0),
	(3, 'F81KZP', 6, 0),
	(3, 'MUJHY0', 3, 0),
	(3, '31J4C9', 8, 0),
	(3, '7GHACO', 5, 0),
	(3, 'V62V6Q', 14, 0),
	(3, 'O69MQG', 15, 0),
	(3, 'A84LZ6', 8, 0),
	(3, '68NT01', 0, 0),
	(3, '059270', 2, 0),
	(3, 'XKSSIP', 18, 0),
	(3, 'OB2ZY5', 11, 0),
	(3, '8R230I', 4, 0),
	(3, 'BDVGXM', 16, 0),
	(3, '7VMPPJ', 7, 0),
	(3, 'HLY0Z9', 18, 0),
	(3, 'DP2A2M', 9, 0),
	(3, 'A3GITZ', 19, 0),
	(3, '02JZ2C', 1, 0),
	(3, 'DC79B4', 10, 0),
	(3, 'Z8C9X2', 6, 0),
	(3, '4CLC14', 14, 0),
	(3, 'ZD3V9R', 12, 0),
	(3, '4XAWX5', 3, 0),
	(3, '843GIA', 11, 0),
	(3, 'Y7OWIE', 12, 0),
	(3, 'ZLU1NO', 18, 0),
	(3, 'LCAALA', 15, 0),
	(3, 'HVICQZ', 5, 0),
	(3, 'QWM28D', 7, 0),
	(3, 'S0PMF4', 3, 0),
	(3, 'KLEW5P', 17, 0),
	(3, 'XVGHOA', 18, 0),
	(3, 'IDN592', 10, 0),
	(3, 'XCAUAQ', 11, 0),
	(3, '3TRCA0', 13, 0),
	(3, '77D0CT', 16, 0),
	(3, '11Z8SD', 2, 0),
	(3, '1S1FM6', 6, 0),
	(3, 'NC9B2A', 11, 0),
	(3, 'RQXQZ9', 8, 0),
	(3, 'ZA141Z', 9, 0),
	(3, 'EB0Z8G', 5, 0),
	(3, 'S6K8AW', 0, 0),
	(3, 'HJMILL', 11, 0),
	(3, 'N0FQ0N', 1, 0),
	(3, 'FVBXD8', 5, 0),
	(3, 'OZ3WSV', 11, 0),
	(3, 'AMVNBC', 1, 0),
	(3, '0267TL', 3, 0),
	(3, '318HRM', 12, 0),
	(3, 'O2K6BP', 14, 0),
	(3, 'S70K5S', 8, 0),
	(3, 'WQC3HR', 5, 0),
	(3, 'E1SKWQ', 9, 0),
	(3, 'LAOIOV', 15, 0),
	(3, 'EG2BBA', 19, 0),
	(3, '3HXIPL', 6, 0),
	(3, 'NSKTPT', 9, 0),
	(3, 'T6CQ4N', 11, 0),
	(3, 'IBHQBJ', 5, 0),
	(3, 'KYZQLT', 6, 0),
	(3, 'G36IN2', 16, 0),
	(3, '3M1CSP', 2, 0),
	(3, 'N6DQJL', 11, 0),
	(3, 'BGKHXW', 5, 0),
	(3, 'E3E834', 15, 0),
	(3, 'OJWW4C', 6, 0),
	(3, 'V0YE4F', 19, 0),
	(3, '2B4NXA', 11, 0),
	(3, 'RFBX67', 5, 0),
	(3, '9RO8ZT', 19, 0),
	(3, 'FGGM5D', 9, 0),
	(3, '01VJPX', 2, 0),
	(3, 'X4RZOP', 14, 0),
	(3, 'X6SFMW', 6, 0),
	(3, '710LGH', 14, 0),
	(3, 'JDZBSZ', 5, 0),
	(3, 'FUSI9V', 10, 0),
	(3, '8L6H9S', 15, 0),
	(3, 'LT63UM', 16, 0),
	(3, 'ELSUQP', 15, 0),
	(3, 'GN148F', 13, 0),
	(3, 'UP4KKI', 20, 0),
	(3, 'Y7PWAQ', 7, 0),
	(3, 'K5WWKM', 5, 0),
	(3, 'WOSL2D', 19, 0),
	
	(4, 'DWVKBC', 11, 0),
	(4, 'F4BOEX', 5, 0),
	(4, 'UEGU93', 15, 0),
	(4, 'D6FDSR', 10, 0),
	(4, '9DXU4I', 14, 0),
	(4, 'JSJ4FS', 10, 0),
	(4, '3IZSM1', 14, 0),
	(4, 'ZYA778', 12, 0),
	(4, 'XNR5H9', 11, 0),
	(4, '8CNN21', 4, 0),
	(4, 'MRPG6J', 0, 0),
	(4, 'XRBPH8', 9, 0),
	(4, 'RTNXAV', 13, 0),
	(4, 'G26FIY', 8, 0),
	(4, 'F81KZP', 13, 0),
	(4, 'MUJHY0', 16, 0),
	(4, '31J4C9', 5, 0),
	(4, '7GHACO', 10, 0),
	(4, 'V62V6Q', 16, 0),
	(4, 'O69MQG', 4, 0),
	(4, 'A84LZ6', 18, 0),
	(4, '68NT01', 5, 0),
	(4, '059270', 8, 0),
	(4, 'XKSSIP', 15, 0),
	(4, 'OB2ZY5', 12, 0),
	(4, '8R230I', 14, 0),
	(4, 'BDVGXM', 12, 0),
	(4, '7VMPPJ', 3, 0),
	(4, 'HLY0Z9', 14, 0),
	(4, 'DP2A2M', 4, 0),
	(4, 'A3GITZ', 19, 0),
	(4, '02JZ2C', 10, 0),
	(4, 'DC79B4', 7, 0),
	(4, 'Z8C9X2', 1, 0),
	(4, '4CLC14', 9, 0),
	(4, 'ZD3V9R', 8, 0),
	(4, '4XAWX5', 5, 0),
	(4, '843GIA', 4, 0),
	(4, 'Y7OWIE', 16, 0),
	(4, 'ZLU1NO', 12, 0),
	(4, 'LCAALA', 16, 0),
	(4, 'HVICQZ', 6, 0),
	(4, 'QWM28D', 5, 0),
	(4, 'S0PMF4', 9, 0),
	(4, 'KLEW5P', 2, 0),
	(4, 'XVGHOA', 13, 0),
	(4, 'IDN592', 1, 0),
	(4, 'XCAUAQ', 13, 0),
	(4, '3TRCA0', 10, 0),
	(4, '77D0CT', 14, 0),
	(4, '11Z8SD', 18, 0),
	(4, '1S1FM6', 20, 0),
	(4, 'NC9B2A', 6, 0),
	(4, 'RQXQZ9', 8, 0),
	(4, 'ZA141Z', 20, 0),
	(4, 'EB0Z8G', 15, 0),
	(4, 'S6K8AW', 17, 0),
	(4, 'HJMILL', 0, 0),
	(4, 'N0FQ0N', 16, 0),
	(4, 'FVBXD8', 4, 0),
	(4, 'OZ3WSV', 7, 0),
	(4, 'AMVNBC', 13, 0),
	(4, '0267TL', 4, 0),
	(4, '318HRM', 1, 0),
	(4, 'O2K6BP', 2, 0),
	(4, 'S70K5S', 10, 0),
	(4, 'WQC3HR', 5, 0),
	(4, 'E1SKWQ', 8, 0),
	(4, 'LAOIOV', 9, 0),
	(4, 'EG2BBA', 11, 0),
	(4, '3HXIPL', 17, 0),
	(4, 'NSKTPT', 8, 0),
	(4, 'T6CQ4N', 2, 0),
	(4, 'IBHQBJ', 16, 0),
	(4, 'KYZQLT', 13, 0),
	(4, 'G36IN2', 5, 0),
	(4, '3M1CSP', 7, 0),
	(4, 'N6DQJL', 2, 0),
	(4, 'BGKHXW', 17, 0),
	(4, 'E3E834', 7, 0),
	(4, 'OJWW4C', 13, 0),
	(4, 'V0YE4F', 2, 0),
	(4, '2B4NXA', 17, 0),
	(4, 'RFBX67', 16, 0),
	(4, '9RO8ZT', 1, 0),
	(4, 'FGGM5D', 20, 0),
	(4, '01VJPX', 18, 0),
	(4, 'X4RZOP', 6, 0),
	(4, 'X6SFMW', 2, 0),
	(4, '710LGH', 5, 0),
	(4, 'JDZBSZ', 0, 0),
	(4, 'FUSI9V', 6, 0),
	(4, '8L6H9S', 20, 0),
	(4, 'LT63UM', 15, 0),
	(4, 'ELSUQP', 19, 0),
	(4, 'GN148F', 8, 0),
	(4, 'UP4KKI', 6, 0),
	(4, 'Y7PWAQ', 17, 0),
	(4, 'K5WWKM', 6, 0),
	(4, 'WOSL2D', 4, 0),
	
	(5, 'DWVKBC', 7, 0),
	(5, 'F4BOEX', 17, 0),
	(5, 'UEGU93', 14, 0),
	(5, 'D6FDSR', 4, 0),
	(5, '9DXU4I', 11, 0),
	(5, 'JSJ4FS', 4, 0),
	(5, '3IZSM1', 5, 0),
	(5, 'ZYA778', 16, 0),
	(5, 'XNR5H9', 6, 0),
	(5, '8CNN21', 8, 0),
	(5, 'MRPG6J', 6, 0),
	(5, 'XRBPH8', 0, 0),
	(5, 'RTNXAV', 20, 0),
	(5, 'G26FIY', 12, 0),
	(5, 'F81KZP', 11, 0),
	(5, 'MUJHY0', 18, 0),
	(5, '31J4C9', 17, 0),
	(5, '7GHACO', 8, 0),
	(5, 'V62V6Q', 0, 0),
	(5, 'O69MQG', 9, 0),
	(5, 'A84LZ6', 8, 0),
	(5, '68NT01', 1, 0),
	(5, '059270', 3, 0),
	(5, 'XKSSIP', 17, 0),
	(5, 'OB2ZY5', 11, 0),
	(5, '8R230I', 9, 0),
	(5, 'BDVGXM', 16, 0),
	(5, '7VMPPJ', 12, 0),
	(5, 'HLY0Z9', 13, 0),
	(5, 'DP2A2M', 19, 0),
	(5, 'A3GITZ', 5, 0),
	(5, '02JZ2C', 9, 0),
	(5, 'DC79B4', 19, 0),
	(5, 'Z8C9X2', 6, 0),
	(5, '4CLC14', 16, 0),
	(5, 'ZD3V9R', 0, 0),
	(5, '4XAWX5', 19, 0),
	(5, '843GIA', 15, 0),
	(5, 'Y7OWIE', 3, 0),
	(5, 'ZLU1NO', 6, 0),
	(5, 'LCAALA', 11, 0),
	(5, 'HVICQZ', 6, 0),
	(5, 'QWM28D', 20, 0),
	(5, 'S0PMF4', 11, 0),
	(5, 'KLEW5P', 4, 0),
	(5, 'XVGHOA', 11, 0),
	(5, 'IDN592', 10, 0),
	(5, 'XCAUAQ', 1, 0),
	(5, '3TRCA0', 6, 0),
	(5, '77D0CT', 1, 0),
	(5, '11Z8SD', 7, 0),
	(5, '1S1FM6', 17, 0),
	(5, 'NC9B2A', 5, 0),
	(5, 'RQXQZ9', 11, 0),
	(5, 'ZA141Z', 20, 0),
	(5, 'EB0Z8G', 3, 0),
	(5, 'S6K8AW', 12, 0),
	(5, 'HJMILL', 1, 0),
	(5, 'N0FQ0N', 18, 0),
	(5, 'FVBXD8', 8, 0),
	(5, 'OZ3WSV', 16, 0),
	(5, 'AMVNBC', 8, 0),
	(5, '0267TL', 9, 0),
	(5, '318HRM', 17, 0),
	(5, 'O2K6BP', 13, 0),
	(5, 'S70K5S', 19, 0),
	(5, 'WQC3HR', 2, 0),
	(5, 'E1SKWQ', 12, 0),
	(5, 'LAOIOV', 16, 0),
	(5, 'EG2BBA', 10, 0),
	(5, '3HXIPL', 18, 0),
	(5, 'NSKTPT', 3, 0),
	(5, 'T6CQ4N', 5, 0),
	(5, 'IBHQBJ', 8, 0),
	(5, 'KYZQLT', 6, 0),
	(5, 'G36IN2', 15, 0),
	(5, '3M1CSP', 3, 0),
	(5, 'N6DQJL', 19, 0),
	(5, 'BGKHXW', 3, 0),
	(5, 'E3E834', 18, 0),
	(5, 'OJWW4C', 8, 0),
	(5, 'V0YE4F', 10, 0),
	(5, '2B4NXA', 14, 0),
	(5, 'RFBX67', 19, 0),
	(5, '9RO8ZT', 5, 0),
	(5, 'FGGM5D', 8, 0),
	(5, '01VJPX', 13, 0),
	(5, 'X4RZOP', 1, 0),
	(5, 'X6SFMW', 9, 0),
	(5, '710LGH', 12, 0),
	(5, 'JDZBSZ', 1, 0),
	(5, 'FUSI9V', 16, 0),
	(5, '8L6H9S', 12, 0),
	(5, 'LT63UM', 11, 0),
	(5, 'ELSUQP', 3, 0),
	(5, 'GN148F', 18, 0),
	(5, 'UP4KKI', 11, 0),
	(5, 'Y7PWAQ', 9, 0),
	(5, 'K5WWKM', 15, 0),
	(5, 'WOSL2D', 11, 0);




/*
	ADDING TESTING USER
*/
insert into usager 
	(email, password, first_name, last_name, address, enabled)
values
	('soap.test@email.com', '$2a$10$ehrR08.WQZqnb7aT1gLR4O9FZCKy1gATA8EE/dsFjwKPP5RX0j0ey', 'John', 'Doe', '37 avenue du Marechal Juin 50000 SAINT-LÔ', true);