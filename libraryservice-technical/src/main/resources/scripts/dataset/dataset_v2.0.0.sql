/*
	ADDING TEST USER
*/
DO $$
    BEGIN
        INSERT INTO usager (id, password, first_name, last_name, email, address, enabled)
        VALUES
            (9, '$2a$10$c2B1pabIhvh7P5uJ1fgK8u9YZXdc2Pm5LjybYbyoYYrW8tokQCvl2', 'Prenom', 'Nom', 'soap.test2@email.com', '1 rue de la Paix 75000 Paris', true),
            (11, '$2a$10$8vYk4PPTP2o1BWRsW7wHRuaFi.zmB43X4oprXgxkk9uopzLW1mF7O', 'Prenom', 'Nom', 'soap.test3@emaiol.com', '1 rue de la Paix 75000 Paris', false);
    EXCEPTION
        WHEN sqlstate '23505' THEN
    END;
$$;

update usager set password='$2a$10$8vYk4PPTP2o1BWRsW7wHRuaFi.zmB43X4oprXgxkk9uopzLW1mF7O' where email='soap.test@email.com';

/*
	ADDING BORROWING AND RESERVATION
*/
DO $$
    BEGIN
        INSERT INTO borrowing (usager_id, book_reference, library_number_ref, borrowing_date, expiry_date, quantity, status, extended)
        VALUES (9, '9DXU4I', 3, (select to_timestamp('2018-09-24 22:21:20', 'YYYY-MM-DD HH24:MI:SS')), (select to_timestamp('2018-10-10 22:21:20', 'YYYY-MM-DD HH24:MI:SS')), 1, 'OVERDUE', false);
    EXCEPTION
        WHEN sqlstate '23505' THEN
    END;
$$;

DO $$
    BEGIN
        INSERT INTO waiting_list (usager_id, book_reference, reservation_date, is_email_sent, sending_mail_date)
        VALUES (9, 'JSJ4FS', (select to_timestamp('2021-09-24 22:21:20', 'YYYY-MM-DD HH24:MI:SS')), false, null);
    EXCEPTION
        WHEN sqlstate '23505' THEN
    END;
$$;