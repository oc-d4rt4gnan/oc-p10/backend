#!/bin/bash

sh /mnt/c/DEVENV/tomcat/apache-tomcat-9.0.27/bin/shutdown.sh
rm -rf /mnt/c/DEVENV/tomcat/apache-tomcat-9.0.27/webapps/libraryservice-webservice
rm /mnt/c/DEVENV/tomcat/apache-tomcat-9.0.27/webapps/libraryservice-webservice.war
cd ../../../../../../
mvn clean install
cd libraryservice-webservice/target/
mv libraryservice-webservice.war /mnt/c/DEVENV/tomcat/apache-tomcat-9.0.27/webapps/
sh /mnt/c/DEVENV/tomcat/apache-tomcat-9.0.27/bin/startup.sh
