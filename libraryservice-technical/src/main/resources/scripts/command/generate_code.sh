#!/bin/bash

cd ../../../../../../libraryservice-webservice/
rm -rf ./src/main/java/generated/
wsimport ./src/main/resources/LibraryService.wsdl -Xnocompile -d ./src/main/java -p generated.libraryservice
mvn clean install
