-- =====================================================================================================================
-- = OC-P10 #5 : Add a reservation for a book to the waiting list
-- =====================================================================================================================

-- Creation of the waiting list
DO $$
    BEGIN
        CREATE TABLE public.Waiting_List (
             Book_Reference VARCHAR NOT NULL,
             Usager_Id INTEGER NOT NULL,
             Reservation_Date TIMESTAMP NOT NULL,
             Sending_Mail_Date TIMESTAMP,
             Is_Email_Sent BOOLEAN NOT NULL,
             CONSTRAINT waiting_list_pk PRIMARY KEY (Book_Reference, Usager_Id)
        );
    EXCEPTION
        WHEN sqlstate '42P07' THEN
    END;
$$;

-- Adding the link between the list and the book
DO $$
    BEGIN
        ALTER TABLE public.Waiting_List ADD CONSTRAINT book_reservation_fk
        FOREIGN KEY (Book_Reference)
        REFERENCES public.Book (Reference)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
        NOT DEFERRABLE;
    EXCEPTION
        WHEN sqlstate '42710' THEN
    END;
$$;

-- Adding the link between the list and the user
DO $$
    BEGIN
        ALTER TABLE public.Waiting_List ADD CONSTRAINT usager_reservation_fk
        FOREIGN KEY (Usager_Id)
        REFERENCES public.Usager (Id)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
        NOT DEFERRABLE;
    EXCEPTION
        WHEN sqlstate '42710' THEN
    END;
$$;