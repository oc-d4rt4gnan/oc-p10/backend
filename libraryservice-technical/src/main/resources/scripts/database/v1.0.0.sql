
CREATE SEQUENCE public.library_number_seq;

CREATE TABLE public.Library (
                Number_ref INTEGER NOT NULL DEFAULT nextval('public.library_number_seq'),
                Name VARCHAR NOT NULL,
                Address VARCHAR NOT NULL,
                CONSTRAINT library_pk PRIMARY KEY (Number_ref)
);


ALTER SEQUENCE public.library_number_seq OWNED BY public.Library.Number_ref;

CREATE SEQUENCE public.usager_id_seq;

CREATE TABLE public.Usager (
                Id INTEGER NOT NULL DEFAULT nextval('public.usager_id_seq'),
                Email VARCHAR NOT NULL,
                Password VARCHAR NOT NULL,
                First_name VARCHAR NOT NULL,
                Last_name VARCHAR NOT NULL,
                Address VARCHAR NOT NULL,
                Enabled BOOLEAN NOT NULL,
                CONSTRAINT usager_pk PRIMARY KEY (Id)
);


ALTER SEQUENCE public.usager_id_seq OWNED BY public.Usager.Id;

CREATE TABLE public.VerificationToken (
                Id INTEGER NOT NULL,
                Token VARCHAR NOT NULL,
                Expiry_date DATE NOT NULL,
                Type VARCHAR NOT NULL,
                Usager_Id INTEGER NOT NULL,
                CONSTRAINT verificationtoken_pk PRIMARY KEY (Id)
);


CREATE TABLE public.Book (
                Reference VARCHAR NOT NULL,
                Image_url VARCHAR NOT NULL,
                Title VARCHAR NOT NULL,
                Author VARCHAR NOT NULL,
                Category VARCHAR NOT NULL,
                Synopsis VARCHAR NOT NULL,
                Publisher VARCHAR NOT NULL,
                Language VARCHAR NOT NULL,
                Mark INTEGER,
                CONSTRAINT book_pk PRIMARY KEY (Reference)
);


CREATE SEQUENCE public.comment_id_seq;

CREATE TABLE public.Comment (
                id INTEGER NOT NULL DEFAULT nextval('public.comment_id_seq'),
                Title VARCHAR NOT NULL,
                Content VARCHAR NOT NULL,
                Date DATE NOT NULL,
                Usager_Id INTEGER NOT NULL,
                Book_Reference VARCHAR NOT NULL,
                CONSTRAINT comment_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.comment_id_seq OWNED BY public.Comment.id;

CREATE TABLE public.Borrowing (
                Usager_Id INTEGER NOT NULL,
                Library_Number_ref INTEGER NOT NULL,
                Book_Reference VARCHAR NOT NULL,
                Borrowing_date DATE NOT NULL,
                Expiry_date DATE NOT NULL,
                Extended BOOLEAN NOT NULL,
                Status VARCHAR NOT NULL,
                Quantity INTEGER NOT NULL,
                CONSTRAINT borrowing_pk PRIMARY KEY (Usager_Id, Library_Number_ref, Book_Reference, Borrowing_date)
);


CREATE TABLE public.Stock (
                Library_Number_ref INTEGER NOT NULL,
                Book_Reference VARCHAR NOT NULL,
                Quantity INTEGER NOT NULL,
                Quantity_borrowed INTEGER NOT NULL,
                CONSTRAINT stock_pk PRIMARY KEY (Library_Number_ref, Book_Reference)
);


ALTER TABLE public.Stock ADD CONSTRAINT library_stock_fk
FOREIGN KEY (Library_Number_ref)
REFERENCES public.Library (Number_ref)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Borrowing ADD CONSTRAINT library_loan_fk
FOREIGN KEY (Library_Number_ref)
REFERENCES public.Library (Number_ref)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Borrowing ADD CONSTRAINT consumer_loan_fk
FOREIGN KEY (Usager_Id)
REFERENCES public.Usager (Id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.VerificationToken ADD CONSTRAINT usager_verificationtoken_fk
FOREIGN KEY (Usager_Id)
REFERENCES public.Usager (Id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Comment ADD CONSTRAINT usager_comment_fk
FOREIGN KEY (Usager_Id)
REFERENCES public.Usager (Id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Stock ADD CONSTRAINT books_stock_fk
FOREIGN KEY (Book_Reference)
REFERENCES public.Book (Reference)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Borrowing ADD CONSTRAINT books_loan_fk
FOREIGN KEY (Book_Reference)
REFERENCES public.Book (Reference)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.Comment ADD CONSTRAINT books_comment_fk
FOREIGN KEY (Book_Reference)
REFERENCES public.Book (Reference)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
