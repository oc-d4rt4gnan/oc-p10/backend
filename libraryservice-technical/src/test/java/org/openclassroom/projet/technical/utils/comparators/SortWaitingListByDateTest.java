package org.openclassroom.projet.technical.utils.comparators;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openclassroom.projet.model.database.service.WaitingList;
import org.openclassroom.projet.technical.utils.camparators.SortWaitingListByDate;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Comparator test class for the {@link WaitingList waiting list}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 28-08-2021
 */
public class SortWaitingListByDateTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    private static SortWaitingListByDate sortWaitingListByDate;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    public static void initialize () {
        sortWaitingListByDate = new SortWaitingListByDate();
    }
    
    @Test
    @DisplayName("As a system, I want the list to be sorted in chronological order of the date it was put on the " +
                 "waiting list, when I pass it into the sorting method.")
    public void givenAWaitingList_whenSortingIt_thenTheOldestElementIsFirst () {
        // GIVEN
        WaitingList waitingList1 = new WaitingList();
        WaitingList waitingList2 = new WaitingList();
        WaitingList waitingList3 = new WaitingList();
        
        waitingList1.setReservationDate(LocalDateTime.of(2019, Month.AUGUST, 10, 8, 10));
        waitingList2.setReservationDate(LocalDateTime.of(2017, Month.JANUARY, 16, 0, 30));
        waitingList3.setReservationDate(LocalDateTime.of(2021, Month.OCTOBER, 24, 15, 45));
        
        List<WaitingList> waitingList = Arrays.asList(waitingList3, waitingList1, waitingList2);
        List<WaitingList> expected = Arrays.asList(waitingList2, waitingList1, waitingList3);
        
        // WHEN
        waitingList.sort(sortWaitingListByDate);
        
        // THEN
        assertThat(waitingList).isEqualTo(expected).usingRecursiveComparison();
    }
    
}
