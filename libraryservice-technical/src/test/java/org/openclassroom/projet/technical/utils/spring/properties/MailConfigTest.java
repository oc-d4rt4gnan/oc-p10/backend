package org.openclassroom.projet.technical.utils.spring.properties;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.model.enums.TechnicalExceptionEnum;
import org.openclassroom.projet.technical.exceptions.TechnicalException;
import org.openclassroom.projet.technical.spring.properties.MailConfig;
import org.springframework.core.env.Environment;

import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.AdditionalMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;


/**
 * Test class for {@link MailConfig}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 18/09/2021
 */
class MailConfigTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    private static MailConfig classUnderTest;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void initialize () {
        classUnderTest = new MailConfig();
    }
    
    @Test
    @DisplayName("As a system, I want return a template in HTML format, When getting it by its name.")
    void givenANameOfTemplate_whenGettingTheTemplateOfEmail_thenItReturnTheTemplate () {
        // GIVEN
        String templateName = "book_available.html";
        
        // WHEN
        String result = classUnderTest.getTemplate(templateName);
        
        // THEN
        assertThat(result).contains("</html>");
    }
    
    @Test
    @DisplayName("As a system, I want throw an exception, When getting HTML template by a wrong name.")
    void givenAWrongNameOfTemplate_whenGettingTheTemplateOfEmail_thenItThrownAnException () {
        // GIVEN
        String templateName = "wrong_name";

        // THEN
        assertThatThrownBy(() -> {
            // WHEN
            classUnderTest.getTemplate(templateName);
        }).isInstanceOf(RuntimeException.class);
    }
}
