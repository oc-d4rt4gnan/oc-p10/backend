package org.openclassroom.projet.technical.utils.mails;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.model.enums.TechnicalExceptionEnum;
import org.openclassroom.projet.technical.exceptions.TechnicalException;

import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


/**
 * Test class for {@link MailUtils}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 12/09/2021
 */
class MailUtilsTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    private static MailUtils mailUtils;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void initialize () {
        mailUtils = new MailUtils();
    }
    
    @Test
    @DisplayName("As a system, I want the book, library and user specific information to be inserted in the " +
                 "availability information email, When the email is going to be sent. ")
    void givenAContentText_whenFillBookAvailabilityTemplate_thenContentTextIsFilledIn () {
        // GIVEN
        Usager user = new Usager();
        user.setFirstName("Maxime");
        
        Book book = new Book();
        book.setTitle("Harry Potter");
        
        Library library = new Library();
        library.setName("Médiabrary");
        library.setAddress("15 rue Saint Julien 06110 Le Cannet");
        
        String content = "Bonjour [PRENOM], Le livre [TITRE] est de nouveau disponible dans la bibliothèque " +
                         "[NOM_BIBLIOTHEQUE], située au [ADRESSE]";
        String expected = "Bonjour Maxime, Le livre Harry Potter est de nouveau disponible dans la bibliothèque " +
                          "Médiabrary, située au 15 rue Saint Julien 06110 Le Cannet";
        
        // WHEN
        String result = mailUtils.fillBookAvailabilityTemplate(content, user, book, library);
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
    @Test
    @DisplayName("As a system, I want to raise an exception, When there is a problem on sending a mail.")
    void givenAProblemInMailConfig_whenSendMailSMTP_thenThrowATechnicalException () {
        // GIVEN
        String recipientsEmailAddress = "";
        String title = "title";
        String content = "content";
        boolean debug = false;
        
        Properties customMailProperties = new Properties();
        customMailProperties.setProperty("mail.username", "username");
        customMailProperties.setProperty("mail.password", "password");
        
        mailUtils = new MailUtils(customMailProperties);
        
        // THEN
        assertThatThrownBy(() -> {
            // WHEN
            mailUtils.sendMailSMTP(recipientsEmailAddress, title, content, debug);
        }).isInstanceOf(TechnicalException.class).hasFieldOrPropertyWithValue("type",
                                                                              TechnicalExceptionEnum.CANT_SEND_EMAIL);
    }
    
}
