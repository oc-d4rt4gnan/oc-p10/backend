package org.openclassroom.projet.technical.spring.security;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openclassroom.projet.model.database.usager.Usager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


/**
 * Test class for security class {@link CustomAuthenticationProvider}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 19/09/2021
 */
class CustomAuthenticationProviderTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    private static CustomAuthenticationProvider classUnderTest;
    
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new CustomAuthenticationProvider();
    }
    
    @Test
    @Tag("Authenticate")
    @DisplayName("As a system, I want to throw an exception, When the input information are incorrect.")
    void givenWrongPassword_whenTryingToAuthenticateUser_thenThrowABadCredentialException () {
        // GIVEN
        Usager usager = new Usager();
        usager.setEmail("nom.prenom@email.com");
        usager.setPassword(passwordEncoder.encode("password"));
    
        Authentication authentication = new UsernamePasswordAuthenticationToken(usager,
                                                                                "wrongpassword",
                                                                                usager.getAuthorities());
    
        classUnderTest.setPasswordEncoder(passwordEncoder);
    
        // THEN
        assertThatThrownBy(() -> {
            // WHEN
            classUnderTest.authenticate(authentication);
        }).isInstanceOf(BadCredentialsException.class);
    }
    
    @Test
    @Tag("Authenticate")
    @DisplayName("As a system, I want return valid authenticate object, When the input information are correct.")
    void givenGoodCredentials_whenTryingToAuthenticateUser_thenReturnUserAuthenticated () {
        // GIVEN
        Usager usager = new Usager();
        usager.setEmail("nom.prenom@email.com");
        usager.setPassword(passwordEncoder.encode("password"));
        
        Authentication authentication = new UsernamePasswordAuthenticationToken(usager,
                                                                                "password",
                                                                                usager.getAuthorities());
        
        Authentication expected = new UsernamePasswordAuthenticationToken("nom.prenom@email.com",
                                                                          "password",
                                                                          new ArrayList<>());
        
        classUnderTest.setPasswordEncoder(passwordEncoder);
        
        // WHEN
        Authentication result = classUnderTest.authenticate(authentication);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    @Tag("Support")
    @DisplayName("As a system, I want return true, When the class is UsernamePasswordAuthenticationToken")
    void givenUsernamePasswordAuthenticationTokenClass_whenCheckTheTypeOfClass_thenReturnTrue () {
        // GIVEN -- Nothing to do
        
        // WHEN
        boolean result = classUnderTest.supports(UsernamePasswordAuthenticationToken.class);
        
        // THEN
        assertThat(result).isTrue();
    }
    
    @Test
    @Tag("Support")
    @DisplayName("As a system, I want return false, When the class is not UsernamePasswordAuthenticationToken")
    void givenOtherClassThanUsernamePasswordAuthenticationTokenClass_whenCheckTheTypeOfClass_thenReturnFalse () {
        // GIVEN -- Nothing to do
    
        // WHEN
        boolean result = classUnderTest.supports(Object.class);
    
        // THEN
        assertThat(result).isFalse();
    }
    
}
