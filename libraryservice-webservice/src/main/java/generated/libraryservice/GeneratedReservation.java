
package generated.libraryservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for generatedReservation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="generatedReservation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="reservationDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="book" type="{http://LibraryService/}generatedBook"/>
 *         &lt;element name="usager" type="{http://LibraryService/}generatedUsager"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generatedReservation", propOrder = {
    "reservationDate",
    "book",
    "usager"
})
public class GeneratedReservation {

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar reservationDate;
    @XmlElement(required = true)
    protected GeneratedBook book;
    @XmlElement(required = true)
    protected GeneratedUsager usager;

    /**
     * Gets the value of the reservationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReservationDate() {
        return reservationDate;
    }

    /**
     * Sets the value of the reservationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReservationDate(XMLGregorianCalendar value) {
        this.reservationDate = value;
    }

    /**
     * Gets the value of the book property.
     * 
     * @return
     *     possible object is
     *     {@link GeneratedBook }
     *     
     */
    public GeneratedBook getBook() {
        return book;
    }

    /**
     * Sets the value of the book property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneratedBook }
     *     
     */
    public void setBook(GeneratedBook value) {
        this.book = value;
    }

    /**
     * Gets the value of the usager property.
     * 
     * @return
     *     possible object is
     *     {@link GeneratedUsager }
     *     
     */
    public GeneratedUsager getUsager() {
        return usager;
    }

    /**
     * Sets the value of the usager property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneratedUsager }
     *     
     */
    public void setUsager(GeneratedUsager value) {
        this.usager = value;
    }

}
