package utils.converters;


import generated.libraryservice.GeneratedReservation;
import org.openclassroom.projet.model.database.service.WaitingList;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Allows you to convert a {@link WaitingList book waiting list} into a {@link GeneratedReservation user's
 * reservation} list
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 04/09/2021
 */
public class ReservationConverter {
    
    /**
     * Allows you to convert a {@link WaitingList waiting list} into a {@link GeneratedReservation reservation} list
     *
     * @param waitingList
     *         : The waiting list to convert
     *
     * @return a reservation list
     */
    public List<GeneratedReservation> fromDatabase (List<WaitingList> waitingList) {
        return waitingList.stream().map(this::fromDatabase).collect(Collectors.toList());
    }
    
    /**
     * Allows you to convert an element of a {@link WaitingList waiting list} into a {@link GeneratedReservation
     * reservation}
     *
     * @param waitingList
     *         : an element of the waiting list to be converted
     *
     * @return the element converted to a reservation
     */
    public GeneratedReservation fromDatabase (WaitingList waitingList) {
        GeneratedReservation reservation = new GeneratedReservation();
        
        reservation.setUsager(UsagerConverter.fromDatabase(waitingList.getWaitingListId().getUsager()));
        reservation.setBook(BookConverter.fromDatabase(waitingList.getWaitingListId().getBook()));
        LocalDateTime reservationLocalDateTime = waitingList.getReservationDate();
        String reservationString = reservationLocalDateTime.toString();
        StringBuilder builder = new StringBuilder();
        builder.append(reservationString);
        if (reservationLocalDateTime.getSecond() == 0 && reservationLocalDateTime.getNano() == 0) {
            builder.append(":00");
        }
        try {
            reservation.setReservationDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(builder.toString()));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        
        return reservation;
    }
    
}
