package libraryservice;


import org.openclassroom.projet.business.services.contract.ServiceFactory;

import javax.inject.Inject;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;


/**
 * Abstract class to provide the {@link ServiceFactory} and one date converter.
 */
abstract class AbstractWebInterface {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    @Inject
    private ServiceFactory serviceFactory;
    
    
    // =================================================================================================================
    //                                              GETTERS / SETTERS
    // =================================================================================================================
    
    /**
     * Spring control inversion for business module.
     *
     * @return The {@link ServiceFactory}.
     */
    ServiceFactory getServiceFactory () {
        return serviceFactory;
    }
    
    
    // =================================================================================================================
    //                                               PUBLIC METHODS
    // =================================================================================================================
    
    /**
     * Converts a date in {@link XMLGregorianCalendar} format to a date in {@link Date} format.
     *
     * @param calendar
     *         - The date in {@link XMLGregorianCalendar} format
     *
     * @return The date in {@link Date} format
     */
    Date XMLGregorianCalendarToDate (XMLGregorianCalendar calendar) {
        return calendar.toGregorianCalendar().getTime();
    }
    
}
