package utils.converters;


import generated.libraryservice.GeneratedBook;
import generated.libraryservice.GeneratedReservation;
import generated.libraryservice.GeneratedUsager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.service.WaitingList;
import org.openclassroom.projet.model.database.service.WaitingListId;
import org.openclassroom.projet.model.database.usager.Usager;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Test class for the {@link WaitingList waiting list} to {@link GeneratedReservation reservation} converter
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 04/09/2021
 */
@ExtendWith(MockitoExtension.class)
class ReservationConverterTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    private static ReservationConverter converter;
    
    private GeneratedReservation reservation;
    private WaitingList          waitingList;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        converter = new ReservationConverter();
    }
    
    @BeforeEach
    void intializeVariable () throws DatatypeConfigurationException {
        Usager user = new Usager();
        user.setId(1);
        user.setFirstName("firstname");
        user.setEnabled(true);
        user.setAddress("address");
        user.setLastName("lastname");
        user.setEmail("email");
        user.setPassword("password");
        
        Book book = new Book();
        book.setTitle("title");
        book.setReference("reference");
        book.setAuthor("author");
        book.setCategory("category");
        book.setImageUrl("url");
        book.setLanguage("language");
        book.setMark(3);
        book.setPublisher("publisher");
        book.setSynopsis("synopsis");
        
        LocalDateTime time = LocalDateTime.of(2020, 12, 21, 8, 10);
        
        String timeString = time.toString();
        if (time.getSecond() == 0 && time.getNano() == 0) {
            timeString += ":00";
        }
        XMLGregorianCalendar xmlTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(timeString);
        
        WaitingListId waitingListId = new WaitingListId();
        waitingListId.setUsager(user);
        waitingListId.setBook(book);
        
        waitingList = new WaitingList();
        waitingList.setReservationDate(time);
        waitingList.setWaitingListId(waitingListId);
        
        GeneratedUsager userConverted = new GeneratedUsager();
        userConverted.setId(1);
        userConverted.setFirstname("firstname");
        userConverted.setAdress("address");
        userConverted.setLastname("lastname");
        userConverted.setEmail("email");
        userConverted.setPassword("password");
        
        GeneratedBook bookConverted = new GeneratedBook();
        bookConverted.setTitle("title");
        bookConverted.setReference("reference");
        bookConverted.setAuthor("author");
        bookConverted.setCategory("category");
        bookConverted.setImageUrl("url");
        bookConverted.setLanguage("language");
        bookConverted.setMark(3);
        bookConverted.setPublisher("publisher");
        bookConverted.setSynopsis("synopsis");
        
        reservation = new GeneratedReservation();
        reservation.setReservationDate(xmlTime);
        reservation.setUsager(userConverted);
        reservation.setBook(bookConverted);
    }
    
    @Test
    @DisplayName("As a system, when I want to convert an item from a book queue, then I have to get an equivalent " +
                 "reservation")
    void givenAWaitingList_whenConvertFromDatabase_thenReturnTheEquivalentReservation () {
        // GIVEN
        GeneratedReservation expected = reservation;
        
        // WHEN
        GeneratedReservation result = converter.fromDatabase(waitingList);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
    @Test
    @DisplayName(
            "As a system, when I want to convert a user's waiting list, then I have to get an equivalent reservation " +
            "list")
    void givenAListOfWaitingList_whenConvertFromDatabase_thenReturnTheListOfEquivalentReservation ()
    throws DatatypeConfigurationException {
        // GIVEN
        Usager user = new Usager();
        user.setId(2);
        user.setFirstName("firstname");
        user.setEnabled(true);
        user.setAddress("address");
        user.setLastName("lastname");
        user.setEmail("email");
        user.setPassword("password");
        
        Book book = new Book();
        book.setTitle("title");
        book.setReference("reference");
        book.setAuthor("author");
        book.setCategory("category");
        book.setImageUrl("url");
        book.setLanguage("language");
        book.setMark(4);
        book.setPublisher("publisher");
        book.setSynopsis("synopsis");
        
        LocalDateTime time = LocalDateTime.of(2021, 12, 21, 8, 10);
        
        String timeString = time.toString();
        if (time.getSecond() == 0 && time.getNano() == 0) {
            timeString += ":00";
        }
        XMLGregorianCalendar xmlTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(timeString);
        
        WaitingListId waitingListId = new WaitingListId();
        waitingListId.setUsager(user);
        waitingListId.setBook(book);
        
        WaitingList waitingList = new WaitingList();
        waitingList.setReservationDate(time);
        waitingList.setWaitingListId(waitingListId);
        
        GeneratedUsager userConverted = new GeneratedUsager();
        userConverted.setId(2);
        userConverted.setFirstname("firstname");
        userConverted.setAdress("address");
        userConverted.setLastname("lastname");
        userConverted.setEmail("email");
        userConverted.setPassword("password");
        
        GeneratedBook bookConverted = new GeneratedBook();
        bookConverted.setTitle("title");
        bookConverted.setReference("reference");
        bookConverted.setAuthor("author");
        bookConverted.setCategory("category");
        bookConverted.setImageUrl("url");
        bookConverted.setLanguage("language");
        bookConverted.setMark(4);
        bookConverted.setPublisher("publisher");
        bookConverted.setSynopsis("synopsis");
        
        GeneratedReservation reservation = new GeneratedReservation();
        reservation.setReservationDate(xmlTime);
        reservation.setUsager(userConverted);
        reservation.setBook(bookConverted);
        
        List<WaitingList> list = Arrays.asList(this.waitingList, waitingList);
        List<GeneratedReservation> expected = Arrays.asList(this.reservation, reservation);
        
        // WHEN
        List<GeneratedReservation> result = converter.fromDatabase(list);
        
        // THEN
        assertThat(result).usingRecursiveComparison().isEqualTo(expected);
    }
    
}
