package libraryservice;


import generated.libraryservice.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.openclassroom.projet.business.services.contract.*;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.library.Stock;
import org.openclassroom.projet.model.database.library.StockId;
import org.openclassroom.projet.model.database.service.*;
import org.openclassroom.projet.model.database.usager.Usager;
import org.openclassroom.projet.model.database.usager.VerificationToken;
import org.openclassroom.projet.technical.exceptions.FunctionalException;
import org.openclassroom.projet.technical.exceptions.TechnicalException;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


/**
 * Test class for the webservice interface class
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 04/09/2021
 */
@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class LibraryServiceTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    @InjectMocks
    private static LibraryService classUnderTest;
    
    @Mock
    private ServiceFactory                                                     serviceFactory;
    @Mock
    private BookService                                                        bookService;
    @Mock
    private org.openclassroom.projet.business.services.contract.LibraryService libraryService;
    @Mock
    private BorrowingService                                                   borrowingService;
    @Mock
    private StockService                                                       stockService;
    @Mock
    private WaitingListService                                                 waitingListService;
    @Mock
    private CommentService                                                     commentService;
    @Mock
    private UserService                                                        userService;
    
    private GeneratedUsager user;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new LibraryService();
    }
    
    @BeforeEach
    void initializeVariable () {
        user = new GeneratedUsager();
        user.setId(0);
        user.setFirstname("firstname");
        user.setAdress("address");
        user.setLastname("lastname");
        user.setEmail("email");
    }
    
    @Nested
    class BorrowABook {
        
        // INNER ATTRIBUTES
        private List<Book> books;
        private Library    library;
        
        // INNER TESTS
        @BeforeEach
        void initializeVariable () {
            Book book = new Book();
            book.setTitle("title");
            book.setReference("reference");
            book.setAuthor("author");
            book.setCategory("category");
            book.setImageUrl("url");
            book.setLanguage("language");
            book.setMark(3);
            book.setPublisher("publisher");
            book.setSynopsis("synopsis");
            
            books = Collections.singletonList(book);
            
            library = new Library();
            library.setNumberRef(1);
            library.setAddress("address");
            library.setName("name");
            
            // mocking
            when(serviceFactory.getBookService()).thenReturn(bookService);
            when(serviceFactory.getLibraryService()).thenReturn(libraryService);
            when(serviceFactory.getStockService()).thenReturn(stockService);
            
            when(bookService.getBooks(anyString())).thenReturn(books);
            when(libraryService.getLibrary(anyInt())).thenReturn(library);
        }
        
        @Test
        @Tag("Borrowing")
        @DisplayName("As a user, I want to borrow a book, When I call the borrowABook function and the book is " +
                     "available")
        void givenAnAvailableBook_whenBorrowABook_thenItReturnANewBorrowing () throws BookingBookException {
            // GIVEN
            GeneratedBook bookConverted = new GeneratedBook();
            bookConverted.setTitle("title");
            bookConverted.setReference("reference");
            bookConverted.setAuthor("author");
            bookConverted.setCategory("category");
            bookConverted.setImageUrl("url");
            bookConverted.setLanguage("language");
            bookConverted.setMark(3);
            bookConverted.setPublisher("publisher");
            bookConverted.setSynopsis("synopsis");
            
            GeneratedLibrary libraryConverted = new GeneratedLibrary();
            libraryConverted.setNumberRef(1);
            libraryConverted.setAddress("address");
            libraryConverted.setName("name");
            
            GeneratedBorrowing expected = new GeneratedBorrowing();
            expected.setBook(bookConverted);
            expected.setLibrary(libraryConverted);
            expected.setUsager(user);
            expected.setStatus("OUTSTANDING");
            expected.setQuantity(1);
            expected.setExtended(false);
            
            Stock stock = new Stock();
            stock.setQuantity(2);
            stock.setQuantityBorrowed(1);
            
            // mocking
            when(serviceFactory.getBorrowingService()).thenReturn(borrowingService);
            when(stockService.getStockForBook(anyInt(), anyString())).thenReturn(stock);
            
            doNothing().when(borrowingService).addNewBorrowing(any());
            doNothing().when(stockService).updateStock(anyInt(), anyString(), anyInt());
            
            // WHEN
            GeneratedBorrowing result = classUnderTest.borrowABook(1, "reference", user);
            
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo(expected);
        }
        
        @Test
        @Tag("Reservation")
        @DisplayName("As a user, I want to be put on the waiting list, When the book is not available")
        void givenAnUnavailableBook_whenBorrowABook_thenItReturnNothing ()
        throws BookingBookException, FunctionalException {
            // GIVEN
            Stock stock = new Stock();
            stock.setQuantity(2);
            stock.setQuantityBorrowed(2);
            
            when(serviceFactory.getWaitingListService()).thenReturn(waitingListService);
            when(stockService.getStockForBook(anyInt(), anyString())).thenReturn(stock);
            when(waitingListService.addReservation(any(), any())).thenReturn(null);
            
            // WHEN
            GeneratedBorrowing result = classUnderTest.borrowABook(1, "reference", user);
            
            // THEN
            assertThat(result).isNull();
        }
        
        @Test
        @Tag("Reservation")
        @DisplayName("As a user, I want throwing a BookingBookException, When I try to reserve a book and a " +
                     "FunctionalException occur")
        void givenAFunctionalExceptionThrown_whenTryingToBorrowABook_thenItThrowABookingBookException ()
        throws FunctionalException {
            // GIVEN
            Stock stock = new Stock();
            stock.setQuantity(2);
            stock.setQuantityBorrowed(2);
            
            when(serviceFactory.getWaitingListService()).thenReturn(waitingListService);
            when(stockService.getStockForBook(anyInt(), anyString())).thenReturn(stock);
            when(waitingListService.addReservation(any(), any())).thenThrow(FunctionalException.class);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.borrowABook(1, "reference", user);
            }).isInstanceOf(BookingBookException.class);
        }
        
    }
    
    @Nested
    class addComment {
        
        @Test
        @DisplayName("As a user, I want to retrieve the added comment, When I try to add it on a book.")
        void givenANewCommentOnABook_whenTryingToAddThisComment_thenItReturnTheSavedComment ()
        throws DatatypeConfigurationException, ParseException {
            // GIVEN
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = format.parse("2014-04-24 11:15:00");
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(date);
            XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            
            GeneratedComment comment = new GeneratedComment();
            comment.setTitle("title");
            comment.setContent("content");
            comment.setBookReference("reference");
            comment.setAuthor(user);
            comment.setDate(xmlGregCal);
            
            Usager dbUser = new Usager();
            dbUser.setId(0);
            dbUser.setEmail("email");
            dbUser.setPassword("password");
            dbUser.setFirstName("firstname");
            dbUser.setLastName("lastname");
            dbUser.setAddress("address");
            
            Comment savedComment = new Comment();
            savedComment.setTitle("title");
            savedComment.setContent("content");
            savedComment.setBookReference("reference");
            savedComment.setUsager(dbUser);
            savedComment.setDate(date);
            
            when(serviceFactory.getCommentService()).thenReturn(commentService);
            when(commentService.addComment(any())).thenReturn(savedComment);
            
            // WHEN
            GeneratedComment result = classUnderTest.addComment(comment);
            
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo(comment);
        }
        
    }
    
    @Nested
    class addUser {
        
        @Test
        @DisplayName("As a user, I want to retrieve the added user, When I try to register him.")
        void givenANewUser_whenTryingToAddThisUser_thenItReturnTheSavedUser () throws Exception {
            // GIVEN
            String token = "token";
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.save(any())).thenReturn(token);
            
            // WHEN
            String result = classUnderTest.addUser(user);
            
            // THEN
            assertThat(result).isEqualTo(token);
        }
        
        @Test
        @DisplayName("As a user, I want throwing a RegisterException, When I try to register a new user and a problem" +
                     " occur.")
        void givenAProblemDuringRegister_whenTryingToAddThisUser_thenItThrowARegisterException () throws Exception {
            // GIVEN
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.save(any())).thenThrow(Exception.class);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.addUser(user);
            }).isInstanceOf(RegisterException.class);
        }
        
    }
    
    @Nested
    class checkExpiration {
        
        // INNER VARIABLE
        private Usager  usager;
        private Book    book;
        private Library library;
        
        @BeforeEach
        void initVariable () {
            usager = new Usager();
            usager.setId(0);
            usager.setEmail("email");
            usager.setPassword("password");
            usager.setFirstName("firstname");
            usager.setLastName("lastname");
            usager.setAddress("address");
            
            book = new Book();
            book.setTitle("title");
            book.setReference("reference");
            book.setAuthor("author");
            book.setCategory("category");
            book.setImageUrl("url");
            book.setLanguage("language");
            book.setMark(3);
            book.setPublisher("publisher");
            book.setSynopsis("synopsis");
            
            library = new Library();
            library.setNumberRef(1);
            library.setAddress("address");
            library.setName("name");
        }
        
        @Test
        @DisplayName("As a user, I want to retrieve the list of expired borrowing, When I check the expiration date " +
                     "of borrowings")
        void givenAnExpiredBorrowing_whenICheckTheExpirationDateOfBorrowings_thenItReturnTheListOfExpiredBorrowing ()
        throws EmailSendingException, TechnicalException, ParseException, DatatypeConfigurationException {
            // GIVEN
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = format.parse("2014-04-24 11:15:00");
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(date);
            XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            
            BorrowingId id = new BorrowingId();
            id.setBorrowingDate(date);
            id.setUsager(usager);
            id.setBook(book);
            id.setLibrary(library);
            
            Borrowing borrowing = new Borrowing();
            borrowing.setBorrowingId(id);
            borrowing.setStatus("OVERDUE");
            borrowing.setQuantity(1);
            borrowing.setExpiryDate(date);
            borrowing.setExtended(false);
            
            List<Borrowing> borrowings = Collections.singletonList(borrowing);
            
            GeneratedBook generatedBook = new GeneratedBook();
            generatedBook.setTitle("title");
            generatedBook.setReference("reference");
            generatedBook.setAuthor("author");
            generatedBook.setCategory("category");
            generatedBook.setImageUrl("url");
            generatedBook.setLanguage("language");
            generatedBook.setMark(3);
            generatedBook.setPublisher("publisher");
            generatedBook.setSynopsis("synopsis");
            
            GeneratedLibrary generatedLibrary = new GeneratedLibrary();
            generatedLibrary.setNumberRef(1);
            generatedLibrary.setAddress("address");
            generatedLibrary.setName("name");
            
            GeneratedBorrowing generatedBorrowing = new GeneratedBorrowing();
            generatedBorrowing.setUsager(user);
            generatedBorrowing.setBook(generatedBook);
            generatedBorrowing.setLibrary(generatedLibrary);
            generatedBorrowing.setStatus("OVERDUE");
            generatedBorrowing.setQuantity(1);
            generatedBorrowing.setExpiryDate(xmlGregCal);
            generatedBorrowing.setExtended(false);
            
            List<GeneratedBorrowing> expected = Collections.singletonList(generatedBorrowing);
            
            when(serviceFactory.getBorrowingService()).thenReturn(borrowingService);
            when(borrowingService.checkExpiration()).thenReturn(borrowings);
            
            // WHEN
            List<GeneratedBorrowing> result = classUnderTest.checkExpiration();
            
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().ignoringCollectionOrder()
                              .isEqualTo(expected);
        }
        
        @Test
        @DisplayName("As a user, I want throwing an EmailSendingException, When I check the validity of borrowings " +
                     "and an error occur on email sending.")
        void givenAProblemOnSendingEmail_whenICheckTheExpirationDateOfBorrowings_thenItThrowAnEmailSendingException ()
        throws TechnicalException {
            // GIVEN
            when(serviceFactory.getBorrowingService()).thenReturn(borrowingService);
            when(borrowingService.checkExpiration()).thenThrow(TechnicalException.class);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.checkExpiration();
            }).isInstanceOf(EmailSendingException.class);
        }
        
    }
    
    @Nested
    class connectUser {
        
        @Test
        @DisplayName("As a user, I want to be connected, When I try to login.")
        void givenAnIdentifierAndPassword_whenTryingToConnectUser_thenItReturnTheLoggedInUser () throws Exception {
            // GIVEN
            Usager usager = new Usager();
            usager.setId(0);
            usager.setEmail("email");
            usager.setPassword("password");
            usager.setFirstName("firstname");
            usager.setLastName("lastname");
            usager.setAddress("address");
            
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.login(anyString(), anyString())).thenReturn(usager);
            
            // WHEN
            GeneratedUsager result = classUnderTest.connectUser(usager.getEmail(), usager.getPassword());
            
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().isEqualTo(user);
        }
        
        @Test
        @DisplayName("As a user, I want throwing a LoginException, When I try to connect an user and a problem occur.")
        void givenAProblemDuringConnection_whenTryingToConnectUser_thenItThrowALoginException () throws Exception {
            // GIVEN
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.login(any(), any())).thenThrow(Exception.class);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.connectUser(user.getEmail(), user.getPassword());
            }).isInstanceOf(LoginException.class);
        }
        
    }
    
    @Nested
    class extendBookBorrowing {
        
        @Test
        @DisplayName("As a user, I want to retrieve an extended borrowing, When I try to extend it.")
        void givenABorrowing_whenTryingToExtendIt_thenItReturnTrue () throws Exception {
            // GIVEN
            XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
            
            when(serviceFactory.getBorrowingService()).thenReturn(borrowingService);
            when(borrowingService.extendBookBorrowed(any(), anyInt(), anyString(), anyInt())).thenReturn(true);
            
            // WHEN
            boolean result = classUnderTest.extendBookBorrowing(xmlGregCal, 1, "reference", user);
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        @DisplayName("As a user, I want getting false, When the borrowing that I try to extend is already extended")
        void givenABorrowingAlreadyExtended_whenTryingToExtendIt_thenItReturnFalse () throws Exception {
            // GIVEN
            XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
            
            when(serviceFactory.getBorrowingService()).thenReturn(borrowingService);
            when(borrowingService.extendBookBorrowed(any(), anyInt(), anyString(), anyInt())).thenReturn(false);
            
            // WHEN
            boolean result = classUnderTest.extendBookBorrowing(xmlGregCal, 1, "reference", user);
            
            // THEN
            assertThat(result).isFalse();
        }
        
        @Test
        @DisplayName("As a user, I want throwing a BookBorrowingExtensionException, When an error occurred during the" +
                     " extension of a borrowing")
        void givenAProblem_whenTryingToExtendABorrowing_thenItThrowABookBorrowingExtensionException ()
        throws Exception {
            // GIVEN
            XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
            
            when(serviceFactory.getBorrowingService()).thenReturn(borrowingService);
            when(borrowingService.extendBookBorrowed(any(), anyInt(), anyString(), anyInt()))
                    .thenThrow(Exception.class);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.extendBookBorrowing(xmlGregCal, 1, "reference", user);
            }).isInstanceOf(BookBorrowingExtensionException.class);
        }
        
    }
    
    @Nested
    class getBookAvailability {
        
        @Test
        @DisplayName("As a user, I want to get the amount of book available, When I getting its availability")
        void givenABookWithAvailableQuantity_whenTryingToGetItsAvailability_thenItReturnReturnTheAmountAvailable () {
            // GIVEN
            Library library = new Library();
            library.setNumberRef(1);
            library.setAddress("address");
            library.setName("name");
            
            List<Library> libraries = Collections.singletonList(library);
            
            StockId id = new StockId();
            id.setLibaryNumberRef(1);
            id.setBookReference("reference");
            
            Stock stock = new Stock();
            stock.setStockId(id);
            stock.setQuantity(2);
            stock.setQuantityBorrowed(1);
            
            GeneratedLibrary generatedLibrary = new GeneratedLibrary();
            generatedLibrary.setName("name");
            generatedLibrary.setAddress("address");
            generatedLibrary.setNumberRef(1);
            
            GeneratedStock generatedStock = new GeneratedStock();
            generatedStock.setLibrary(generatedLibrary);
            generatedStock.setReferenceBook("reference");
            generatedStock.setQuantity(1);
            
            List<GeneratedStock> expected = Collections.singletonList(generatedStock);
            
            when(serviceFactory.getLibraryService()).thenReturn(libraryService);
            when(libraryService.getLibraries()).thenReturn(libraries);
            when(serviceFactory.getStockService()).thenReturn(stockService);
            when(stockService.getStockForBook(1, "reference")).thenReturn(stock);
            
            // WHEN
            List<GeneratedStock> result = classUnderTest.getBookAvailability("reference");
            
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().ignoringCollectionOrder()
                              .isEqualTo(expected);
        }
        
    }
    
    @Nested
    class getBooksWithKeyword {
        
        @Test
        @DisplayName("As a user, I want to retrieve a list of book, When trying to get a list of book by keyword")
        void givenAKeyword_whenTryingToGetAListOfBooks_thenItReturnAListOfBooks ()
        throws ParseException, DatatypeConfigurationException {
            // GIVEN
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = format.parse("2014-04-24 11:15:00");
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(date);
            XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            
            Book book = new Book();
            book.setTitle("title");
            book.setReference("reference");
            book.setAuthor("author");
            book.setCategory("category");
            book.setImageUrl("url");
            book.setLanguage("language");
            book.setMark(3);
            book.setPublisher("publisher");
            book.setSynopsis("synopsis");
            
            List<Book> books = Collections.singletonList(book);
            
            Usager usager = new Usager();
            usager.setId(0);
            usager.setEmail("email");
            usager.setPassword("password");
            usager.setFirstName("firstname");
            usager.setLastName("lastname");
            usager.setAddress("address");
            
            Comment comment = new Comment();
            comment.setTitle("title");
            comment.setContent("content");
            comment.setBookReference("reference");
            comment.setUsager(usager);
            comment.setDate(date);
            
            List<Comment> comments = Collections.singletonList(comment);
            
            GeneratedComment generatedComment = new GeneratedComment();
            generatedComment.setTitle("title");
            generatedComment.setContent("content");
            generatedComment.setBookReference("reference");
            generatedComment.setAuthor(user);
            generatedComment.setDate(xmlGregCal);
            
            List<GeneratedComment> generatedComments = Collections.singletonList(generatedComment);
            
            GeneratedBook generatedBook = new GeneratedBook();
            generatedBook.setTitle("title");
            generatedBook.setReference("reference");
            generatedBook.setAuthor("author");
            generatedBook.setCategory("category");
            generatedBook.setImageUrl("url");
            generatedBook.setLanguage("language");
            generatedBook.setMark(3);
            generatedBook.setPublisher("publisher");
            generatedBook.setSynopsis("synopsis");
            generatedBook.getComments().addAll(generatedComments);
            
            List<GeneratedBook> expected = Collections.singletonList(generatedBook);
            
            when(serviceFactory.getBookService()).thenReturn(bookService);
            when(bookService.getBooks(anyString())).thenReturn(books);
            when(serviceFactory.getCommentService()).thenReturn(commentService);
            when(commentService.getCommentsFor(anyString())).thenReturn(comments);
            
            // WHEN
            List<GeneratedBook> result = classUnderTest.getBooksWithKeyword("");
            
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().ignoringCollectionOrder()
                              .isEqualTo(expected);
        }
        
    }
    
    @Nested
    class getBorrowingFor {
        
        @Test
        @DisplayName("As a user, I want to retrieve the borrowings list of an user, When I try to getting it by user " +
                     "id")
        void givenAnUserId_whenTryingToGetTheBorrowingsListOfTheUser_thenItReturnIt ()
        throws ParseException, DatatypeConfigurationException {
            // GIVEN
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = format.parse("2014-04-24 11:15:00");
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(date);
            XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            
            Book book = new Book();
            book.setTitle("title");
            book.setReference("reference");
            book.setAuthor("author");
            book.setCategory("category");
            book.setImageUrl("url");
            book.setLanguage("language");
            book.setMark(3);
            book.setPublisher("publisher");
            book.setSynopsis("synopsis");
            
            Library library = new Library();
            library.setName("name");
            library.setAddress("address");
            library.setNumberRef(1);
            
            Usager usager = new Usager();
            usager.setId(0);
            usager.setEmail("email");
            usager.setPassword("password");
            usager.setFirstName("firstname");
            usager.setLastName("lastname");
            usager.setAddress("address");
            
            BorrowingId id = new BorrowingId();
            id.setBook(book);
            id.setLibrary(library);
            id.setUsager(usager);
            id.setBorrowingDate(date);
            
            Borrowing borrowing = new Borrowing();
            borrowing.setBorrowingId(id);
            borrowing.setStatus("OUTSTANDING");
            borrowing.setQuantity(1);
            borrowing.setExpiryDate(date);
            borrowing.setExtended(false);
            
            List<Borrowing> borrowings = Collections.singletonList(borrowing);
            
            GeneratedBook generatedBook = new GeneratedBook();
            generatedBook.setTitle("title");
            generatedBook.setReference("reference");
            generatedBook.setAuthor("author");
            generatedBook.setCategory("category");
            generatedBook.setImageUrl("url");
            generatedBook.setLanguage("language");
            generatedBook.setMark(3);
            generatedBook.setPublisher("publisher");
            generatedBook.setSynopsis("synopsis");
            
            GeneratedLibrary generatedLibrary = new GeneratedLibrary();
            generatedLibrary.setName("name");
            generatedLibrary.setAddress("address");
            generatedLibrary.setNumberRef(1);
            
            GeneratedBorrowing generatedBorrowing = new GeneratedBorrowing();
            generatedBorrowing.setBook(generatedBook);
            generatedBorrowing.setLibrary(generatedLibrary);
            generatedBorrowing.setUsager(user);
            generatedBorrowing.setStatus("OUTSTANDING");
            generatedBorrowing.setQuantity(1);
            generatedBorrowing.setExpiryDate(xmlGregCal);
            generatedBorrowing.setExtended(false);
            
            List<GeneratedBorrowing> expected = Collections.singletonList(generatedBorrowing);
            
            when(serviceFactory.getBorrowingService()).thenReturn(borrowingService);
            when(borrowingService.getBorrowingsFor(anyInt())).thenReturn(borrowings);
            
            // WHEN
            List<GeneratedBorrowing> result = classUnderTest.getBorrowingFor(0);
            
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().ignoringCollectionOrder()
                              .isEqualTo(expected);
        }
        
    }
    
    @Nested
    class getReservationFor {
        
        @Test
        @DisplayName(
                "As a user, I want to retrieve the waiting list of an user, When I try to getting it by user " + "id")
        void givenAnUserId_whenTryingToGetTheWaitingListOfTheUser_thenItReturnIt () throws ParseException {
            // GIVEN
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = format.parse("2014-04-24 11:15:00");
            
            Book book = new Book();
            book.setTitle("title");
            book.setReference("reference");
            book.setAuthor("author");
            book.setCategory("category");
            book.setImageUrl("url");
            book.setLanguage("language");
            book.setMark(3);
            book.setPublisher("publisher");
            book.setSynopsis("synopsis");
            
            Usager usager = new Usager();
            usager.setId(0);
            usager.setEmail("email");
            usager.setPassword("password");
            usager.setFirstName("firstname");
            usager.setLastName("lastname");
            usager.setAddress("address");
            
            WaitingListId id = new WaitingListId();
            id.setBook(book);
            id.setUsager(usager);
            
            WaitingList reservation = new WaitingList();
            reservation.setWaitingListId(id);
            reservation.setReservationDate(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
            reservation.setEmailSent(true);
            reservation.setSendingMailDate(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
            
            List<WaitingList> reservations = Collections.singletonList(reservation);
            
            GeneratedBook generatedBook = new GeneratedBook();
            generatedBook.setTitle("title");
            generatedBook.setReference("reference");
            generatedBook.setAuthor("author");
            generatedBook.setCategory("category");
            generatedBook.setImageUrl("url");
            generatedBook.setLanguage("language");
            generatedBook.setMark(3);
            generatedBook.setPublisher("publisher");
            generatedBook.setSynopsis("synopsis");
            
            GeneratedReservation generatedReservation = new GeneratedReservation();
            generatedReservation.setBook(generatedBook);
            generatedReservation.setUsager(user);
            
            List<GeneratedReservation> expected = Collections.singletonList(generatedReservation);
            
            when(serviceFactory.getWaitingListService()).thenReturn(waitingListService);
            when(waitingListService.getReservationFor(anyInt())).thenReturn(reservations);
            
            // WHEN
            List<GeneratedReservation> result = classUnderTest.getReservationFor(0);
            
            // THEN
            assertThat(result).usingRecursiveComparison().ignoringExpectedNullFields().ignoringCollectionOrder()
                              .isEqualTo(expected);
        }
        
    }
    
    @Nested
    class requestPasswordReset {
        
        @Test
        @DisplayName("As a user, I want to retrieve the newly created token, When I try to send a request for " +
                     "reseting user password")
        void givenAnUserEmail_whenTryingToSendARequestForRestPassword_thenItReturnTheNewlyCreatedToken ()
        throws Exception {
            // GIVEN
            String token = "token";
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.sendEmailToResetPasswordFor(anyString())).thenReturn(token);
            
            // WHEN
            String result = classUnderTest.requestPasswordReset("email");
            
            // THEN
            assertThat(result).isEqualTo(token);
        }
        
        @Test
        @DisplayName("As a user, I want throwing a ForgotPasswordException, When I try to send a request for " +
                     "reseting a password of an inactivate user")
        void givenAnInactivateUser_whenTryingToSendARequestForRestPassword_thenItThrowAForgotPasswordException ()
        throws Exception {
            // GIVEN
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.sendEmailToResetPasswordFor(anyString())).thenThrow(Exception.class);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.requestPasswordReset("email");
            }).isInstanceOf(ForgotPasswordException.class);
        }
        
    }
    
    @Nested
    class resendVerificationEmail {
        
        @Test
        @DisplayName("As a user, I want return true, When I try to send back the verification account email")
        void givenAnUserEmail_whenTryingToSendBackAVerificationAccountEmail_thenItReturnTrue () throws Exception {
            // GIVEN
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.resendVerificationEmail(anyString())).thenReturn(true);
            
            // WHEN
            boolean result = classUnderTest.resendVerificationEmail("email");
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        @DisplayName("As a user, I want throwing a EmailSendingException, When I try to send back the verification " +
                     "account email and a problem occur")
        void givenAProblemDuringEmailSending_whenTryingToSendBackAVerificationAccountEmail_thenItThrowAnEmailSendingException ()
        throws Exception {
            // GIVEN
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.resendVerificationEmail(anyString())).thenThrow(Exception.class);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.resendVerificationEmail("email");
            }).isInstanceOf(EmailSendingException.class);
        }
        
        @Nested
        class resetPassword {
            
            @Test
            @DisplayName("As a user, I want return true, When I try to reset my password")
            void givenANewPassword_whenTryingToResetUserPassword_thenItReturnTrue () {
                // GIVEN
                when(serviceFactory.getUserService()).thenReturn(userService);
                when(userService.createNewPasswordForUsagerWith(anyString(), anyString(), anyString()))
                        .thenReturn(true);
                
                // WHEN
                boolean result = classUnderTest.resetPassword("token", "password", "confirmation");
                
                // THEN
                assertThat(result).isTrue();
            }
            
            @Test
            @DisplayName("As a user, I want return false, When I try to reset my password and a proble occur")
            void givenAProblem_whenTryingToResetUserPassword_thenItReturnFalse () {
                // GIVEN
                when(serviceFactory.getUserService()).thenReturn(userService);
                when(userService.createNewPasswordForUsagerWith(anyString(), anyString(), anyString()))
                        .thenReturn(false);
                
                // WHEN
                boolean result = classUnderTest.resetPassword("token", "password", "confirmation");
                
                // THEN
                assertThat(result).isFalse();
            }
            
        }
        
    }
    
    @Nested
    class returnBook {
        
        @Test
        @DisplayName("As a user, I want to retrieve a status of RETURNED, When I return a book")
        void givenABorrowing_whenTryingToReturnABook_thenItReturnAStatusOfRETURNED ()
        throws DatatypeConfigurationException, EmailSendingException, TechnicalException {
            // GIVEN
            XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
            
            when(serviceFactory.getBorrowingService()).thenReturn(borrowingService);
            when(borrowingService.stopBorrowing(any(), anyInt(), anyString(), anyInt())).thenReturn(1);
            when(serviceFactory.getStockService()).thenReturn(stockService);
            doNothing().when(stockService).updateStock(anyInt(), anyString(), anyInt());
            when(serviceFactory.getWaitingListService()).thenReturn(waitingListService);
            doNothing().when(waitingListService).informsTheFirstUserInTheList(anyString(), anyInt());
            
            // WHEN
            String result = classUnderTest.returnBook(xmlGregCal, 1, "reference", user);
            
            // THEN
            assertThat(result).isEqualTo("RETURNED");
        }
        
        @Test
        @DisplayName("As a user, I want to retrieve a status of NOT RETURNED, When I try to return a book with no " +
                     "quantity loaned")
        void givenABorrowingWithQuantityOf0_whenTryingToReturnABook_thenItReturnAStatusOfNOTRETURNED ()
        throws DatatypeConfigurationException, EmailSendingException, TechnicalException {
            // GIVEN
            XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
            
            when(serviceFactory.getBorrowingService()).thenReturn(borrowingService);
            when(borrowingService.stopBorrowing(any(), anyInt(), anyString(), anyInt())).thenReturn(0);
            when(serviceFactory.getStockService()).thenReturn(stockService);
            doNothing().when(stockService).updateStock(anyInt(), anyString(), anyInt());
            when(serviceFactory.getWaitingListService()).thenReturn(waitingListService);
            doNothing().when(waitingListService).informsTheFirstUserInTheList(anyString(), anyInt());
            
            // WHEN
            String result = classUnderTest.returnBook(xmlGregCal, 1, "reference", user);
            
            // THEN
            assertThat(result).isEqualTo("NOT RETURNED");
        }
        
        @Test
        @DisplayName("As a user, I want throwing an EmailSendingException, When I return a book and a problem occur")
        void givenAProblemWithSendingMail_whenTryingToReturnABook_thenItThrowAnEmailSendingException ()
        throws DatatypeConfigurationException, TechnicalException {
            // GIVEN
            XMLGregorianCalendar xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar();
            
            when(serviceFactory.getBorrowingService()).thenReturn(borrowingService);
            when(borrowingService.stopBorrowing(any(), anyInt(), anyString(), anyInt())).thenReturn(1);
            when(serviceFactory.getStockService()).thenReturn(stockService);
            doNothing().when(stockService).updateStock(anyInt(), anyString(), anyInt());
            when(serviceFactory.getWaitingListService()).thenReturn(waitingListService);
            doThrow(TechnicalException.class).when(waitingListService).informsTheFirstUserInTheList(anyString(),
                                                                                                    anyInt());
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.returnBook(xmlGregCal, 1, "reference", user);
            }).isInstanceOf(EmailSendingException.class);
        }
        
    }
    
    @Nested
    class updatePassword {
        
        @Test
        @DisplayName("As a user, I want return true, When I try to change the password of an user")
        void givenANewPassword_whenTryingToChangeThePasswordOfAnUser_thenItReturnTrue () {
            // GIVEN
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.changeUserPassword(anyString(), anyString(), anyString())).thenReturn(true);
            
            // WHEN
            boolean result = classUnderTest.updatePassword("email", "password", "confirmation");
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        @DisplayName("As a user, I want return false, When I try to change the password of an user with bad password")
        void givenABadPassword_whenTryingToChangeThePasswordOfAnUser_thenItReturnFalse () {
            // GIVEN
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.changeUserPassword(anyString(), anyString(), anyString())).thenReturn(false);
            
            // WHEN
            boolean result = classUnderTest.updatePassword("email", "password", "confirmation");
            
            // THEN
            assertThat(result).isFalse();
        }
        
    }
    
    @Nested
    class updateUserInfos {
        
        @Test
        @DisplayName("As a user, I want return true, When I try to change user information")
        void givenANewUserInformation_whenTryingToUpdateUserInformation_thenItReturnTrue () throws Exception {
            // GIVEN
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.updateUsagerInfos(anyString(), any())).thenReturn(true);
            
            // WHEN
            boolean result = classUnderTest.updateUserInfos("email", user);
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        @DisplayName("As a user, I want return false, When I try to update user with no new information")
        void givenNoNewUserInformation_whenTryingToUpdateUserInformation_thenItReturnFalse () throws Exception {
            // GIVEN
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.updateUsagerInfos(anyString(), any())).thenReturn(false);
            
            // WHEN
            boolean result = classUnderTest.updateUserInfos("email", user);
            
            // THEN
            assertThat(result).isFalse();
        }
        
        @Test
        @DisplayName("As a user, I want throwing UpdateUserException, When a problem occur during updating of user " +
                     "information")
        void givenAProblem_whenTryingToUpdateUserInformation_thenItThrowAnUpdateUserException () throws Exception {
            // GIVEN
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.updateUsagerInfos(anyString(), any())).thenThrow(Exception.class);
            
            // THEN
            assertThatThrownBy(() -> {
                // WHEN
                classUnderTest.updateUserInfos("email", user);
            }).isInstanceOf(UpdateUserException.class);
        }
        
    }
    
    @Nested
    class validEmailWith {
        
        @Test
        @DisplayName("As a user, I want return true, When I try to validate an account with a right token")
        void givenARightToken_whenTryingToValidateAnAccount_thenItReturnTrue () {
            // GIVEN
            VerificationToken verificationToken = new VerificationToken();
            
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.verifyEmailFrom(anyString())).thenReturn(verificationToken);
            when(userService.validAccountFor(any())).thenReturn(true);
            
            // WHEN
            boolean result = classUnderTest.validEmailWith("token");
            
            // THEN
            assertThat(result).isTrue();
        }
        
        @Test
        @DisplayName("As a user, I want return false, When I try to validate an account with a wrong token")
        void givenAWrongToken_whenTryingToValidateAnAccount_thenItReturnFalse () {
            // GIVEN
            VerificationToken verificationToken = new VerificationToken();
            
            when(serviceFactory.getUserService()).thenReturn(userService);
            when(userService.verifyEmailFrom(anyString())).thenReturn(verificationToken);
            when(userService.validAccountFor(any())).thenReturn(false);
            
            // WHEN
            boolean result = classUnderTest.validEmailWith("token");
            
            // THEN
            assertThat(result).isFalse();
        }
        
    }
    
}
