You can find the project on : 

- V1 - https://github.com/D4RT4GNaN/library-web-service.git
- V2 - https://gitlab.com/oc-d4rt4gnan/oc-p10/backend.git
# Library
P10 - DA JAVA Openclassroom - Information system improvements



## Getting Started


You can get this project either by downloading it in ZIP file or cloning it.


This project works in duo with a web service that you will find [here](https://gitlab.com/oc-d4rt4gnan/oc-p10/frontend).


To clone it, go to the folder of your choice in command line and use the following command :

```
$ git clone https://gitlab.com/oc-d4rt4gnan/oc-p10/backend.git

```
See deployment for notes on how to deploy the project on a live system.



### Prerequisites


You need to install :

* [Apache Tomcat](https://tomcat.apache.org/download-90.cgi) version 9.X preferably

* [PostgreSQL](https://www.postgresql.org/download/), the version 11 is used for this project, but you can use the version you like!

* An IDE like [IntelliJ IDEA Ultimate](https://www.jetbrains.com/idea/download/) or [Eclipse JEE](https://www.eclipse.org/downloads/packages/release/2019-06/r/eclipse-ide-enterprise-java-developers)



### Installing

To start with this project :

1. Open it in IDE and setup a tomcat server, if it has not already been done.

2. In IDE, open **/src/main/resources/application.properties** and change data with your connection information to the database.

3. If you want, a demo data set is available in the folder. There are SQL scripts for creating tables and filling them.
Just open the request tool of PgAdmin and open the script *v1.0.0.sql* first and then open the script *v2.0.0.sql* later.
Do the same with scripts in the dataset folder.



## Deployment


To deploy the project on Tomcat server, start by cleaning package (optional but advisor)

```
$ mvn clean

```

Continue by packaging the project

```
$ mvn package

```

Put the war which has just been created, into **apache-tomcat-X.X.XX/webapps**.

And finally, launch the tomcat server with the command

```
$ sh startup.sh

```

*You can found the script into the bin folder of the tomcat server*


The web service is normally accessible at [http://localhost:8080/libraryservice-webservice/](http://localhost:8080/libraryservice-webservice/)



## Built With


* [Maven](https://maven.apache.org/) - Dependency Management and Multi-module Management

* [Spring](https://spring.io/projects/spring-framework) - Used to manage the backend service like DAO and Manager



## Versioning


I use [Git](https://git-scm.com/) for versioning.



## Authors


* **Maxime Blaise** - [D4RT4GNaN](https://gitlab.com/oc-d4rt4gnan/)


