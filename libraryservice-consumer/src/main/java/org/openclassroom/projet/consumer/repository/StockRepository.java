package org.openclassroom.projet.consumer.repository;


import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Stock;
import org.openclassroom.projet.model.database.library.StockId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * JPA Repository for the {@link Stock} object.
 */
@Repository
public interface StockRepository extends JpaRepository<Stock, StockId> {
    
    /**
     * Allows you to find a {@link Stock stock} from the nunber of library and the reference of book associated.
     */
    Stock findByStockId_LibaryNumberRefAndStockId_BookReference (int libraryNumberRef, String bookReference);
    
    
    
    /**
     * Retrieves the {@link Stock inventory} of a {@link Book book} by its reference
     *
     * @param bookReference : The reference of the book you are looking for
     *
     * @return the inventory of a book
     */
    List<Stock> findAllByStockIdBookReference (String bookReference);
    
}
