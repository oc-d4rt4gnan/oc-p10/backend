package org.openclassroom.projet.consumer.repository;


import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.service.WaitingList;
import org.openclassroom.projet.model.database.service.WaitingListId;
import org.openclassroom.projet.model.database.usager.Usager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * JPA Repository for the {@link WaitingList} object.
 */
@Repository
public interface WaitingListRepository extends JpaRepository<WaitingList, WaitingListId> {
    
    /**
     * Checks that a {@link Usager user} is not already on the {@link WaitingList waiting list} for a {@link Book book}
     *
     * @param bookReference : The reference of the book containing the waiting list to check
     * @param usagerId : the identifier of the user to be verified
     *
     * @return if the user is already in the waiting list for the book
     */
    boolean existsWaitingListByWaitingListIdBookReferenceAndWaitingListIdUsagerId(String bookReference, int usagerId);
    
    
    /**
     * Allows you to obtain the size of the {@link WaitingList waiting list} for a {@link Book book}
     *
     * @param reference : The reference of the book you want to know the size of your waiting list
     *
     * @return the size of the waiting list for a book
     */
    int countByWaitingListIdBookReference(String reference);
    
    
    
    /**
     * Allows you to obtain the {@link WaitingList waiting list} for a {@link Book book}
     *
     * @param reference : The reference of the book for which we get the waiting list
     *
     * @return the book waiting list
     */
    List<WaitingList> findWaitingListsByWaitingListIdBookReference(String reference);
    
    
    
    /**
     * Allows you to obtain the {@link WaitingList waiting list} for an {@link Usager user}
     *
     * @param userID
     *         : The user identifier
     *
     * @return the list of reserved books of an user
     */
    List<WaitingList> findWaitingListsByWaitingListIdUsagerId (int userID);
    
}
