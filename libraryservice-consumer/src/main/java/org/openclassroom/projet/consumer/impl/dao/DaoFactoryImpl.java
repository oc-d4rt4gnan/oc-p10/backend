package org.openclassroom.projet.consumer.impl.dao;


import org.openclassroom.projet.consumer.DaoFactory;
import org.openclassroom.projet.consumer.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.inject.Named;


/**
 * Implementation of the access interface to the DAO module.
 */
@Named("daoFactory")
@EnableJpaRepositories(basePackages = "org.openclassroom.projet.consumer.repository")
public class DaoFactoryImpl implements DaoFactory {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    @Autowired
    private BookRepository              bookRepository;
    @Autowired
    private BorrowingRepository         borrowingRepository;
    @Autowired
    private CommentRepository           commentRepository;
    @Autowired
    private LibraryRepository           libraryRepository;
    @Autowired
    private StockRepository             stockRepository;
    @Autowired
    private UsagerRepository            usagerRepository;
    @Autowired
    private VerificationTokenRepository verificationTokenRepository;
    @Autowired
    private WaitingListRepository       waitingListRepository;
    
    
    // =================================================================================================================
    //                                                  GETTERS
    // =================================================================================================================
    
    /**
     * {@inheritDoc}
     */
    @Override
    public BookRepository getBookRepository () {
        return bookRepository;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public BorrowingRepository getBorrowingRepository () {
        return borrowingRepository;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public CommentRepository getCommentRepository () {
        return commentRepository;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public LibraryRepository getLibraryRepository () {
        return libraryRepository;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public StockRepository getStockRepository () {
        return stockRepository;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public UsagerRepository getUsagerRepository () {
        return usagerRepository;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public VerificationTokenRepository getVerificationTokenRepository () {
        return verificationTokenRepository;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public WaitingListRepository getWaitingListRepository () {
        return waitingListRepository;
    }
    
}
