package org.openclassroom.projet.model.database.library;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;


/**
 * Java object for the composite primary key of the {@link Stock} object
 */
@Embeddable
public class StockId implements Serializable {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    @Column(name = "book_reference")
    private String bookReference;
    @Column(name = "library_number_ref")
    private int    libaryNumberRef;
    
    
    // =================================================================================================================
    //                                                CONSTRUCTORS
    // =================================================================================================================
    
    public StockId () {
    }
    
    public StockId (int libaryNumberRef, String bookReference) {
        this.libaryNumberRef = libaryNumberRef;
        this.bookReference   = bookReference;
    }
    
    
    // =================================================================================================================
    //                                              GETTERS / SETTERS
    // =================================================================================================================
    
    public String getBookReference () {
        return bookReference;
    }
    
    public void setBookReference (String bookReference) {
        this.bookReference = bookReference;
    }
    
    public int getLibaryNumberRef () {
        return libaryNumberRef;
    }
    
    public void setLibaryNumberRef (int libaryNumberRef) {
        this.libaryNumberRef = libaryNumberRef;
    }
    
    
    // =================================================================================================================
    //                                              OVERRIDE METHODS
    // =================================================================================================================
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockId stockId = (StockId) o;
        return Objects.equals(libaryNumberRef, stockId.libaryNumberRef) && Objects.equals(bookReference,
                                                                                          stockId.bookReference);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode () {
        return Objects.hash(libaryNumberRef, bookReference);
    }
    
}
