package org.openclassroom.projet.model.security.validators;


import org.openclassroom.projet.model.security.annotations.EnumMatches;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;


/**
 * Validates a field according to certain constraints.
 */
public class EnumMatchesValidator implements ConstraintValidator<EnumMatches, String> {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private List<String> valueList = null;
    
    
    // =================================================================================================================
    //                                              OVERRIDE METHODS
    // =================================================================================================================
    
    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("rawtypes")
    @Override
    public void initialize (EnumMatches constraintAnnotation) {
        valueList = new ArrayList<String>();
        Class<? extends Enum<?>> enumClass = constraintAnnotation.enumClass();
        Enum[] enumValArr = enumClass.getEnumConstants();
    
        for (Enum enumVal : enumValArr) {
            valueList.add(enumVal.toString().toUpperCase());
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid (String value, ConstraintValidatorContext constraintValidatorContext) {
        return valueList.contains(value.toUpperCase());
    }
    
    // =================================================================================================================
    //                                                   SETTERS
    // =================================================================================================================
    
    public void setValueList (List<String> valueList) {
        this.valueList = valueList;
    }
    
}
