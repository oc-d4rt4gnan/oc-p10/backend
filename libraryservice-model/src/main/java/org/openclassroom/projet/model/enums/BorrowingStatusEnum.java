package org.openclassroom.projet.model.enums;


/**
 * Java enum of the Borrowing status
 */
public enum BorrowingStatusEnum {
    OUTSTANDING,
    
    EXTENDED,
    
    OVERDUE,
    
    RETURNED
}
