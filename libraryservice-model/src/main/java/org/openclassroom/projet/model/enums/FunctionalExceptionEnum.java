package org.openclassroom.projet.model.enums;


/**
 * Enumerator of functional exception codes.
 * It allows you to retrieve error messages from the properties files.
 */
public enum FunctionalExceptionEnum {
    
    // =================================================================================================================
    //                                                   ENUMS
    // =================================================================================================================
    
    CURRENTLY_BOOKED("exception.functional.currently_booked"),
    
    WAITINGLIST_TOO_LONG("exception.functional.too_long"),
    
    NO_BOOK_AVAILABLE("exception.functional.no_book_available"),
    
    EMAIL_NOT_SENT("exception.functional.email_not_sent");
    
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private String code;
    
    
    // =================================================================================================================
    //                                                CONSTRUCTORS
    // =================================================================================================================
    
    FunctionalExceptionEnum (String code) {
        this.code = code;
    }
    
    
    // =================================================================================================================
    //                                              GETTERS / SETTERS
    // =================================================================================================================
    
    public String getCode () {
        return code;
    }
}
