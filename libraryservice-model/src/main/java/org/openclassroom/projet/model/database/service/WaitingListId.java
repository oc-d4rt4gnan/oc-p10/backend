package org.openclassroom.projet.model.database.service;


import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.usager.Usager;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * Java object for the composite primary key of the {@link WaitingList} object
 */
@Embeddable
public class WaitingListId implements Serializable {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    @OneToOne
    @JoinColumn(name = "book_reference")
    private Book    book;
    @OneToOne
    @JoinColumn(name = "usager_id")
    private Usager  usager;
    
    
    // =================================================================================================================
    //                                                CONSTRUCTORS
    // =================================================================================================================
    
    public WaitingListId () {
    }
    
    public WaitingListId (Book book, Usager usager) {
        this.book   = book;
        this.usager = usager;
    }
    
    
    // =================================================================================================================
    //                                              GETTERS / SETTERS
    // =================================================================================================================
    
    public Book getBook () {
        return book;
    }
    
    public void setBook (Book book) {
        this.book = book;
    }
    
    public Usager getUsager () {
        return usager;
    }
    
    public void setUsager (Usager usager) {
        this.usager = usager;
    }
    
    
    // =================================================================================================================
    //                                              OVERRIDE METHODS
    // =================================================================================================================
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WaitingListId waitingListId = (WaitingListId) o;
        return Objects.equals(book, waitingListId.book) && Objects.equals(usager, waitingListId.usager);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode () {
        return Objects.hash(book, usager);
    }
    
}
