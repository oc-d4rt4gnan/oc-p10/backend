package org.openclassroom.projet.model.security.validators;


import org.openclassroom.projet.model.security.annotations.ValidEmail;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Validates an email according to certain constraints.
 */
public class EmailValidator implements ConstraintValidator<ValidEmail, String> {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private static final String  EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@" +
                                                 "[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$";
    private              Matcher matcher;
    private              Pattern pattern;
    
    
    // =================================================================================================================
    //                                              OVERRIDE METHODS
    // =================================================================================================================
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize (ValidEmail constraintAnnotation) {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid (String email, ConstraintValidatorContext context) {
        return (validateEmail(email));
    }
    
    
    // =================================================================================================================
    //                                              PRIVATE  METHODS
    // =================================================================================================================
    
    // Compares an email with a pattern in regex format.
    private boolean validateEmail (String email) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    
}
