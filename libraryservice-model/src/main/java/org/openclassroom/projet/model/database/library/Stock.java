package org.openclassroom.projet.model.database.library;


import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * Java bean of the Stock object
 */
@Entity
@Table(name = "stock")
public class Stock {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    @NotNull
    @NotEmpty
    private int     quantity;
    @NotNull
    @NotEmpty
    @Column(name = "quantity_borrowed")
    private int     quantityBorrowed;
    @EmbeddedId
    private StockId stockId;
    
    
    // =================================================================================================================
    //                                                CONSTRUCTORS
    // =================================================================================================================
    
    public Stock () {
    }
    
    public Stock (StockId stockId, int quantity, int quantityBorrowed) {
        this.stockId          = stockId;
        this.quantity         = quantity;
        this.quantityBorrowed = quantityBorrowed;
    }
    
    
    // =================================================================================================================
    //                                              GETTERS / SETTERS
    // =================================================================================================================
    
    public int getQuantity () {
        return quantity;
    }
    
    public void setQuantity (int quantity) {
        this.quantity = quantity;
    }
    
    public int getQuantityBorrowed () {
        return quantityBorrowed;
    }
    
    public void setQuantityBorrowed (int quantityBorrowed) {
        this.quantityBorrowed = quantityBorrowed;
    }
    
    public StockId getStockId () {
        return stockId;
    }
    
    public void setStockId (StockId stockId) {
        this.stockId = stockId;
    }
    
}
