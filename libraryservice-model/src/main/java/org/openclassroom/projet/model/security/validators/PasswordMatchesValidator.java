package org.openclassroom.projet.model.security.validators;


import org.openclassroom.projet.model.database.usager.UsagerDto;
import org.openclassroom.projet.model.security.annotations.PasswordMatches;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


/**
 * Compares the equivalence of 2 passwords .
 */
public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    
    
    // =================================================================================================================
    //                                              OVERRIDE METHODS
    // =================================================================================================================
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize (PasswordMatches constraintAnnotation) {
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isValid (Object obj, ConstraintValidatorContext context) {
        UsagerDto user = (UsagerDto) obj;
        if (user.getConfirmPassword() != null) {
            return user.getPassword().equals(user.getConfirmPassword()) ||
                   passwordEncoder.matches(user.getConfirmPassword(), user.getPassword());
        } else {
            return true;
        }
    }
}
