package org.openclassroom.projet.model.security.annotations;


import org.openclassroom.projet.model.security.validators.EmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 * Checks if the email matches the classic pattern of an email and is likely to be a real email.
 */
@Target({ TYPE, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = EmailValidator.class)
@Documented
public @interface ValidEmail {
    
    Class<?>[] groups () default {};
    
    
    
    String message () default "Invalid email";
    
    
    
    Class<? extends Payload>[] payload () default {};
    
}
