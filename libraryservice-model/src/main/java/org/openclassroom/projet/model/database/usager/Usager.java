package org.openclassroom.projet.model.database.usager;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;


/**
 * Java bean of the Usager object
 */
@Entity
@Table(name = "usager")
public class Usager implements Serializable, UserDetails {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private String  address;
    private String  email;
    @Column(name = "enabled")
    private boolean enabled;
    @Column(name = "first_name")
    private String  firstName;
    @Id
    @GeneratedValue
    private int     id;
    @Column(name = "last_name")
    private String  lastName;
    private String  password;
    
    
    // =================================================================================================================
    //                                                CONSTRUCTORS
    // =================================================================================================================
    
    public Usager () {
        super();
        this.enabled = false;
        
    }
    
    public Usager (UsagerDto usagerDto) {
        this.email     = usagerDto.getEmail();
        this.password  = usagerDto.getPassword() != null ? encodePassword(usagerDto.getPassword()) : null;
        this.firstName = usagerDto.getFirstName();
        this.lastName  = usagerDto.getLastName();
        this.address   = usagerDto.getAddress();
        this.enabled   = false;
    }
    
    public Usager (String email, String password, String firstName, String lastName, String address) {
        super();
        this.enabled   = false;
        this.email     = email;
        this.password  = password;
        this.firstName = firstName;
        this.lastName  = lastName;
        this.address   = address;
    }
    
    
    // =================================================================================================================
    //                                              GETTERS / SETTERS
    // =================================================================================================================
    
    public String getAddress () {
        return address;
    }
    
    public void setAddress (String address) {
        this.address = address;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities () {
        return null;
    }
    
    public String getPassword () {
        return password;
    }
    
    public void setPassword (String password) {
        this.password = password;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getUsername () {
        return email;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAccountNonExpired () {
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAccountNonLocked () {
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCredentialsNonExpired () {
        return false;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEnabled () {
        return this.enabled;
    }
    
    public void setEnabled (boolean enabled) {
        this.enabled = enabled;
    }
    
    public String getEmail () {
        return email;
    }
    
    public void setEmail (String email) {
        this.email = email;
    }
    
    public String getFirstName () {
        return firstName;
    }
    
    public void setFirstName (String firstName) {
        this.firstName = firstName;
    }
    
    public int getId () {
        return id;
    }
    
    public void setId (int id) {
        this.id = id;
    }
    
    public String getLastName () {
        return lastName;
    }
    
    public void setLastName (String lastName) {
        this.lastName = lastName;
    }
    
    
    // =================================================================================================================
    //                                              PRIVATE  METHODS
    // =================================================================================================================
    
    // Encode the password with BCryptPasswordEncoder.
    private String encodePassword (String password) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.encode(password);
    }
    
}
