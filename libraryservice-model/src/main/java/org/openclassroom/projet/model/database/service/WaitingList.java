package org.openclassroom.projet.model.database.service;


import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.usager.Usager;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


/**
 * Java bean of the WaitingList object
 */
@Entity
@Table(name = "waiting_list")
public class WaitingList {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    @EmbeddedId
    private WaitingListId waitingListId;
    @NotNull
    @NotEmpty
    @Column(name = "reservation_date")
    private LocalDateTime reservationDate;
    @Column(name = "sending_mail_date")
    private LocalDateTime sendingMailDate;
    @NotNull
    @NotEmpty
    @Column(name = "is_email_sent")
    private boolean       isEmailSent;
    
    
    // =================================================================================================================
    //                                                CONSTRUCTORS
    // =================================================================================================================
    
    public WaitingList () {
    }
    
    public WaitingList (Book book, Usager user) {
        this.waitingListId   = new WaitingListId(book, user);
        this.reservationDate = LocalDateTime.now();
        this.isEmailSent     = false;
    }
    
    
    // =================================================================================================================
    //                                              GETTERS / SETTERS
    // =================================================================================================================
    
    public WaitingListId getWaitingListId () {
        return waitingListId;
    }
    
    public void setWaitingListId (WaitingListId waitingListId) {
        this.waitingListId = waitingListId;
    }
    
    public LocalDateTime getReservationDate () {
        return this.reservationDate;
    }
    
    public void setReservationDate (LocalDateTime reservationDate) {
        this.reservationDate = reservationDate;
    }
    
    public LocalDateTime getSendingMailDate () {
        return sendingMailDate;
    }
    
    public void setSendingMailDate (LocalDateTime sendingMailDate) {
        this.sendingMailDate = sendingMailDate;
    }
    
    public boolean isEmailSent () {
        return isEmailSent;
    }
    
    public void setEmailSent (boolean emailSent) {
        isEmailSent = emailSent;
    }
    
}
