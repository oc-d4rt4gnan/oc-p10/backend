package org.openclassroom.projet.model.security.annotations;


import org.openclassroom.projet.model.security.validators.PasswordConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 * Verifies that the password corresponds to the restrictions imposed.
 */
@Documented
@Constraint(validatedBy = PasswordConstraintValidator.class)
@Target({ TYPE, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
public @interface ValidPassword {
    
    Class<?>[] groups () default {};
    
    
    
    String message () default "Invalid Password";
    
    
    
    Class<? extends Payload>[] payload () default {};
    
}
