package org.openclassroom.projet.model.security.validators;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.openclassroom.projet.model.database.usager.UsagerDto;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Validator test class {@link PasswordMatchesValidator}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 12/09/2021
 */
class PasswordMatchesValidatorTest {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private static PasswordMatchesValidator classUnderTest;
    
    
    // =================================================================================================================
    //                                                   TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new PasswordMatchesValidator();
    }
    
    @Test
    @DisplayName("As a system, I want to return true, When the password and its confirmation matches.")
    void givenSamePasswordAndConfirmation_whenCheckingIfThePasswordAndItsConfirmationIsMatch_thenItReturnTrue () {
        // GIVEN
        UsagerDto user = new UsagerDto();
        user.setPassword("password");
        user.setConfirmPassword("password");
        // WHEN
        boolean result = classUnderTest.isValid(user, null);
        
        // THEN
        assertThat(result).isTrue();
    }
    
    @Test
    @DisplayName("As a system, I want to return false, When the password and its confirmation doesn't matches.")
    void givenDifferentPasswordAndConfirmation_whenCheckingIfThePasswordAndItsConfirmationIsMatch_thenItReturnFalse () {
        // GIVEN
        UsagerDto user = new UsagerDto();
        user.setPassword("password");
        user.setConfirmPassword("confirmation");
        
        // WHEN
        boolean result = classUnderTest.isValid(user, null);
        
        // THEN
        assertThat(result).isFalse();
    }
    
}
