package org.openclassroom.projet.model.security.validators;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

import javax.validation.ConstraintValidatorContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


/**
 * {@link PasswordConstraintValidator} test class
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 05/09/2021
 */
class PasswordConstraintValidatorTest {
    
    // =================================================================================================================
    //                                                   ATTRIBUTES
    // =================================================================================================================
    
    private static PasswordConstraintValidator classUnderTest;
    
    private ConstraintValidatorContext context;
    
    
    // =================================================================================================================
    //                                                      TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void globalInit () {
        classUnderTest = new PasswordConstraintValidator();
    }
    
    @BeforeEach
    void prepareMocks () {
        context = Mockito.mock(ConstraintValidatorContext.class);
        ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder = Mockito.mock(
                ConstraintValidatorContext.ConstraintViolationBuilder.class);
        
        when(context.buildConstraintViolationWithTemplate(Mockito.anyString())).thenReturn(constraintViolationBuilder);
    }
    
    @Test
    @DisplayName("As a system, I want to return true, when the provided password is valid.")
    void givenAValidPassword_whenCheckIfPasswordIsValid_thenItReturnTrue () {
        // GIVEN
        String password = "e@#m_8T9aT-$JVmS";
        
        // WHEN
        boolean result = classUnderTest.isValid(password, context);
        
        // THEN
        assertThat(result).isTrue();
    }
    
    @ParameterizedTest(name = "\"{0}\" must not be a valid password")
    @ValueSource(strings = {
            "4pW+y9=", "CJTXTUT2A687", "=8__&=&!*#4676", "XLcVaDAvSBdnY", "mpT-7-R2@ =*6q4Hr"
    })
    @DisplayName("As a system, I want to return false, When the password provided is not valid.")
    void givenAnInvalidPassword_whenCheckIfPasswordIsValid_thenItReturnFalse (String password) {
        // GIVEN
        doNothing().when(context).disableDefaultConstraintViolation();
        when(context.buildConstraintViolationWithTemplate(anyString()).addConstraintViolation()).thenReturn(context);
        
        // WHEN
        boolean result = classUnderTest.isValid(password, context);
        
        // THEN
        assertThat(result).isFalse();
    }
    
}
