package org.openclassroom.projet.model.security.validators;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;


/**
 * @author D4RT4GNaN - Maxime Blaise
 * @since 05/09/2021
 */
class EnumMatchesValidatorTest {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private static EnumMatchesValidator classUnderTest;
    
    
    // =================================================================================================================
    //                                                   TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new EnumMatchesValidator();
    }
    
    @BeforeEach
    void initializeEnumValues () {
        List<String> enumValues = Arrays.asList("ELEMENT_1", "ELEMENT_2");
        classUnderTest.setValueList(enumValues);
        EnumMatchesValidator spy = Mockito.spy(classUnderTest);
        doNothing().when(spy).initialize(any());
    }
    
    @Test
    @DisplayName("As a system, I want to return true, When the provided element is contained in the enumerator")
    void givenAnElementInEnum_whenCheckingIfElementIsInEnum_thenItReturnTrue () {
        // GIVEN
        String value = "ELEMENT_2";
        
        // WHEN
        boolean result = classUnderTest.isValid(value, null);
        
        // THEN
        assertThat(result).isTrue();
    }
    
    @Test
    @DisplayName("As a system, I want to return false, when the provided element is not contained in the enumerator")
    void givenAnElementNotInEnum_whenCheckingIfElementIsInEnum_thenItReturnFalse () {
        // GIVEN
        String value = "ELEMENT_3";
        
        // WHEN
        boolean result = classUnderTest.isValid(value, null);
        
        // THEN
        assertThat(result).isFalse();
    }
    
}
