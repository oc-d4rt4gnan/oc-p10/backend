package org.openclassroom.projet.model.security.validators;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Validator test class {@link EmailValidator}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 05/09/2021
 */
class EmailValidatorTest {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private static EmailValidator classUnderTest;
    
    
    // =================================================================================================================
    //                                                   TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new EmailValidator();
    }
    
    @Test
    @DisplayName("As a system, I want to return true, When the string of an email is valid")
    void givenAValidEmail_whenCheckingTheValidityOfEmailString_thenItReturnTrue () {
        // GIVEN
        String email = "nom.prenom@email.com";
        
        // WHEN
        boolean result = classUnderTest.isValid(email, null);
        
        // THEN
        assertThat(result).isTrue();
    }
    
    @ParameterizedTest(name = "\"{0}\" must not be a valid email")
    @ValueSource(strings = {
            "@email.com", "nom.prenomemail.com", "nom.prenom@.com", "nom.prenom@", "nom.prenom", "@", ".", "@.", ""
    })
    @DisplayName("As a system, I want to return false, When the string of an email is not valid")
    void givenAnInvalidEmail_whenCheckingTheValidityOfEmailString_thenItReturnFalse (String email) {
        // GIVEN - Nothing to do
        
        // WHEN
        boolean result = classUnderTest.isValid(email, null);
        
        // THEN
        assertThat(result).isFalse();
    }
    
}
