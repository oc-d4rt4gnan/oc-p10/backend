package org.openclassroom.projet.model.database.usager;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author D4RT4GNaN - Maxime Blaise
 * @since 05/09/2021
 */
class VerificationTokenTest {
    
    // =================================================================================================================
    //                                                  ATTRIBUTES
    // =================================================================================================================
    
    private static VerificationToken classUnderTest;
    
    
    // =================================================================================================================
    //                                                    TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new VerificationToken();
    }
    
    @Test
    @DisplayName(
            "As a system, I want to have an expiration date equivalent to 24 hours longer than now, When resetting " +
            "the expiration date")
    void givenAnExpirationDateInVerificationToken_whenResetTheExpiryDate_thenTheNewExpiryDateIsIn24Hours () {
        // GIVEN
        DateFormat shortDateFormat = DateFormat.getDateTimeInstance(
                DateFormat.SHORT,
                DateFormat.SHORT);
        
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 12);
        classUnderTest.setExpiryDate(calendar.getTime());
        
        calendar.add(Calendar.MINUTE, 1428); // 60 * 24 - 12
        String expected = shortDateFormat.format(calendar.getTime());
        
        // WHEN
        classUnderTest.resetExpiryDate();
        String result = shortDateFormat.format(classUnderTest.getExpiryDate());
        
        // THEN
        assertThat(result).isEqualTo(expected);
    }
    
}
