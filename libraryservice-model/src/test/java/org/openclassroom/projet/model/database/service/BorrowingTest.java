package org.openclassroom.projet.model.database.service;


import org.junit.jupiter.api.*;
import org.openclassroom.projet.model.enums.BorrowingStatusEnum;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Test class for the business code of the {@link Borrowing} model
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 05/09/2021
 */
class BorrowingTest {
    
    // =================================================================================================================
    //                                                 ATTRIBUTES
    // =================================================================================================================
    
    private static Borrowing classUnderTest;
    private Calendar calendar;
    
    
    
    // =================================================================================================================
    //                                                    TESTS
    // =================================================================================================================
    
    @BeforeAll
    static void init () {
        classUnderTest = new Borrowing();
    }
    
    @BeforeEach
    void initializeVariable () {
        classUnderTest.setStatus(BorrowingStatusEnum.OUTSTANDING.name());
        calendar = Calendar.getInstance();
    }
    
    @Nested
    @DisplayName("Checks the validity of a loan against its expiration date")
    class isValid {
    
        @Test
        @DisplayName("As a system, I want to return true, When the borrowing is valid")
        void givenAValidBorrowing_whenCheckBorrowingIsValid_thenItReturnTrue () {
            // GIVEN
            calendar.add(Calendar.DATE, 14);
            classUnderTest.setExpiryDate(calendar.getTime());
        
            // WHEN
            boolean result = classUnderTest.isValid();
        
            // THEN
            assertThat(result).isTrue();
        }
    
        @Test
        @DisplayName("As a system, I want to return false, When the borrowing is invalid")
        void givenAnInvalidBorrowing_whenCheckBorrowingIsValid_thenItReturnFalse () {
            // GIVEN
            calendar.add(Calendar.DATE, -7);
            classUnderTest.setExpiryDate(calendar.getTime());
        
            // WHEN
            boolean result = classUnderTest.isValid();
        
            // THEN
            assertThat(result).isFalse();
        }
    
        @Test
        @DisplayName("As a system, I want to change the status to OVERDUE, When the borrowing is invalid")
        void givenAnInvalidBorrowing_whenCheckBorrowingIsValid_thenTheStatusOfBorrowingIsChangesByOverdue () {
            // GIVEN
            calendar.add(Calendar.DATE, -7);
            classUnderTest.setExpiryDate(calendar.getTime());
        
            // WHEN
            classUnderTest.isValid();
        
            // THEN
            assertThat(classUnderTest.getStatus()).isEqualTo(BorrowingStatusEnum.OVERDUE.name());
        }
    }
    
    @Test
    @DisplayName("As a system, I want to add 2 weeks to the expiration date, When extending a borrowing period")
    void givenABorrowing_whenExtendingTheBorrowing_thenTheNewExpiryDateIs28DaysAfterTheActualDate () {
        // GIVEN
        calendar.add(Calendar.DATE, 14);
        classUnderTest.setExpiryDate(calendar.getTime());
        calendar.add(Calendar.DATE, 14);
        Date expected = calendar.getTime();
        
        // WHEN
        classUnderTest.extendBorrowing();
        
        // THEN
        assertThat(classUnderTest.getExpiryDate()).isEqualTo(expected);
    }
}
