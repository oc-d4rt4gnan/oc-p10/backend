package org.openclassroom.projet.batch.configs;


import org.openclassroom.projet.batch.tasklets.DeleteTask;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * Batch configuration class
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 28-08-2021
 */
@Configuration
@EnableBatchProcessing
public class BatchConfig {
    
    // =================================================================================================================
    //                                                    ATTRIBUTES
    // =================================================================================================================
    
    @Autowired
    private JobBuilderFactory jobs;
    
    @Autowired
    private StepBuilderFactory steps;
    
    
    // =================================================================================================================
    //                                                       BEANS
    // =================================================================================================================
    
    @Bean
    public Step stepDelete () {
        return steps.get("stepDelete").tasklet(new DeleteTask()).build();
    }
    
    @Bean
    public Job batchJob () {
        return jobs.get("batchJob").incrementer(new RunIdIncrementer()).start(stepDelete()).build();
    }
    
}
