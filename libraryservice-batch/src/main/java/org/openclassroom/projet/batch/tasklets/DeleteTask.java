package org.openclassroom.projet.batch.tasklets;


import org.openclassroom.projet.business.services.contract.ServiceFactory;
import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.library.Library;
import org.openclassroom.projet.model.database.service.WaitingList;
import org.openclassroom.projet.model.database.usager.Usager;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


/**
 * Check for each {@link Book book} if it has been less than 48 hours since the
 * emails of availability have been sent to the first {@link Usager users}, if not they are removed from the
 * {@link WaitingList list} and the email is sent back to the following {@link Usager ones}.
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 28-08-2021
 */
public class DeleteTask implements Tasklet {
    
    // =================================================================================================================
    //                                                    ATTRIBUTES
    // =================================================================================================================
    
    @Autowired
    private ServiceFactory services;
    
    
    // =================================================================================================================
    //                                                       TASKS
    // =================================================================================================================
    
    /**
     * {@inheritDoc}
     */
    @Override
    public RepeatStatus execute (
            StepContribution stepContribution, ChunkContext chunkContext
    ) {
        // Retrieving the book list
        List<Book> books = services.getBookService().getBooks("");
        
        // For each book
        books.forEach(book -> {
            // We check that it has been less than 48 hours since the email was sent to the first user on the waiting
            // list
            try {
                if (!services.getWaitingListService().checkIsUnderDelay(book)) {
            
                    // Otherwise, it is removed from the waiting list
                    services.getWaitingListService().removeFirstReservation(book);
            
                    // And we send the email to the next user in the list
                    Library library = services.getLibraryService().getLibraryWhereAreTheReturnedBook(book);
                    services.getWaitingListService().informsTheFirstUserInTheList(book.getReference(),
                                                                                  library.getNumberRef());
                }
            } catch (Exception exception) {
                throw new RuntimeException(exception);
            }
        });
        
        return RepeatStatus.FINISHED;
    }
    
}
