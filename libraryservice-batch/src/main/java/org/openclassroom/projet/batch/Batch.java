package org.openclassroom.projet.batch;


import org.openclassroom.projet.model.database.library.Book;
import org.openclassroom.projet.model.database.usager.Usager;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


/**
 * Batch to check the delay between the sending of the availability email of a {@link Book book} and its recovery by
 * the {@link Usager user}
 *
 * @author D4RT4GNaN - Maxime Blaise
 * @since 28-08-2021
 */
@SpringBootApplication
@EnableScheduling
public class Batch {
    
    // =================================================================================================================
    //                                                    ATTRIBUTES
    // =================================================================================================================
    
    @Autowired
    private JobLauncher jobLauncher;
    
    @Autowired
    private Job job;
    
    
    // =================================================================================================================
    //                                                       MAIN
    // =================================================================================================================
    
    public static void main (String[] args) {
        SpringApplication.run(Batch.class, args);
    }
    
    
    // =================================================================================================================
    //                                                     SCHELUDER
    // =================================================================================================================
    
    @Scheduled(cron = "0 0 * ? * *")
    public void perform () throws Exception {
        JobParameters params = new JobParametersBuilder().addString("JobID", String.valueOf(System.currentTimeMillis()))
                                                         .toJobParameters();
        jobLauncher.run(job, params);
    }
    
}
